# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html). Dates comply with the ISO 8601 format.

Please keep in mind that KerA is still in the initial development phase.

## [Unreleased]

In progress.

This release focuses on shifting the terminology used in the code from that of RAMCloud to that of KerA.

### Added


### Changed


## [0.3.1] - 2021-03-25

This release focuses on making clients easier to compile.

Corresponding APP deposit: `none`.

### Added

- Make target to build all clients at once: `make clients` and `make install-clients`.
- Dockerfile to build an image containing all clients.

### Changed

- Moved the logic in `jni-library-plugin` to a standard `build.gradle.kts` file to build Java bindings.

### Fixed

- Compilation of unit tests. They do not pass yet.

## [0.3.0] - 2020-11-20

This release brings some consistency to KerA.

Corresponding APP deposit: `0.3.0`.

### Added

- Docker support.
- Support for JDK 11 and Python 3.
- Pre-commit hooks.

### Changed

- Gradle version to compile Java bindings 2.7 -> 6.7.
- Python version to run Python bindings 2 -> 3.
- Renamed some poorly named classes.
- Renamed C++ namepaces and Java packages.

### Removed

- Packed Flink logic as a standalone connector, now living in a separate repository.
- Moved Flink samples to a separate repository.

### Fixed

- An assertion causing a failure in Java bindings.

## [0.2.0] - 2020-03-02

Corresponding APP deposit: `0.1.1`.

### Added

- Multi-write operations.
- Java bindings.
- Optimized clients.
- Apache Arrow integration.
- Apache Flink integration, including code samples.

## [0.1.0] - 2017-11-09

This release is an early prototype of KerA as described in [the whitepaper](https://hal.inria.fr/tel-02127065/).

Corresponding APP deposit: `0.1.0` (a few commits behind this release).

### Added

- First implementation of KerA-specific concepts: segments, groups, streamlets, persistence manager.
- Basic clients for demonstration purposes.

## [0.0.0] - 2017-07-06

KerA is built on top of [RAMCloud](https://github.com/PlatformLab/RAMCloud), a project based in the Department of Computer Science at Stanford University (no longer maintained).

This release actually corresponds to the status of RAMCloud as of 2017-07-06 ([commit f532023](https://github.com/PlatformLab/RAMCloud/commit/f53202398b4720f20b0cdc42732edf48b928b8d7)).

[Unreleased]: https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/compare/v0.3.1...develop
[0.3.1]: https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/compare/v0.0.0...v0.1.0
[0.0.0]: https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/tags/v0.0.0
