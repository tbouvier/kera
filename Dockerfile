# ##############################################################################
# 
# Builder stage
#
# ##############################################################################

FROM debian:buster AS builder

ARG JAVA_VERSION=11.0.2-open
ENV SDKMAN_DIR=/root/.sdkman

WORKDIR /opt
COPY . /opt/kera

#
# Install dependencies
#

RUN apt-get update \
 && apt-get install --yes \
      apt-transport-https \
      build-essential \
      ca-certificates \
      curl \
      doxygen \
      g++ \
      gdb \
      git \
      libboost-filesystem-dev \
      libboost-program-options-dev \
      libboost-system-dev \
      libibverbs-dev \
      libpcre++-dev \
      libssl-dev \
      libzookeeper-mt-dev \
      procps \
      protobuf-compiler \
      python3 \
      python3-pip \
      software-properties-common \
      unzip \
      wget \
      zip \
 && rm -rf /var/lib/apt/lists/*

#
# Install Python
#

# Make python3 the default python binary
RUN ln -s /usr/bin/python3 /usr/bin/python \
 && ln -s /usr/bin/pip3 /usr/bin/pip

#
# Install Java
#

RUN curl -s "https://get.sdkman.io" | bash \
 && echo "sdkman_auto_answer=true" > $SDKMAN_DIR/etc/config \
 && echo "sdkman_auto_selfupdate=false" >> $SDKMAN_DIR/etc/config

# Source sdkman to make the sdk command available and install java candidate
RUN bash -c "source $SDKMAN_DIR/bin/sdkman-init.sh && sdk install java $JAVA_VERSION"

# Add candidate path to $PATH environment variable
ENV JAVA_HOME="$SDKMAN_DIR/candidates/java/current"
ENV PATH="$JAVA_HOME/bin:$PATH"

#
# Install cmake
#

#RUN wget https://cmake.org/files/v3.11/cmake-3.11.1.tar.gz \
# && tar -xvf cmake-3.11.1.tar.gz && mv cmake-3.11.1 cmake \
# && cd cmake \
# && ./configure --prefix=/opt/cmake/ \
# && make

RUN curl -sSL https://cmake.org/files/v3.11/cmake-3.11.1-Linux-x86_64.tar.gz | tar -xzC . \
 && mv cmake-3.11.1-Linux-x86_64 cmake

ENV PATH="/opt/cmake/bin/:$PATH"

#
# Compile KerArrow
#

RUN cd /opt/kera/kerarrow \
 && mkdir -p cpp/release \
 && cd cpp/release/ \
 && cmake .. -DCMAKE_BUILD_TYPE=Release -DARROW_PLASMA=on \
 && make -j12 \
 && make install

#
# Compile KerA
#

RUN cd kera \
 && make clean \
 && make -j12 \
 && make install

# ##############################################################################
# 
# Final stage
# No JDK is included in the final image.
#
# ##############################################################################

FROM debian:buster

#
# Installing binaries
#

COPY --from=builder \
 /opt/kera/install/bin /opt/kera/install/bin

COPY --from=builder \
 /opt/kera/kerarrow/cpp/release/release/plasma_store \
 /opt/kera/install/bin/

#
# Installing libraries
#

COPY --from=builder \
 /opt/kera/install/lib/kera/kera_jni.jar \
 /opt/kera/install/lib/kera/

COPY --from=builder \
 /opt/kera/install/lib/kera/lib* \
 /opt/kera/kerarrow/cpp/release/release/lib* \
 /usr/local/lib/

COPY --from=builder \
 /usr/lib/x86_64-linux-gnu/libzookeeper* \
 /usr/lib/x86_64-linux-gnu/libboost* \
 /usr/lib/x86_64-linux-gnu/libprotobuf* \
 /usr/lib/x86_64-linux-gnu/libpcre* \
 /usr/lib/x86_64-linux-gnu/libssl* \
 /usr/lib/x86_64-linux-gnu/libcrypto* \
 /usr/lib/x86_64-linux-gnu/

RUN mkdir /logs

ENV LD_LIBRARY_PATH="/usr/local/lib/:$LD_LIBRARY_PATH"

RUN ldd /usr/local/lib/libkera.so \
 && ldd /opt/kera/install/bin/coordinator \
 && ldd /opt/kera/install/bin/server

COPY ./docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
