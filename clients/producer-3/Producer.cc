/* Copyright 2017-2020 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <thread>
#include <list>
#include <string>
#include <queue>
#include <iostream>
#include <sstream>
#include <atomic>
#include <algorithm>
#include <cassert>

#include "Logger.h"
#include "Common.h"
#include "Cycles.h"
#include "Buffer.h"
#include "StreamObject.h"
#include "ShortMacros.h"
#include "ClientException.h"
#include "RamCloud.h"
#include "Util.h"
#include "Tub.h"
#include "Segment.h"
#include "WireFormat.h"
#include "LogMetadata.h"
#include "WriteGroupBatchMultiRpcWrapper.h"
#include "ChunkEntryDigestChecksum.h"
#include "ChunkEntry.h"
#include "TimeTrace.h"
#include "PerfStats.h"
#include "Crc32C.h"

// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void) msg, exp))

// How much memory the producer can use to batch records before sending them
// = CACHE_SIZE * BATCH_SIZE
#define CACHE_SIZE 1024
#define MAXNUMSTREAMLETS 128

struct MultiWriteGroupBatchObjectE;

// Used to access streamletBatches[streamletId]
KerA::UnnamedSpinLock streamletActiveLocks[MAXNUMSTREAMLETS];  // gives a lock for each streamletId deque

// key=streamletId, value is a list of indexes to batchesReuse.
// We append to the tail of indexes, the others should be closed.
// to be used by producers for appending next record
// to be used by writers to identify "to be/closed" batches to be written
// streamletBatches[streamletId] is protected by streamletActiveLocks[streamletId%MAXNUMSTREAMLETS]
std::map<uint32_t, std::deque<uint32_t>> streamletBatches;

// contains batch objects with objects' content in buffers[i]
// access is protected by streamletActiveLocks[streamletId%MAXNUMSTREAMLETS]
std::array<MultiWriteGroupBatchObjectE*, CACHE_SIZE> batchesReuse;  // size of CACHE_SIZE

// next is used to access batchesReuseIndexes
KerA::SpinLock cacheLock("batches");  // for batches

// here the writers push written object's indexes
// it is used by producers ONlY if streamletBatches[streamletId].back contains no open batch to append records to
// protected by cacheLock
std::queue<uint32_t> batchesReuseIndexes;
// used to drain batchesReuseIndexes
std::queue<uint32_t> batchesReuseIndexesCached;

std::atomic<uint64_t> records;  // total records written until now, accessed by main thread and writers
std::atomic<bool> producerStop;
std::atomic<bool> writerStop;

KerA::Tub<KerA::Buffer> buffers[CACHE_SIZE];

struct MultiWriteGroupBatchObjectE: public KerA::MultiWriteGroupBatchObject {
    std::atomic<bool> closed;
    uint64_t creationTime;  // initialized when first record is appended to
    KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart* batchPart;
    uint32_t streamletIdBatchIndex;

    MultiWriteGroupBatchObjectE(KerA::Tub<KerA::Buffer>* objects, uint64_t entryId, uint64_t keyHash) :
            MultiWriteGroupBatchObject(objects, entryId, keyHash),
            closed(false),
            creationTime(-1),
            batchPart(NULL),
            streamletIdBatchIndex(-1) {}

    MultiWriteGroupBatchObjectE() :
            MultiWriteGroupBatchObject(),
            closed(),
            creationTime(),
            batchPart(),
            streamletIdBatchIndex() {}

    MultiWriteGroupBatchObjectE(const MultiWriteGroupBatchObjectE& other) :
            MultiWriteGroupBatchObject(other),
            closed(false),
            creationTime(other.creationTime),
            batchPart(NULL),
            streamletIdBatchIndex(other.streamletIdBatchIndex) {}

    MultiWriteGroupBatchObjectE& operator=(const MultiWriteGroupBatchObjectE& other) {
        MultiWriteGroupBatchObject::operator=(other);
        closed = false;
        creationTime = other.creationTime;
        batchPart = other.batchPart;
        streamletIdBatchIndex = other.streamletIdBatchIndex;
        return *this;
    }
};

/**
 * streamId
 * BATCH_SIZE in total bytes per batch
 * RECORD_SIZE in bytes record value
 * KEY_SIZE in bytes record key
 * NSTREAMLETS number of streamlets from 1..N
 * NNODES number of nodes from 1..N not used for now
 * partitionerMode if false partition round-robin (next streamlet), if true partition by key#hash
 */
void produce_batches(uint64_t streamId, uint32_t BATCH_SIZE,
            uint32_t RECORD_SIZE, uint16_t KEY_SIZE, uint32_t NSTREAMLETS,
            uint32_t NNODES, bool partitionerMode, uint64_t recordCount, uint64_t PRODID)
try {
    uint32_t streamletId = 0;  // to be initialized later, >=1

    uint32_t streamletSpan = NSTREAMLETS;
    uint64_t tabletRange = 1 + ~0UL / streamletSpan;
    uint64_t keyHash = -1;  // (streamletId-1) * tabletRange;
    std::string prodids = std::to_string(PRODID) + " ";

    assertm(KEY_SIZE >= prodids.length(), "Key buffer is not large enough");

    // When stream record has no key, initialize entryHeaderBuffer
    KerA::Tub<KerA::Buffer> entryHeaderBuffer;  // overhead per record
    entryHeaderBuffer.construct();

    // Total object length including overhead/headers - minus entryHeader
    KerA::ObjectLength objectLength = KerA::sizeof32(KerA::KeyCount) + KerA::sizeof32(KerA::CumulativeKeyLength) + KEY_SIZE + RECORD_SIZE;  // + sizeof32(StreamObject::HeaderGroup);
    KerA::Segment::EntryHeader entryHeader(KerA::LOG_ENTRY_TYPE_OBJ, objectLength);
    uint32_t entryHeaderBufferLength = KerA::sizeof32(entryHeader) + entryHeader.getLengthBytes();
    entryHeaderBuffer.get()->append(&entryHeader, KerA::sizeof32(entryHeader));
    entryHeaderBuffer.get()->append(&objectLength, entryHeader.getLengthBytes());

    // The position in the batch buffer where records are appended
    // before submitting a batch we compute the checksum over all records
    uint32_t recordsOffset = KerA::sizeof32(KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
    std::cout << "Overhead per batch, recordsOffset: " << recordsOffset << std::endl;

    KerA::Tub<KerA::Key> key;
    key.construct(streamId, static_cast<void*>(NULL), 0);  // supposed KEY_SIZE==0 no key
    KerA::Tub<KerA::SpinLock::Guard> takeCacheLock;
    KerA::Tub<KerA::SpinLock::Guard> takeStreamletActiveLock;

    uint32_t totalRecords = 0;
    char value[RECORD_SIZE];
    char keyv[KEY_SIZE];
    for (int val = 0; val < RECORD_SIZE; ++val) {
        value[val] = 'x';
    }

    size_t tslength = 0;
    if (KEY_SIZE > 0) {
        memset(keyv, 0, KEY_SIZE);
        tslength = prodids.copy(keyv, prodids.length());
        key.construct(streamId, static_cast<void*>(keyv), KEY_SIZE);
    }

    KerA::Tub<KerA::Buffer> objectBuffer;  // contains entry header + length + object's [keys-value]
    objectBuffer.construct();

    objectBuffer.get()->allocFirstChunkInternal(entryHeaderBufferLength + objectLength);  // contiguous region
    objectBuffer.get()->resetFirstChunk();  // use only batchStream

    objectBuffer.get()->appendCopy(entryHeaderBuffer.get()->getRange(0, entryHeaderBufferLength), entryHeaderBufferLength);

    // uint32_t checksum =
    KerA::StreamObject::appendHeaderKeysAndValueToBuffer(*key.get(), value, RECORD_SIZE, objectBuffer.get(), true);
    uint32_t objectBufferLength = objectBuffer.get()->size();

    // a reference to reusable nextBatch
    uint32_t batchIndexReuse = -1;
    std::string tsnow = "";

    uint32_t prevStreamletId = 0;  // we keep it to load balance records

    MultiWriteGroupBatchObjectE* nextBatch = NULL;
    MultiWriteGroupBatchObjectE* lastBatch = NULL;

    std::vector<MultiWriteGroupBatchObjectE*> lastBatches;  // a hint to avoid querying closed batches by writers
    lastBatches.resize(NSTREAMLETS);

    int count = 0;
    while (!producerStop) {
        batchIndexReuse = -1;
        nextBatch = NULL;
        lastBatch = NULL;

        if (totalRecords >= recordCount) {
            std::cout << "Processed produce_batches()" << std::endl;
            break;
        }

        if (KEY_SIZE > 0) {
            memset(keyv + tslength, 0, KEY_SIZE - tslength);
            // *reinterpret_cast<uint64_t*>(keyv + tslength) = Cycles::toMicroseconds(Cycles::rdtsc());
            tsnow = std::to_string(KerA::Cycles::toMicroseconds(KerA::Cycles::rdtsc()));
            tsnow.copy(keyv+tslength, tsnow.length());
        }

        // check partitionerMode and initialize streamletId
        if (partitionerMode && KEY_SIZE > 0) {
            uint64_t keyPartHash = key.get()->getHash(streamId, keyv, KEY_SIZE);
            streamletId = keyPartHash % NSTREAMLETS + 1;
        } else {
            prevStreamletId = streamletId;
            streamletId = (++streamletId > NSTREAMLETS) ? 1 : streamletId;  // next one
        }

        lastBatch = lastBatches[streamletId-1];  // points to last known

        // if last known batch exists and not closed then
        // take streamlet lock, check still not closed
        // append record if possible
        // release streamlet lock

        if (lastBatch != NULL && !lastBatch->closed) {
            KerA::SpinLock::Guard _(streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]);

            nextBatch = lastBatch;
            // append if possible
            if (!nextBatch->closed) {
                // do the append IF I have space for another record and its overhead todo enable next part
                if ((nextBatch->batchPart->length + objectBufferLength) <= BATCH_SIZE) {
                    // append the record
                    // ======== [entryHeaderBuffer + content] our client buffer looks exactly like what we write on server

                    nextBatch->objects->get()->appendCopy(objectBuffer.get()->getRange(0, objectBufferLength), objectBufferLength);
                    nextBatch->batchPart->length += objectBufferLength;
                    nextBatch->batchPart->countEntries +=1;
                    // nextBatch->batchPart->lengthLastEntry = objectBufferLength;
                    totalRecords++;

                    // close it here if possible assuming records of 100 bytes and more
                    if (BATCH_SIZE - nextBatch->batchPart->length < objectBufferLength) {
                        // compute checksum once for all records
                        KerA::Crc32C crc;
                        crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length-recordsOffset), nextBatch->batchPart->length-recordsOffset);
                        nextBatch->batchPart->header.checksum = crc.getResult();

                        // fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
                        //         recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
                        // fflush(stdout);

                        nextBatch->closed = true;
                        lastBatches[streamletId-1] = NULL;
                    }

                    continue;
                } else {  // no more space, mark this closed;
                    // compute checksum
                    KerA::Crc32C crc;
                    crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length-recordsOffset), nextBatch->batchPart->length-recordsOffset);
                    nextBatch->batchPart->header.checksum = crc.getResult();

                    // fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
                    //         recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
                    // fflush(stdout);

                    nextBatch->closed = true;
                    lastBatches[streamletId-1] = NULL;
                }

                // VERSION pre-fill batch todo disable it - just for testing
                // do the append IF I have space for another record and its overhead
                /*
                while ((nextBatch->batchPart->length + objectBufferLength) <= BATCH_SIZE) {
                    // append the record
                    // ======== [entryHeaderBuffer + content] our client buffer looks exactly like what we write on server
                    nextBatch->objects->get()->append(entryHeaderBuffer.get(), 0, entryHeaderBufferLength);
                    // next also computes a checksum over the entry
                    uint32_t checksum = Object::appendHeaderKeysAndValueToBuffer(*key.get(), value, RECORD_SIZE, nextBatch->objects->get(), false);
                    // update WriteGroupBatchPart
                    nextBatch->objects->get()->append(objectBuffer.get(), 0, objectBufferLength);
                    nextBatch->objects->get()->appendCopy(objectBuffer.get()->getRange(0, objectBufferLength), objectBufferLength);
                    nextBatch->batchPart->length += objectBufferLength;

                    // update entry digest checksum
                    nextBatch->chunkDigest->updateChecksum(checksum); // covers entry's checksum todo enable it
                    nextBatch->chunkDigest->chunk->chunklength += objectBufferLength;
                    nextBatch->chunkDigest->chunk->lengthLastEntry = objectBufferLength;
                    nextBatch->chunkDigest->chunk->countEntries +=1;

                    totalRecords++;

                    // close it here if possible assuming records of 100 bytes and more
                    if (BATCH_SIZE - nextBatch->batchPart->length < objectBufferLength ) {
                        nextBatch->closed = true;
                        break;
                }
                }

                // no more space, mark this closed;
                nextBatch->closed = true;
                continue;
                */
            }
        }

        // if last known batch exists and closed then take and reuse one new batch OR no known batch exists
        if (lastBatch == NULL || lastBatch->closed) {
            // get unused batch index
            if (batchesReuseIndexesCached.size() > 0) {
                batchIndexReuse = batchesReuseIndexesCached.front();
                batchesReuseIndexesCached.pop();
            } else {
                while (batchIndexReuse == -1) {
                    takeCacheLock.construct(cacheLock);  // protects prodBatchesReuseIndexes
                    // should drain prodBatchesReuseIndexes once and use a local cache
                    while (batchesReuseIndexes.size() > 0) {
                        batchesReuseIndexesCached.push(batchesReuseIndexes.front());
                        batchesReuseIndexes.pop();
                    }
                    takeCacheLock.destroy();

                    if (batchesReuseIndexesCached.size() > 0) {
                        batchIndexReuse = batchesReuseIndexesCached.front();
                        batchesReuseIndexesCached.pop();
                    } else {
                        streamletId = prevStreamletId;
                        KerA::Cycles::sleep(500);  // give some time to writers
                        count++;
                    }
                }
            }

            // take a new batch and refresh it
            nextBatch = batchesReuse[batchIndexReuse];
            lastBatches[streamletId-1] = nextBatch;

            // reset nextBatch

            keyHash = (streamletId-1) * tabletRange;
            nextBatch->keyHash = keyHash;
            nextBatch->closed = false;
            nextBatch->objects->get()->resetFirstChunk();
            nextBatch->batchPart = nextBatch->objects->get()->emplaceAppend<KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(0, streamletId);
            nextBatch->batchPart->length = KerA::sizeof32(KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
            nextBatch->batchPart->countEntries = 0;
            nextBatch->numberObjectsAppended = 0;  // this is taken from response, should match nextBatch->batchPart->countEntries
            nextBatch->creationTime = KerA::Cycles::toMicroseconds(KerA::Cycles::rdtsc());

            // Append the record
            nextBatch->objects->get()->appendCopy(objectBuffer.get()->getRange(0, objectBufferLength), objectBufferLength);
            nextBatch->batchPart->length += objectBufferLength;
            // NextBatch->batchPart->lengthLastEntry = objectBufferLength;
            nextBatch->batchPart->countEntries = 1;
            totalRecords++;

            // Give this batch to writers
            KerA::SpinLock::Guard _(streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]);
            streamletBatches[streamletId].push_back(batchIndexReuse);
        }
    }

    while (!writerStop) {
        KerA::Cycles::sleep(1000000);  // give time to writers to finish
    }

    // Release resources
    std::cout << "Cleaning and stopping produce_batches(), count: " << count << std::endl;

    takeCacheLock.construct(cacheLock);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        MultiWriteGroupBatchObjectE* nextBatch = batchesReuse[b];
        nextBatch->objects->get()->reset();
    }

    takeCacheLock.destroy();
    producerStop = false;
    writerStop = false;

    std::cout << "Processed produce_batches()" << std::endl;
}
catch (const KerA::ClientException& e) {
    std::cerr << "KerA exception: " << e.str() << std::endl;
    std::cout << "Cleaning produce_batches()" << std::endl;

    KerA::Tub<KerA::SpinLock::Guard> takeCacheLock;
    takeCacheLock.construct(cacheLock);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        MultiWriteGroupBatchObjectE* nextBatch = batchesReuse[b];
        nextBatch->objects->get()->reset();
    }

    takeCacheLock.destroy();
    producerStop = false;
    writerStop = false;
}
catch (const KerA::Exception& e) {
    std::cerr << "KerA exception: " << e.str() << std::endl;
    std::cout << "Cleaning produce_batches()" << std::endl;

    KerA::Tub<KerA::SpinLock::Guard> takeCacheLock;
    takeCacheLock.construct(cacheLock);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        MultiWriteGroupBatchObjectE* nextBatch = batchesReuse[b];
        nextBatch->objects->get()->reset();
    }

    takeCacheLock.destroy();
    producerStop = false;
    writerStop = false;
}

void write_batches(uint64_t streamId, KerA::Tub<KerA::RamCloud>* ramcloud,
        uint32_t BATCH_SIZE, uint32_t NRECORDS, uint32_t NSTREAMLETS,
        uint32_t NNODES, uint32_t REQUEST_SIZE, uint64_t PRODID, uint64_t recordCount, uint64_t LINGERMS)
try {
    uint32_t totalRecords = 0;

    KerA::Tub<KerA::SpinLock::Guard> takeCacheLock;
    KerA::Tub<KerA::SpinLock::Guard> takeStreamletActiveLock;

    std::map<uint32_t, std::vector<uint32_t>> nodesToStreamletList;
    uint32_t nextNode = 1;

    // for each streamlet, associate the next node
    for (uint32_t streamletId = 1; streamletId <= NSTREAMLETS; streamletId++) {
        if (nextNode > NNODES) {
            nextNode = 1;
        }
        if (!KerA::contains(nodesToStreamletList, nextNode)) {
            nodesToStreamletList[nextNode] = {};
        }

        nodesToStreamletList[nextNode].push_back(streamletId);
        nextNode++;
    }

    int countInFlight = 1;  // todo make it configurable
    uint32_t recordsOffset = KerA::sizeof32(KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);

    std::vector<MultiWriteGroupBatchObjectE*> batches[countInFlight];

    // Allows to fill all then block until 1/2 are ready, then again
    KerA::WriteGroupBatchMultiRpcWrapper* requestsInFlight[countInFlight];
    std::deque<int> requestsIndexes;  // available requestsInFlight
    std::deque<int> inFlightRequests;
    for (int i = 0; i < countInFlight; i++) {
        requestsInFlight[i] = NULL;
        requestsIndexes.push_back(i);
    }

    int nextRequest = -1;
    int countNoRecords = 0;
    std::queue<int> freeBatchIndexes;

    bool exit = false;
    while (!writerStop) {
        if (exit) {
            std::cout << "Processed produce_batches()" << std::endl;
            break;
        }

        // no batches to write
        // make some progress with cached requests
        while (requestsIndexes.size() == 0) {
            // do poll, maybe one request finished
            ramcloud->get()->poll();

            // first check the oldest request
            int requestsInProgress = inFlightRequests.size();
            int currentRequest = -1;

            for (int j = 0; j < requestsInProgress; j++) {
                currentRequest = inFlightRequests.front();
                inFlightRequests.pop_front();
                assert(requestsInFlight[currentRequest] != NULL);

                uint32_t numRequests = batches[currentRequest].size();
                bool requestReady = requestsInFlight[currentRequest]->isReady();
                if (requestReady) {
                    // allow reusing these batches
                    for (size_t i = 0; i < numRequests; i++) {
                        MultiWriteGroupBatchObjectE* batch = batches[currentRequest][i];
                        // each request was processed - STATUS_RETRY handled by MultiOpGroup
                        records += batch->numberObjectsAppended;  // taken from responses
                        freeBatchIndexes.push(batch->streamletIdBatchIndex);
                    }

                    if (records >= recordCount) {
                        exit = true;
                    }

                    batches[currentRequest].clear();
                    delete requestsInFlight[currentRequest];
                    requestsInFlight[currentRequest] = NULL;

                    requestsIndexes.push_back(currentRequest);
                } else {
                    inFlightRequests.push_back(currentRequest);
                }
            }

            // give indexes back once
            KerA::SpinLock::Guard _(cacheLock);
            while (freeBatchIndexes.size() > 0) {
                batchesReuseIndexes.push(freeBatchIndexes.front());
                freeBatchIndexes.pop();
            }
        }

        nextRequest = requestsIndexes.front();
        requestsIndexes.pop_front();

        // take available batches for each streamlet

        // drain up to REQUEST_SIZE per node - considering all streamlets of a node
        // for each streamlet take at least one batch
        // MultiOpGroup has nBatches (requestSize) and nRpcs to launch in parallel
        // if nBatches * batchSize = REQUEST_SIZE and there is 1 node => synchronous RPC
        // on-the-fly requests == 1 for all nodes

        for (uint32_t nodeId = 1; nodeId <= NNODES; nodeId++) {
            uint32_t nodeRequestSize = 0;
            bool nodeFilled = false;

            // next is used to increase the chance to take more than one batch thus reduce locks
            uint32_t countStreamletBatchesPerRequest = (REQUEST_SIZE/BATCH_SIZE)/nodesToStreamletList[nodeId].size();
            uint32_t count = 0;

            for (uint32_t streamletIdIndex = 0; streamletIdIndex < nodesToStreamletList[nodeId].size(); streamletIdIndex++) {
                uint32_t streamletId = nodesToStreamletList[nodeId][streamletIdIndex];

                KerA::SpinLock::Guard _(streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]);

                count = 0;
                while (streamletBatches[streamletId].size() > 0 && count < countStreamletBatchesPerRequest) {
                    count++;
                    uint32_t streamletIdBatchIndex = streamletBatches[streamletId].front();

                    MultiWriteGroupBatchObjectE* nextBatch = batchesReuse[streamletIdBatchIndex];
                    // assert(streamletId == nextBatch->batchPart->streamletId);

                    if (nextBatch->closed || (!nextBatch->closed  // && nextBatch->chunkDigest->chunk->countEntries > 0
                            && KerA::Cycles::toMicroseconds(KerA::Cycles::rdtsc()) - nextBatch->creationTime >= LINGERMS)) {  // 5msec equiv. linger.ms; should have at least one record
                        if (nodeRequestSize + nextBatch->batchPart->length <= REQUEST_SIZE) {
                            nodeRequestSize += nextBatch->batchPart->length;

                            // make sure checksum is computed
                            if (!nextBatch->closed) {
                                // compute checksum
                                KerA::Crc32C crc;
                                crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length - recordsOffset),
                                        nextBatch->batchPart->length - recordsOffset);
                                nextBatch->batchPart->header.checksum = crc.getResult();

                                // fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
                                //         recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
                                // fflush(stdout);
                            }

                            nextBatch->closed = true;
                            // save this index to restore in case of errors
                            nextBatch->streamletIdBatchIndex = streamletIdBatchIndex;
                            batches[nextRequest].push_back(nextBatch);  // batchPart contains streamletId
                            streamletBatches[streamletId].pop_front();  // it means we sent it
                        } else {
                            nodeFilled = true;  // move to next node, this is one is filled already
                            break;  // for
                        }
                    }  // end if closed
                }
            }

            if (!nodeFilled && nodeRequestSize < REQUEST_SIZE) {
                for (uint32_t streamletIdIndex = 0; streamletIdIndex < nodesToStreamletList[nodeId].size(); streamletIdIndex++) {
                    uint32_t streamletId = nodesToStreamletList[nodeId][streamletIdIndex];

                    KerA::SpinLock::Guard _(streamletActiveLocks[streamletId%MAXNUMSTREAMLETS]);

                    if (streamletBatches[streamletId].size() > 0) {
                        uint32_t streamletIdBatchIndex = streamletBatches[streamletId].front();

                        MultiWriteGroupBatchObjectE* nextBatch = batchesReuse[streamletIdBatchIndex];
                        // assert(streamletId == nextBatch->batchPart->streamletId);

                        if (nextBatch->closed || (!nextBatch->closed  // && nextBatch->chunkDigest->chunk->countEntries > 0
                                && KerA::Cycles::toMicroseconds(KerA::Cycles::rdtsc()) - nextBatch->creationTime >= LINGERMS)) {  // 5msec equiv. linger.ms; should have at least one record
                            if (nodeRequestSize + nextBatch->batchPart->length <= REQUEST_SIZE) {
                                nodeRequestSize += nextBatch->batchPart->length;

                                // make sure checksum is computed
                                if (!nextBatch->closed) {
                                    // compute checksum
                                    KerA::Crc32C crc;
                                    crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length-recordsOffset), nextBatch->batchPart->length-recordsOffset);
                                    nextBatch->batchPart->header.checksum = crc.getResult();

                                    // fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
                                    //        recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
                                    // fflush(stdout);
                                }

                                nextBatch->closed = true;
                                // save this index to restore in case of errors
                                nextBatch->streamletIdBatchIndex = streamletIdBatchIndex;
                                batches[nextRequest].push_back(nextBatch);  // batchPart contains streamletId
                                streamletBatches[streamletId].pop_front();  // it means we sent it
                            } else {
                                nodeFilled = true;  // move to next node, this is one is filled already
                                break;  // for
                            }
                        }  // end if batch closed
                    }
                }  // end for
            }
        }

        uint32_t numRequests = batches[nextRequest].size();

        if (numRequests == 0) {
            // do poll, maybe one request finished
            ramcloud->get()->poll();

            // first check the oldest request
            int requestsInProgress = inFlightRequests.size();
            int currentRequest = inFlightRequests.front();

            for (int j = 0; j < requestsInProgress; j++) {
                currentRequest = inFlightRequests.front();
                inFlightRequests.pop_front();
                assert(requestsInFlight[currentRequest] != NULL);

                uint32_t numRequests = batches[currentRequest].size();
                bool requestReady = requestsInFlight[currentRequest]->isReady();
                if (requestReady) {
                    // allow reusing these batches
                    for (size_t i = 0; i < numRequests; i++) {
                        MultiWriteGroupBatchObjectE* batch = batches[currentRequest][i];
                        // each request was processed - STATUS_RETRY handled by MultiOpGroup
                        records += batch->numberObjectsAppended;  // taken from responses
                        freeBatchIndexes.push(batch->streamletIdBatchIndex);
                    }

                    if (records >= recordCount) {
                        exit = true;
                    }

                    batches[currentRequest].clear();
                    delete requestsInFlight[currentRequest];
                    requestsInFlight[currentRequest] = NULL;

                    requestsIndexes.push_back(currentRequest);
                } else {
                    inFlightRequests.push_back(currentRequest);
                }
            }

            // give indexes back once
            takeCacheLock.construct(cacheLock);
            while (freeBatchIndexes.size() > 0) {
                batchesReuseIndexes.push(freeBatchIndexes.front());
                freeBatchIndexes.pop();
            }
            takeCacheLock.destroy();

            requestsIndexes.push_back(nextRequest);  // not used
            // give back the index
            KerA::Cycles::sleep(500);  // give some time to producers
            countNoRecords++;
            continue;
        }

        // throttle, no more than NRECORDS/second
        /*
        stopInsertSec = Cycles::rdtsc();
        double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));

        if (totalRecords >= NRECORDS && time <= 999999) { // check should we throttle
            totalRecords = 0;
            Cycles::sleep(1000000 - time); // up to 1s
            startInsertSec = Cycles::rdtsc();
        }

        if (time > 999999) { // one second passed, reset throttle stuff?
            totalRecords = 0; // maybe we produced less last second, reset counter
            startInsertSec = stopInsertSec;
        }
        */

        std::random_shuffle(batches[nextRequest].begin(), batches[nextRequest].end());

        // startRpcs is called internally
        // reusing requestsInFlight[nextRequest] MultiOpGroup object may bring some benefits todo
        requestsInFlight[nextRequest] = new KerA::WriteGroupBatchMultiRpcWrapper(ramcloud->get(),
                reinterpret_cast<KerA::MultiWriteGroupBatchObject* const *>(&batches[nextRequest][0]), numRequests, streamId, PRODID);

        inFlightRequests.push_back(nextRequest);
    }

    for (int i = 0; i < countInFlight; i++) {
        delete requestsInFlight[i];
    }

    std::cout << "Records misses, countNoRecords: " << countNoRecords << std::endl;
    std::cout << "Processed write_batches()" << std::endl;
}
catch (const KerA::ClientException& e) {
        std::cerr << "RAMCloud exception: " << e.str() << std::endl;
}
catch (const KerA::Exception& e) {
        std::cerr << "RAMCloud exception: " << e.str() << std::endl;
}

int main(int argc, char *argv[])
try {
    if (argc != 14) {
        std::cerr << "Usage: " << argv[0] << " coordinatorLocator RECORD_COUNT BATCH_SIZE RECORD_SIZE KEY_SIZE NRECORDS NSTREAMLETS NNODES REQUEST_SIZE PRODID PARTITIONER LINGERMS" << std::endl;
        return -1;
    }

    // after NRECORDS/s throttle source producer
    // NSTREAMLETS represents the number of streamlets from 1..NSTREAMLETS
    // NNODES represents the number of nodes on which NSTREAMLETS/NNODES streamlets live
    uint16_t KEY_SIZE, PARTITIONER;
    uint32_t BATCH_SIZE, RECORD_SIZE, NRECORDS, NSTREAMLETS, NNODES, REQUEST_SIZE;
    uint64_t recordCount, PRODID, LINGERMS;
    std::stringstream s;
    uint32_t countLogs;
    s << argv[2] << ' ' << argv[3] << ' ' << argv[4] << ' ' << argv[5]
        << ' ' << argv[6] << ' ' << argv[7] << ' ' << argv[8] << ' ' << argv[9] << ' ' << argv[10] << ' ' << argv[11] << ' ' << argv[12] << ' ' << argv[13];
    s >> recordCount >> BATCH_SIZE >> RECORD_SIZE >> KEY_SIZE >> NRECORDS >> NSTREAMLETS >> NNODES >> REQUEST_SIZE >> PRODID >> PARTITIONER >> LINGERMS >> countLogs;
    std::cout << "n.streamlets: " << NSTREAMLETS << std::endl;

    // todo add also the record overhead ~11B if key null
    BATCH_SIZE += KerA::sizeof32(KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
    REQUEST_SIZE += KerA::sizeof32(KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);

    KerA::Tub<KerA::RamCloud> ramcloud;
    // Tub<RamCloud> ramcloudControl;
    ramcloud.construct(argv[1], "test");
    // ramcloudControl.construct(argv[1], "test");
    uint64_t streamId = ramcloud->createTable("table1", NNODES, NSTREAMLETS, countLogs);

    // do sleep
    KerA::Cycles::sleep(1999990);

    // assert(RECORD_SIZE+11 <= BATCH_SIZE); // todo confirm, 11 overhead of record entry
    uint64_t entryId = 0;

    for (size_t i = 0; i < MAXNUMSTREAMLETS; i++)
        streamletActiveLocks[i].setName("streamlet::ActiveLock"+i);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        buffers[b].construct();

        // preparing a batch for batchesReuse
        MultiWriteGroupBatchObjectE* nextBatch =
                new MultiWriteGroupBatchObjectE(&buffers[b], entryId++, -1);  // keyHash==-1, to reset at reuse

        // nextBatch->objects->get()->reset();
        nextBatch->objects->get()->allocFirstChunkInternal(BATCH_SIZE);  // contiguous region
        nextBatch->objects->get()->resetFirstChunk();  // use only batchStream

        nextBatch->batchPart = nextBatch->objects->get()->emplaceAppend<KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(0, 0);

        // update WriteGroupBatchPart - after every object append; initially contains the WriteGroupBatchPart header
        nextBatch->batchPart->length = KerA::sizeof32(KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
        nextBatch->batchPart->streamletId = 0;  // invalid, should be streamletId > 0
        nextBatch->batchPart->countEntries = 0;
        nextBatch->numberObjectsAppended = 0;  // this is taken from response, should match nextBatch->batchPart->countEntries

        batchesReuse[b] = nextBatch;
        batchesReuseIndexes.push(b);
    }

    for (uint32_t streamletId = 1; streamletId <= NSTREAMLETS; streamletId++) {
        streamletBatches[streamletId] = {};
    }

    records = 0;  // how many stream records written
    producerStop = false;
    writerStop = false;
    bool partitionerMode = (PARTITIONER > 0);  // 0 round robin , 1 key hash to streamlet

    // ramcloudControl->serverControlAll(WireFormat::START_PERF_COUNTERS);

    uint64_t startInsertSec = KerA::Cycles::rdtsc();
    uint64_t startInsert = KerA::Cycles::rdtsc();
    uint64_t stopInsertSec = KerA::Cycles::rdtsc();
    uint64_t lastNRecords = records;

    // this thread is responsible to send available batches
    // linger.ms = 0, i.e., we send a set of full batches of records (REQUEST_SIZE/BATCH_SIZE) but no more than REQUEST_SIZE (bytes)
    // once a batch/buffer is sent, it is also cleaned and put back to free buffers
    std::thread twrite(write_batches, streamId, &ramcloud, BATCH_SIZE, NRECORDS, NSTREAMLETS, NNODES, REQUEST_SIZE, PRODID, recordCount, LINGERMS);

    // this thread is the source of the stream, continuously producing batches of records
    std::thread tproduce(produce_batches, streamId, BATCH_SIZE, RECORD_SIZE, KEY_SIZE, NSTREAMLETS, NNODES, partitionerMode, recordCount, PRODID);

    twrite.detach();
    tproduce.detach();

    while (recordCount > lastNRecords) {
        uint64_t prevNRecords = lastNRecords;
        lastNRecords = records;
        stopInsertSec = KerA::Cycles::rdtsc();

        double time = static_cast<double>(
            KerA::Cycles::toMicroseconds(stopInsertSec - startInsertSec)
        );
        std::cout << ">>> last seconds: " << time <<
            ", time: " << KerA::Cycles::toSeconds(stopInsertSec) <<
            ", records: " << lastNRecords - prevNRecords <<
            ", total_records: " << lastNRecords << std::endl;

        // ramcloudControl->serverControlAll(WireFormat::GET_PERF_STATS, NULL, 0, &statsBefore);

        KerA::Cycles::sleep(999990);  // 1000ms

        // ramcloudControl->serverControlAll(WireFormat::GET_PERF_STATS, NULL, 0, &statsAfter);
        // std::cout << PerfStats::printMinClusterStats(&statsBefore, &statsAfter) << std::endl;
    }

    uint64_t stopInsert = KerA::Cycles::rdtsc();
    double time = static_cast<double>(
        KerA::Cycles::toMicroseconds(stopInsert - startInsert)
    );
    std::cout << ">>>>>>>>>>>>> total_last: " << time <<
        ", time: " << KerA::Cycles::toSeconds(stopInsert) <<
        ", records: " << static_cast<double>(records) << std::endl;

    writerStop = true;
    producerStop = true;
    while (producerStop && writerStop) {
        KerA::Cycles::sleep(1000000);
    }

    return 0;
}
catch (const KerA::ClientException& e) {
    std::cerr << "RAMCloud exception: " << e.str() << std::endl;
    return 1;
}
catch (const KerA::Exception& e) {
    std::cerr << "RAMCloud exception: " << e.str() << std::endl;
    return 1;
}
