/* Copyright 2017-2020 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <thread>
#include <list>
#include <iostream>
#include <sstream>
#include <atomic>
#include <algorithm>
#include <deque>
#include <mutex>
#include <condition_variable>

#include "Logger.h"
#include "Common.h"
#include "Cycles.h"
#include "Buffer.h"
#include "StreamObject.h"
#include "ShortMacros.h"
#include "ClientException.h"
#include "MultiReadGroup.h"
#include "GroupMultiRpcWrapper.h"
#include "RamCloud.h"
#include "Util.h"
#include "SegmentIterator.h"
#include "Tub.h"
#include "Segment.h"
#include "WireFormat.h"
#include "LogMetadata.h"
struct GroupSegmentOffset {
    uint32_t streamletId;
    uint64_t groupId;
    uint64_t segmentId;
    uint32_t offset;  // last object position to read from exclusive, ignored if -1
    uint32_t offsetIndex;  // index into cached reusable offsets
    uint32_t pulledRecords;  // n records pulled per segment
    bool segmentProcessed;
};

#define CACHE_SIZE 256
#define MAXNUMSTREAMLETS 128

// next is used to access streamletBatches[streamletId]
KerA::UnnamedSpinLock streamletActiveLocks[MAXNUMSTREAMLETS];  // gives a lock for each streamletId deque

// key=streamletId, value is a list of indexes to batchesReuse; tail is first to be processed
// streamletBatches[streamletId] is protected by streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]
std::map<uint32_t, std::deque<uint32_t>> streamletBatches;

// contains batch objects with objects' content in buffers[i]
// access is protected by streamletActiveLocks[streamletId%MAXNUMSTREAMLETS]
std::array<KerA::MultiReadGroupObject*, CACHE_SIZE> batchesReuse;  // size of CACHE_SIZE

// next is used to access batchesReuseIndexes
KerA::SpinLock cacheLock("batches");  // for batches

// here the consumers push processed object's indexes
// it is used by pullers
// protected by cacheLock
std::deque<uint32_t> batchesReuseIndexes;

std::atomic<uint32_t> records;  // total records pulled and processed until now
std::atomic<bool> pullerStop;
std::atomic<bool> consumerStop;

KerA::Tub<KerA::Buffer> buffers[CACHE_SIZE];
KerA::Tub<KerA::GroupSegmentInfo> batchOffsets[CACHE_SIZE];

KerA::Tub<GroupSegmentOffset> readerOffsets[CACHE_SIZE];
std::deque<uint32_t> readerOffsetsReuseIndexes;

static const uint64_t INVALID_GROUP_ID = -1UL;
static const uint64_t INVALID_SEGMENT_ID = -1UL;
static const uint32_t INVALID_SEGMENT_OFFSET = -1U;

void pull_batches(uint64_t tableId1, KerA::Tub<KerA::RamCloud>* ramcloud, uint64_t recordCount,
        uint32_t BATCH_STREAM, uint64_t readerId, std::vector<uint32_t> streamletIds, uint32_t NSTREAMLETS, uint32_t NACTIVEGROUPS)
try {
    fprintf(stdout, "Starting pull_batches()\n");
    fflush(stdout);

    KerA::Tub<KerA::SpinLock::Guard> takeCacheLock;
    KerA::Tub<KerA::SpinLock::Guard> takeStreamletActiveLock;

  // puller cached last used offsets - used by next requests
    std::unordered_map<uint32_t,  // streamletId
        std::map<uint64_t, GroupSegmentOffset*>  // groupId -> last segment offset/s in order of segmentId
    > streamletIdGroupIdOffsets;  // managing reader offsets

    std::unordered_map<uint32_t,  // streamletId
        std::map<uint64_t, KerA::AvailableSegments*>  // groupId -> first last available segments
    > streamletIdGroupIdNextSegments;  // managing reader offsets - it keeps a list of segment ids for [streamletId, groupId]

    for (uint32_t sindex = 0; sindex < streamletIds.size(); sindex++) {
        streamletIdGroupIdOffsets[streamletIds[sindex]] = {};  // no group, no segment offset
        streamletIdGroupIdNextSegments[streamletIds[sindex]] = {};  // no group, no next segment/s
    }

    uint32_t totalNumberObjectsPulled = 0;
    uint32_t streamletSpan = NSTREAMLETS;  // number of streamlets
    uint64_t tabletRange = 1 + ~0UL / streamletSpan;
    uint64_t keyHash = -1;  // (streamletId-1) * tabletRange;
    const uint32_t maxStreamletFetchBytes = 1000000;  // not used; up to this n bytes per streamlet

    std::unordered_map<uint32_t,  // streamletId
        std::map<uint64_t, uint32_t>  // groupId -> index into batchesReuse
    > streamletIdGroupIdIndexes;  // managing used batch indexes

    uint32_t prevNumberObjectsPulled = 0;
    int countZeroes = 0;
    std::vector<KerA::MultiReadGroupObject*> batches;  // to be pulled

    // std::deque<uint32_t> reqIndexes; //used by next request batches

    while (!pullerStop && totalNumberObjectsPulled < recordCount) {
        prevNumberObjectsPulled = totalNumberObjectsPulled;
        streamletIdGroupIdIndexes.clear();
        countZeroes = 0;

        // we should have at least one batch object for each streamlet's active group
        // each request is guaranteed to have one batch for each streamlet group
        takeCacheLock.construct(cacheLock);  // protects batchesReuseIndexes
        if (batchesReuseIndexes.size() < NACTIVEGROUPS * streamletIds.size()) {
            takeCacheLock.destroy();
            KerA::Cycles::sleep(500);  // give some time to readers
            continue;
        }

        // for (uint32_t bi = 0; bi < NACTIVEGROUPS * streamletIds.size(); bi++) {
        //     uint32_t batchIndex = batchesReuseIndexes.front();
        //     batchesReuseIndexes.pop_front();
        //     reqIndexes.push_back(batchIndex);
        // }
        takeCacheLock.destroy();

        batches.clear();

        // for each streamlet: check and take next available groupId/s - from group-offset cache or query
        for (uint32_t sindex = 0; sindex < streamletIds.size(); sindex++) {
            uint32_t streamletId = streamletIds[sindex];
            keyHash = (streamletId - 1) * tabletRange;

            uint32_t streamletGroupLength = streamletIdGroupIdOffsets[streamletId].size();

            // get next available [NACTIVEGROUPS - streamletGroupLength] groups
            if (NACTIVEGROUPS - streamletGroupLength > 0) {
                KerA::Buffer groupsBuffer;

                (*ramcloud)->getNextAvailableGroupSegmentIds(tableId1,
                        streamletId, readerId, keyHash,
                        NACTIVEGROUPS - streamletGroupLength, &groupsBuffer);

                uint32_t groupRespOffset = 0;
                const KerA::WireFormat::GetNextAvailableGroupSegmentId::Response* groupRespHdr =
                        groupsBuffer.getOffset<KerA::WireFormat::GetNextAvailableGroupSegmentId::Response>(0);
                groupRespOffset += KerA::sizeof32(*groupRespHdr);
                uint32_t discoveredGroups = groupRespHdr->length;

                for (uint32_t i = 0; i < discoveredGroups; i++) {
                    const KerA:: GroupSegmentId* nextGroup = groupsBuffer.getOffset<KerA::GroupSegmentId>(groupRespOffset);
                    // nextGroup gives a groupId and max segmentId -> available segments 1..maxSegmentId
                    uint64_t nextGroupId = nextGroup->groupId;
                    if (!KerA::contains(streamletIdGroupIdOffsets[streamletId], nextGroupId)) {
                        streamletIdGroupIdOffsets[streamletId][nextGroupId] = NULL;  // next offset to be initialized

                        // initialize available segments
                        // lastSegmentId was given to previous rpc multiRead up to maxSegmentId inclusive
                        KerA::AvailableSegments *as = new KerA::AvailableSegments();
                        as->firstSegmentId = 1;
                        as->lastSegmentId = nextGroup->segmentId;  // last available segment
                        streamletIdGroupIdNextSegments[streamletId][nextGroupId] = as;
                    }

                    // RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered groupId %lu", nextGroupId );
                    groupRespOffset += KerA::sizeof32(*nextGroup);
                }
            }

            // for each group obtain next available segment from group-offset cache or query it, update streamletIdGroupIdOffsets
            for (auto it = streamletIdGroupIdOffsets[streamletId].begin(); it != streamletIdGroupIdOffsets[streamletId].end(); ++it) {
                uint64_t groupId = it->first;
                uint64_t segmentId = INVALID_SEGMENT_ID;  // it->second.front()->segmentId;
                uint32_t offset = INVALID_SEGMENT_OFFSET;  // it->second.front()->offset;

                GroupSegmentOffset* segmentInfo = streamletIdGroupIdOffsets[streamletId][groupId];  // if I have a group-offset use it
                if (segmentInfo != NULL) {
                    segmentId = segmentInfo->segmentId;
                    offset = segmentInfo->offset;
                }

                // if segmentId invalid OR
                // segmentId valid and segment processed => query getSegmentsByGroupId
                if (segmentId == INVALID_SEGMENT_ID || (segmentInfo != NULL && segmentInfo->segmentProcessed)) {
                    KerA::AvailableSegments* as = contains(streamletIdGroupIdNextSegments[streamletId], groupId) ? streamletIdGroupIdNextSegments[streamletId][groupId] : NULL;
                    // 4. first check streamletIdGroupIdNextSegments
                    if (as != NULL) {
                        if (as->lastSegmentId >= as->firstSegmentId) {
                            uint64_t nextSegmentId = as->firstSegmentId;
                            as->firstSegmentId += 1;
                            if (as->firstSegmentId > as->lastSegmentId) {
                                streamletIdGroupIdNextSegments[streamletId].erase(groupId);
                            }

                            if (segmentInfo == NULL) {
                                uint32_t nextOffsetIndex = readerOffsetsReuseIndexes.front();
                                segmentInfo = readerOffsets[nextOffsetIndex].get();
                                segmentInfo->offsetIndex = nextOffsetIndex;  // to be pushed back for reuse
                                readerOffsetsReuseIndexes.pop_front();
                                segmentInfo->streamletId = streamletId;
                                segmentInfo->groupId = groupId;
                                streamletIdGroupIdOffsets[streamletId][groupId] = segmentInfo;
                            }
                            segmentInfo->segmentProcessed = false;
                            segmentInfo->pulledRecords = 0;
                            segmentInfo->segmentId = nextSegmentId;
                            segmentInfo->offset = 0;
                            continue;
                        } else {
                            streamletIdGroupIdNextSegments[streamletId].erase(groupId);
                        }
                    }

                    // 5. get next available segments by groupId and its current group-offset segment if one exists
                    bool useGroupOffset = segmentId != INVALID_SEGMENT_ID;
                    KerA::Buffer responseBufferSegments;
                    // should return segments starting from provided offset > lastOffset.segmentId
                    // todo change to return maxSegmentId for given group then use segment&offset to take next available
                    // todo could be nice to have an rpc to query more groups
                    uint32_t groupSegmentsCount = (*ramcloud)->getSegmentsByGroupId(
                            tableId1, streamletId, keyHash, groupId, useGroupOffset,
                            segmentId, offset, -1, &responseBufferSegments);

                    if (groupSegmentsCount == 0) {
                        continue;  // this groupId has no new segments
                    }

                    // 6. identify segments in responseBufferSegments
                    uint32_t respOffset = 0;
                    // first object in the response
                    const KerA::WireFormat::GetSegmentsByGroupId::Response* respHdr =
                            responseBufferSegments.getOffset<KerA::WireFormat::GetSegmentsByGroupId::Response>(respOffset);
                    respOffset = KerA::sizeof32(*respHdr);

                    assert(respHdr->length == groupSegmentsCount);

                    // 7. initialize streamletIdGroupIdOffsets
                    KerA::AvailableSegments *as1 = new KerA::AvailableSegments();  // first last available segments
                    KerA::SegmentIdAndOffset* next = responseBufferSegments.getOffset<KerA::SegmentIdAndOffset>(respOffset);
                    respOffset += KerA::sizeof32(*next);
                    as1->firstSegmentId = next->segmentId;
                    uint64_t nextSegmentId = as1->firstSegmentId;
                    as1->firstSegmentId += 1;
                    next = responseBufferSegments.getOffset<KerA::SegmentIdAndOffset>(respOffset);
                    as1->lastSegmentId = next->segmentId;  // last available segment

                    if (as1->lastSegmentId >= as1->firstSegmentId) {
                        streamletIdGroupIdNextSegments[streamletId][groupId] = as1;
                    }

                    if (segmentInfo == NULL) {
                        uint32_t nextOffsetIndex = readerOffsetsReuseIndexes.front();
                        segmentInfo = readerOffsets[nextOffsetIndex].get();
                        segmentInfo->offsetIndex = nextOffsetIndex;  // to be pushed back for reuse
                        readerOffsetsReuseIndexes.pop_front();
                        segmentInfo->streamletId = streamletId;
                        segmentInfo->groupId = groupId;
                        streamletIdGroupIdOffsets[streamletId][groupId] = segmentInfo;
                    }
                    segmentInfo->segmentProcessed = false;
                    segmentInfo->pulledRecords = 0;
                    segmentInfo->segmentId = nextSegmentId;
                    segmentInfo->offset = 0;
                }

                if (segmentInfo == NULL) {
                    continue;
                }

                // take batch
                // uint32_t batchIndex = reqIndexes.front();
                // reqIndexes.pop_front();
                takeCacheLock.construct(cacheLock);  // protects batchesReuseIndexes
                if (batchesReuseIndexes.size() == 0) {  // we check before to make sure there is one batch for each streamlet
                    takeCacheLock.destroy();
                    continue;
                }
                uint32_t batchIndex = batchesReuseIndexes.front();
                batchesReuseIndexes.pop_front();  // exclusive access
                takeCacheLock.destroy();

                KerA::MultiReadGroupObject* nextBatch = batchesReuse[batchIndex];
                streamletIdGroupIdIndexes[streamletId][groupId] = batchIndex;
                // we push batchIndex to streamletBatches after pulling succeeded

                // do reset nextBatch
                nextBatch->isSegmentClosed = false;
                nextBatch->isGroupProcessed = false;
                nextBatch->nextOffset = 0;
                nextBatch->numberObjectsRead = 0;
                nextBatch->numberOfObjectEntries = 0;
                nextBatch->responseLength = 0;
                nextBatch->responseOffset = 0;
                nextBatch->respvalue->get()->reset();  // done in MultiReadGroup: destroy & reset
                nextBatch->keyHash = keyHash;  // has to reflect streamletId and streamletSpan
                nextBatch->segmentInfo->streamletId = streamletId;
                nextBatch->segmentInfo->groupId = groupId;
                nextBatch->segmentInfo->segmentId = segmentInfo->segmentId;
                nextBatch->segmentInfo->offset = segmentInfo->offset;
                nextBatch->segmentInfo->maxObjects = 100000;
                nextBatch->segmentInfo->maxResponseLength = BATCH_STREAM;  // batch size

                batches.push_back(nextBatch);
            }
        }

        if (batches.size() == 0) {
            KerA::Cycles::sleep(500);
            continue;
        }

        // build request multi-read with head segmentIdAndOffset for each [streamletId,groupId]
        // pull, check segment not closed, every time make sure each streamlet is pulled, else check again for segments/groups
        // always take first group/first segment

        // std::random_shuffle ( batches.begin(), batches.end());
        uint32_t requestsSize = batches.size();

        // currently 1 rpc todo pipelining readers ?order
        (*ramcloud)->multiReadGroup(reinterpret_cast<KerA::MultiReadGroupObject**>(&batches[0]),
                requestsSize, maxStreamletFetchBytes,
                NULL, static_cast<uint32_t>(0), static_cast<uint64_t>(0), static_cast<uint64_t>(0), false);

        // check each object - maybe segment processed or group processed
        for (uint32_t sindex = 0; sindex < requestsSize; sindex++) {
            KerA::MultiReadGroupObject* nextBatch = batches[sindex];
            uint32_t streamletId = nextBatch->segmentInfo->streamletId;
            uint64_t groupId = nextBatch->segmentInfo->groupId;
            GroupSegmentOffset* streamletGroupOffset = streamletIdGroupIdOffsets[streamletId][groupId];

            // keyHash = (streamletId-1) * tabletRange;
            // assert(keyHash == nextBatch->keyHash);
            // assert(groupId == streamletGroupOffset->groupId);
            // assert(streamletGroupOffset->segmentId == nextBatch->segmentInfo->segmentId);

            // 9. get and process objects from response
            totalNumberObjectsPulled += nextBatch->numberObjectsRead;
            streamletGroupOffset->pulledRecords += nextBatch->numberObjectsRead;
            // total number objects read per segment to avoid numberObjectsRead to be 0

            // fprintf(stdout, ">>> Current Group Offset [segmentId-offset-groupId]: %lu %u %lu SegmLength %u Entries %u \n",
            //         nextBatch->segmentInfo->segmentId,
            //         nextBatch->segmentInfo->offset,
            //         groupId,
            //         streamletIdGroupIdOffsets[streamletId][groupId].front()->pulledRecords,
            //         nextBatch->numberOfObjectEntries);
            // if (nextBatch->isSegmentClosed) {
            //     assert(streamletGroupOffset->pulledRecords == nextBatch->numberOfObjectEntries);
            //     assert(nextBatch->numberOfObjectEntries > 0);
            // }

            // 10. condition to exit segment processing: closed and all records pulled
            // mark this segment processed and closed
            // so next step involves another getSegmentsByGroupId query
            streamletGroupOffset->segmentProcessed = nextBatch->isSegmentClosed
                    && (streamletGroupOffset->pulledRecords == nextBatch->numberOfObjectEntries);  // segmentProcessedAndClosed;

            if (nextBatch->numberObjectsRead == static_cast<uint32_t>(0)
                    && !streamletGroupOffset->segmentProcessed) {
                countZeroes++;
                continue;
            }

            // if (streamletGroupOffset->offset > 0 && nextBatch->nextOffset == 0) {  // see Stream multiread
            //     assert(false); //our invalid offset went past segment head
            // }

            // move segment offset to last pulled record's offset
            if (nextBatch->nextOffset > 0) {
                streamletGroupOffset->offset = nextBatch->nextOffset;
            }

            // if segment processed and segment is last in group
            if (streamletGroupOffset->segmentProcessed && nextBatch->isGroupProcessed) {
                // fprintf(stdout, ">>>>>>>>>>>>>groupProcessed [streamletId,groupId] [%lu,%lu] \n", streamletId, groupId);
                // fflush(stdout);

                // remove groupId and give back index
                readerOffsetsReuseIndexes.push_back(streamletGroupOffset->offsetIndex);
                streamletIdGroupIdOffsets[streamletId].erase(groupId);
                streamletIdGroupIdNextSegments[streamletId].erase(groupId);
            }
        }

        if (prevNumberObjectsPulled == totalNumberObjectsPulled) {
            KerA::Cycles::sleep(500);  // wait 1ms do not stress brokers
        }

        // from streamletIdGroupIdIndexes give back indexes to consumers - update streamletBatches!
        for (auto its = streamletIdGroupIdIndexes.begin(); its != streamletIdGroupIdIndexes.end(); ++its) {
            uint32_t streamletId = its->first;
            takeStreamletActiveLock.construct(streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]);

            for (auto it = its->second.begin(); it != its->second.end(); ++it) {
                uint32_t batchIndex = it->second;  // it->first is groupId
                streamletBatches[streamletId].push_back(batchIndex);
            }
            takeStreamletActiveLock.destroy();
        }
    }

    while (!consumerStop) {
        KerA::Cycles::sleep(1000000);  // give time to writers to finish
    }

    pullerStop = false;
    consumerStop = false;

    fprintf(stdout, "Processed pull_batches(): countZeroes=%d\n", countZeroes);
    fflush(stdout);
}
catch (const KerA::ClientException& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fflush(stdout);
}
catch (const KerA::Exception& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fflush(stdout);
}

void consume_batches(uint64_t tableId1, uint64_t recordCount, uint32_t NRECORDS, std::vector<uint32_t> streamletIds, uint32_t NACTIVEGROUPS)
try {
    fprintf(stdout, "Started consume_batches\n");
    fflush(stdout);

    uint32_t totalNumberObjectsRead = 0;
    uint64_t totalCountValueLength = 0;

    // used for throttle
    uint64_t startInsertSec = KerA::Cycles::rdtsc();
    uint64_t stopInsertSec = KerA::Cycles::rdtsc();
    uint32_t totalRecords = 0;

    KerA::Tub<KerA::SpinLock::Guard> takeCacheLock;
    KerA::Tub<KerA::SpinLock::Guard> takeStreamletActiveLock;

    while (!consumerStop) {
        // // throttle, no more than NRECORDS/second
        // stopInsertSec = Cycles::rdtsc();
        // double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));
        //
        // if (totalRecords >= NRECORDS && time <= 999999) {  // check should we throttle
        //     totalRecords = 0;
        //     Cycles::sleep(1000000 - time);  // 100us
        //     startInsertSec = Cycles::rdtsc();
        // }
        // if (time > 999999) {  // one second passed, reset throttle stuff?
        //     totalRecords = 0;  // maybe we produced less last second, reset counter
        //     startInsertSec = stopInsertSec;
        // }

        // take next NACTIVEGROUPS available batches for each streamlet, process them, and release their indexes back to puller
        for (uint32_t sindex = 0; sindex < streamletIds.size(); sindex++) {
            uint32_t streamletId = streamletIds[sindex];

            uint32_t batchesReuseFrontIndex = -1;
            uint32_t activeGroups = NACTIVEGROUPS;
            bool streamletDone = false;

            while (activeGroups > 0 && !streamletDone) {
                KerA::MultiReadGroupObject* nextBatch = NULL;
                activeGroups--;
                takeStreamletActiveLock.construct(streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]);

                if (streamletBatches[streamletId].size() > 0) {
                    batchesReuseFrontIndex = streamletBatches[streamletId].front();
                    nextBatch = batchesReuse[batchesReuseFrontIndex];
                    assert(streamletId == nextBatch->segmentInfo->streamletId);
                    streamletBatches[streamletId].pop_front();  // it means we consumed it
                } else {
                    streamletDone = true;
                }
                takeStreamletActiveLock.destroy();  // let the puller advance

                if (nextBatch != NULL) {
                    uint32_t numberObjectsRead = nextBatch->numberObjectsRead;

                    if (numberObjectsRead != static_cast<uint32_t>(0)) {
                        KerA::Buffer* responseBuffer = nextBatch->respvalue->get();  // shared buffer
                        assert(responseBuffer != NULL);

                        uint32_t respOffset = 0;
                        uint32_t totalLength = *responseBuffer->getOffset<uint32_t>(respOffset);
                        respOffset += KerA::sizeof32(uint32_t);  // length
                        KerA::SegmentCertificate sc;  // take this from responseBuffer as well
                        sc.segmentLength = totalLength;
                        KerA::SegmentIterator it(responseBuffer->getRange(respOffset, totalLength), totalLength, sc);

                        uint32_t iterRecords = 0;
                        KerA::StreamObject obj0(tableId1, *responseBuffer);
                        // uint32_t headerGroupSize = sizeof32(StreamObject::HeaderGroup);

                        while (!it.isDone()) {
                            respOffset += it.getEntryOffset();
                            uint32_t itLength = it.getLength();
                            if (it.getType() != KerA::LOG_ENTRY_TYPE_CHUNK) {
                                obj0.reset(respOffset, itLength);
                                // StreamObject obj0(tableId1, 1, 0, *responseBuffer,
                                //         respOffset + sizeof32(StreamObject::HeaderGroup),
                                //         it.getLength() - sizeof32(StreamObject::HeaderGroup));
                                // fprintf(stdout, ">>> Latency: key=%s now=%lu\n", //value=%s
                                //         string(reinterpret_cast<const char*>(obj0.getKey()),
                                //         obj0.getKeyLength()).c_str(),
                                //         string(reinterpret_cast<const char*>(obj0.getValue()), obj0.getValueLength()).c_str(),
                                //         Cycles::toMicroseconds(Cycles::rdtsc()));
                                totalCountValueLength += obj0.getValueLength();
                                iterRecords++;
                            }

                            respOffset += itLength;
                            it.next();
                        }

                        // assert(iterRecords == numberObjectsRead);
                        records += numberObjectsRead;
                        totalRecords += numberObjectsRead;
                    }

                    // give back nextBatch's index
                    takeCacheLock.construct(cacheLock);
                    batchesReuseIndexes.push_back(batchesReuseFrontIndex);
                    takeCacheLock.destroy();
                }
            }
        }
    }

    fprintf(stdout, "Processed write_batches(): totalValueLength=%lu\n", totalCountValueLength);
    fflush(stdout);

    consumerStop = false;
}
catch (const KerA::ClientException& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fflush(stdout);
}
catch (const KerA::Exception& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fflush(stdout);
}

int main(int argc, char *argv[])
try {
    if (argc != 12) {
        fprintf(stderr,
                "Usage: %s coordinatorLocator RECORD_COUNT BATCH_STREAM NRECORDS NSTREAMLETS NNODES READERID streamletCount clientId NACTIVEGROUPS",
                argv[0]);
        return -1;
    }

    fprintf(stdout, "Started client");

    uint32_t BATCH_STREAM, NRECORDS, NSTREAMLETS, NNODES, streamletCount, clientId, NACTIVEGROUPS;  // NRECORDS used for throttling
    uint64_t recordCount, READERID;
    std::stringstream s;
    uint32_t countLogs;

    s << argv[2] << ' ' << argv[3] << ' ' << argv[4] << ' ' << argv[5] << ' ' << argv[6] << ' ' << argv[7] << ' ' << argv[8] << ' ' << argv[9] << ' ' << argv[10] << ' ' << argv[11];
    s >> recordCount >> BATCH_STREAM >> NRECORDS >> NSTREAMLETS >> NNODES >> READERID >> streamletCount >> clientId >> NACTIVEGROUPS >> countLogs;
    std::cout << "nrecords: " << NRECORDS << '\n';

    // NSTREAMLETS==2, readerdId==1, streamletCount=1 => streamletIds={1}
    // NSTREAMLETS==2, readerdId==2, streamletCount=1 => streamletIds={2}
    // NSTREAMLETS==4, readerdId==1, streamletCount=2 => streamletIds={1,2}
    // NSTREAMLETS==4, readerdId==2, streamletCount=2 => streamletIds={3,4}

    std::vector<uint32_t> streamletIds;

    // NSTREAMLETS==32, readerdId==3, streamletCount=8 => minSID=17, maxSID=24 streamletIds={17..24}
    // NSTREAMLETS==32, readerdId==4, streamletCount=8 => minSID=25, maxSID=32 streamletIds={25..32}

    uint32_t maxStreamletId = streamletCount * clientId;
    uint32_t minStreamletId = maxStreamletId - streamletCount + 1;

    fprintf(stdout, "Streamlet ids: ");
    for (uint32_t streamletId = minStreamletId; streamletId <= maxStreamletId; streamletId++) {
        streamletIds.push_back(streamletId);
        streamletBatches[streamletId] = {};
        fprintf(stdout, " %u, ", streamletId);
    }

    fprintf(stdout, "streamletCount=%u", streamletIds.size());
    fflush(stdout);

    KerA::Tub<KerA::RamCloud> ramcloud;
    ramcloud.construct(argv[1], "test");
    uint64_t tableId1 = ramcloud->createTable("table1", NNODES, NSTREAMLETS, countLogs);

    for (size_t i = 0; i < MAXNUMSTREAMLETS; i++) {
        streamletActiveLocks[i].setName("streamlet::ActiveLock" + i);
    }

    records = 0;

    const uint16_t keysize = 9;
    const uint64_t fakeKeyHash = -1;

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        buffers[b].construct();
        batchOffsets[b].construct();

        // only for cached reader offsets
        readerOffsets[b].construct();
        readerOffsetsReuseIndexes.push_back(b);

        KerA::MultiReadGroupObject* multiReadGroupObject =
                new KerA::MultiReadGroupObject(tableId1, fakeKeyHash, &buffers[b], batchOffsets[b].get());
        multiReadGroupObject->respvalue->get()->reset();

        batchesReuse[b] = multiReadGroupObject;
        batchesReuseIndexes.push_back(b);
    }

    consumerStop = false;
    std::thread tconsume(consume_batches, tableId1, recordCount, NRECORDS,    streamletIds, NACTIVEGROUPS);
    tconsume.detach();

    pullerStop = false;
    std::thread tpull(pull_batches, tableId1, &ramcloud, recordCount, BATCH_STREAM, READERID, streamletIds, NSTREAMLETS, NACTIVEGROUPS);
    tpull.detach();

    uint64_t startInsert = KerA::Cycles::rdtsc();

    uint64_t startInsertSec = KerA::Cycles::rdtsc();
    uint64_t stopInsertSec = KerA::Cycles::rdtsc();

    uint64_t lastNRecords = records;
    while (recordCount > lastNRecords) {
        uint64_t prevNRecords = lastNRecords;
        lastNRecords = records;
        stopInsertSec = KerA::Cycles::rdtsc();
        double time = static_cast<double>(KerA::Cycles::toMicroseconds(stopInsertSec - startInsertSec));

        fprintf(stdout, ">>> last seconds %.0f time %.0f records %lu total_records %lu \n",
                time,
                KerA::Cycles::toSeconds(stopInsertSec),
                lastNRecords - prevNRecords,
                lastNRecords);
        fflush(stdout);

        KerA::Cycles::sleep(999990);  // 1000ms
    }

    uint64_t stopInsert = KerA::Cycles::rdtsc();

    fprintf(stdout, ">>>>>>>>>>>>> total_last %.0f time %.0f records %.0f \n",
            static_cast<double>(KerA::Cycles::toMicroseconds(stopInsert - startInsert)),
            KerA::Cycles::toSeconds(stopInsert),
            static_cast<double>(records));
    fflush(stdout);

    pullerStop = true;
    consumerStop = true;
    while (pullerStop && consumerStop) {
        KerA::Cycles::sleep(500000);  // 500ms
    }

    return 0;
}
catch (const KerA::ClientException& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fflush(stdout);
}
catch (const KerA::Exception& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fflush(stdout);
}
