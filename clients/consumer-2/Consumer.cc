/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <thread>
#include <list>
#include <iostream>
#include <sstream>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "Logger.h"
#include "Common.h"
#include "Cycles.h"
#include "Buffer.h"
#include "StreamObject.h"
#include "ShortMacros.h"
#include "ClientException.h"
#include "MultiReadGroup.h"
#include "MultiWriteGroup.h"
#include "RamCloud.h"
#include "Util.h"
#include "SegmentIterator.h"
#include "Tub.h"
#include "Segment.h"
#include "WireFormat.h"
#include "LogMetadata.h"

#define CACHE_SIZE 1000

std::mutex bmtx;
std::condition_variable cv; //used to announce new batch

std::list<MultiReadGroupObject*> batches;

std::atomic<bool> stop;
std::atomic<uint32_t> records; //total records pulled and processed until now
std::atomic<bool> pullerStop;
std::atomic<bool> consumerStop;

std::list<Tub<Buffer>*> freeBuffers;
Tub<Buffer> buffers[CACHE_SIZE + 1];

static const uint64_t INVALID_GROUP_ID = -1UL;
static const uint64_t INVALID_SEGMENT_ID = -1UL;
static const uint32_t INVALID_SEGMENT_OFFSET = -1U;

void pull_batches(uint64_t tableId1, Tub<RamCloud>* ramcloud,
        uint32_t recordCount, uint32_t BATCH_STREAM)
try {
    std::cout << "==========STARTING pull_batches===============" << std::endl;

    GroupSegmentInfo groupList[1];
    groupList[0].streamletId = 1;  //TODO take this from method signature
    groupList[0].groupId = 1;
    groupList[0].segmentId = 1;
    groupList[0].offset = 0;
    groupList[0].maxObjects = 10000;
    groupList[0].maxResponseLength = BATCH_STREAM;  //~ 10000 objects each 100B+20B overhead

    // Current stream offset to start read from
    SegmentIdAndOffset lastOffset;
    lastOffset.segmentId = INVALID_SEGMENT_ID;
    lastOffset.offset = INVALID_SEGMENT_OFFSET;
    uint32_t START_OFFSET = 0;

    uint32_t totalNumberObjectsPulled = 0;
    const uint16_t keysize = 9;
    uint64_t readerId = 1;  // TODO take this from method signature

    while (!stop && totalNumberObjectsPulled < recordCount)
    {
        // Do not pull more than CACHE_SIZE number of buffers
        {
            std::unique_lock < std::mutex > lck(bmtx);
            if (batches.size() > CACHE_SIZE) {  // CACHE_SIZE batches is the cache limit
                // Sleep 1ms
                lck.unlock();  // let consumers do process some buffers
                //Cycles::sleep(1000);  // 1ms
                continue;
            }
            lck.unlock();
        }

        uint32_t streamletId = 1;  //TODO take this from method signature
        uint32_t streamletSpan = 1;  // number of streamlets
        uint64_t tabletRange = 1 + ~0UL / streamletSpan;
        uint64_t keyHash = (streamletId-1) * tabletRange;

        // 1. get next available groupId; key is used to find the Master responsible of the streamlet
        // TODO create a map of keys to streamlets based on the number of streamlets (ideally we get this from the coordinator once and update it from time to time)
        uint64_t nextGroupId = (*ramcloud)->getNextAvailableGroupId(tableId1, streamletId, readerId, keyHash);

        // 2. if groupId is not valid, wait and try again
        if (INVALID_GROUP_ID == nextGroupId) {
            Cycles::sleep(100000); //100us
            continue;
        }

        // 3. at this point we have a valid group not yet processed
        bool isGroupProcessed = false;
        /*
        fprintf(stdout, ">>>>>>>>>>>>>Discovered groupId %lu \n", nextGroupId);
        fflush(stdout);
        */

        while (!isGroupProcessed) {
            // 4. determine if group offset should be used
            bool useGroupOffset = lastOffset.segmentId != INVALID_SEGMENT_ID;

            // 5. get next available segments by groupId and its current offset
            Buffer responseBufferSegments;
            // Should return segments starting from provided offset >= lastOffset.segmentId
            uint32_t groupSegmentsCount = (*ramcloud)->getSegmentsByGroupId(
                    tableId1, streamletId, keyHash, nextGroupId, useGroupOffset,
                    lastOffset.segmentId, lastOffset.offset, -1,
                    &responseBufferSegments);
            /*
            fprintf(stdout, ">>>>>>>>>>>>>Discovered number of segments %u \n",
                    groupSegmentsCount);
            fflush(stdout);
            */

            // 6. identify segments in responseBufferSegments
            uint32_t respOffset = 0;
            // First object in the response
            const WireFormat::GetSegmentsByGroupId::Response* respHdr =
                    responseBufferSegments.getOffset<WireFormat::GetSegmentsByGroupId::Response>(respOffset);
            respOffset = +sizeof32(*respHdr);

            assert(respHdr->length == groupSegmentsCount);

            // 7. pull data from each available segment until each one is processed
            for (size_t i = 0; i < groupSegmentsCount; i++) {
                const SegmentIdAndOffset* next = responseBufferSegments.getOffset<SegmentIdAndOffset>(respOffset);
                if (next == NULL)
                    break;
                respOffset += sizeof32(*next);
                bool segmentProcessedAndClosed = false;
                uint32_t numberObjectsReadPerSegment = 0;  // total read per segment

                /*
                if (lastOffset.segmentId == next->segmentId ) { //should check also: && lastOffset.offset == next->offset
                    continue;  // ignore this one for now; SegmentManager l#823 includes last offset segment
                }
                */

                lastOffset.segmentId = next->segmentId;
                lastOffset.offset = 0;

                // Do not pull more than CACHE_SIZE number of buffers
                uint32_t currentBatchSize = 0;

                /*
                fprintf(stdout, ">>> Current Group Offset: %lu %u %u \n",
                        lastOffset.segmentId, lastOffset.offset,
                        currentBatchSize);
                */

                while (!segmentProcessedAndClosed)
                {
                    // 8. send request to pull one batch
                    groupList[0].groupId = nextGroupId;
                    groupList[0].segmentId = lastOffset.segmentId;
                    groupList[0].offset = lastOffset.offset;

                    // Try reuse an old buffer
                    Tub<Buffer> *respvalues = NULL;

                    // Do not pull more than CACHE_SIZE number of buffers
                    {
                        std::unique_lock < std::mutex > lck(bmtx);
                        currentBatchSize = batches.size();
                        if (currentBatchSize > CACHE_SIZE) {  // CACHE_SIZE batches is the cache limit
                            lck.unlock();  // let consumers do process some buffers
                            //Cycles::sleep(1000); //1ms
                            continue;
                        }

                        if (freeBuffers.size() > 0) {
                            respvalues = freeBuffers.front();
                            respvalues->get()->reset();
                            freeBuffers.pop_front();
                        } else {
                            lck.unlock();  // let consumers do process some buffers
                            //Cycles::sleep(1000); //1ms
                            continue;
                        }
                    }

                    /*
                    fprintf(stdout,
                            ">>> Current Group Segment Offset: %lu %u %u \n",
                            lastOffset.segmentId, lastOffset.offset,
                            currentBatchSize);
                    fflush(stdout);
                    */

                    uint32_t maxStreamletFetchBytes = 1000000;  //TODO configurable; up to this n bytes per streamlet
                    MultiReadGroupObject* multiReadGroupObject = new MultiReadGroupObject(tableId1, keyHash, respvalues, groupList);
                    MultiReadGroupObject* requests[] = { multiReadGroupObject };

                    uint32_t numResponses = 1;
                    Buffer *rpcResponses[] = {respvalues->get()};
                    (*ramcloud)->multiReadGroup(requests, 1, maxStreamletFetchBytes,
                            rpcResponses, numResponses, static_cast<uint64_t>(0), static_cast<uint64_t>(0), false);

                    // 9. get and process objects from response
                    uint32_t numberObjectsRead = multiReadGroupObject->numberObjectsRead;

                    totalNumberObjectsPulled += numberObjectsRead;
                    numberObjectsReadPerSegment += numberObjectsRead;
                    // 10. condition to exit segment processing: closed and all records pulled
                    segmentProcessedAndClosed = (multiReadGroupObject->isSegmentClosed)
                                    && (numberObjectsReadPerSegment == multiReadGroupObject->numberOfObjectEntries);  // double checking

                    if (numberObjectsRead == static_cast<uint32_t>(0)) {
                        // Sleep .1ms - give some time to producers => config. setting equivalent linger.ms in Kafka
                        Cycles::sleep(5000);  // 5ms
                        if (totalNumberObjectsPulled >= recordCount)
                            break;
                        if (stop)
                            break;
                        /*
                        fprintf(stdout,
                                ">>> numberObjectsRead=0; Current Group Segment Offset: %lu %u "
                                " total segment reads and segment entries %u %u \n",
                                lastOffset.segmentId, lastOffset.offset,
                                numberObjectsReadPerSegment, multiReadGroupObject->numberOfObjectEntries);
                        fflush(stdout);
                        */
                        continue;
                    } else {
                        lastOffset.offset = multiReadGroupObject->nextOffset; //position of last read object in current segment
                        assert(lastOffset.offset != static_cast<uint32_t>(0));

                        // SAVE responseBuffer to batches -> process in consume thread
                        {
                            std::unique_lock < std::mutex > lck(bmtx);
                            batches.push_back(multiReadGroupObject);
                            currentBatchSize = batches.size();
                            lck.unlock();
                            cv.notify_one();
                        }
                    }
                }  // end while - processing one segment

                // 11. a segment was processed, let the store know this and free it

                /*
                fprintf(stdout, "updateGroupOffset %lu %lu %u \n", nextGroupId,
                        lastOffset.segmentId, lastOffset.offset);
                fflush(stdout);
                */

                isGroupProcessed = (*ramcloud)->updateGroupOffset(tableId1,
                        keyHash, streamletId, readerId, nextGroupId, false, -1,
                        -1, -1, -1, lastOffset.segmentId, lastOffset.offset, -1,
                        -1, NULL);

            }  // end for - processing known segments
        }  // end while - processing group

        lastOffset.segmentId = INVALID_SEGMENT_ID;
        lastOffset.offset = INVALID_SEGMENT_OFFSET;
    }

    Cycles::sleep(5000000);  // wait 5s for reader to advance
    {
        std::unique_lock < std::mutex > lck(bmtx);
        std::cout << "======== stopping =======" << std::endl;

        while (batches.size() > 0) {
            MultiReadGroupObject* nextBatch = batches.front();
            nextBatch->respvalue->get()->reset();
            batches.pop_front();
        }
        lck.unlock();
        cv.notify_one(); //in case writer blocked
    }
    pullerStop = true;
    std::cout << "==========done pull_batches===============" << std::endl;
}
catch (const KerA::ClientException& e) {
    std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
}
catch (const KerA::Exception& e) {
    std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
}

void consume_batches(uint64_t tableId1, uint32_t recordCount, uint32_t NRECORDS)
try {
    std::cout << "Started consume_batches" << std::endl;

    uint32_t totalNumberObjectsRead = 0;
    uint64_t totalCountValueLength = 0;

    //used for throttle
    uint64_t startInsertSec = Cycles::rdtsc();
    uint64_t stopInsertSec = Cycles::rdtsc();
    uint32_t totalRecords = 0;

    while (!stop && records < recordCount)
    {
        //throttle, no more than NRECORDS/second
        stopInsertSec = Cycles::rdtsc();
        double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));

        if (totalRecords >= NRECORDS && time <= 999999) { //check should we throttle
            totalRecords = 0;
            Cycles::sleep(1000000 - time); //100us
            startInsertSec = Cycles::rdtsc();
        }
        if (time > 999999) { //one second passed, reset throttle stuff?
            totalRecords = 0; //maybe we produced less last second, reset counter
            startInsertSec = stopInsertSec;
        }
        {
            std::unique_lock < std::mutex > lck(bmtx);
            while (batches.size() == 0)
                cv.wait(lck);

            if (batches.size() == 0) { //perhaps it was freed
                break;
            }
        }

        MultiReadGroupObject* multiReadGroupObject = NULL;
        {
            std::unique_lock < std::mutex > lck(bmtx);
            multiReadGroupObject = batches.front();
        }

        Buffer* responseBuffer = (multiReadGroupObject)->respvalue->get();  // shared buffer
        uint32_t numberObjectsRead = (multiReadGroupObject)->numberObjectsRead;
        assert(responseBuffer != NULL);

        uint32_t respOffset = 0;
        uint32_t totalLength = *responseBuffer->getOffset<uint32_t>(respOffset);
        respOffset += sizeof32(uint32_t);  // length
        SegmentCertificate sc;  // take this from responseBuffer as well
        sc.segmentLength = totalLength;
        SegmentIterator it(responseBuffer->getRange(respOffset, totalLength), totalLength, sc);

        /*
        fprintf(stdout, "Iterating consume_batches one batch\n");
        fflush(stdout);
        */

        uint32_t iterRecords = 0;
        while (!it.isDone()) {
            respOffset += it.getEntryOffset();
            StreamObject obj0(tableId1, 1, 0, *responseBuffer,
                                        respOffset + sizeof32(StreamObject::HeaderGroup),
                                        it.getLength() - sizeof32(StreamObject::HeaderGroup));
//            fprintf(stdout,
//                    ">>> Done request: key=%s value=%s totalValueLength=%lu\n",
//                    string(reinterpret_cast<const char*>(obj0.getKey()),
//                            obj0.getKeyLength()).c_str(),
//                    string(reinterpret_cast<const char*>(obj0.getValue()),
//                            obj0.getValueLength()).c_str(),
//                    totalCountValueLength);
            totalCountValueLength += obj0.getValueLength();
            respOffset += it.getLength();
            iterRecords++;
            it.next();
        }
        assert(iterRecords == numberObjectsRead);
        records += numberObjectsRead;
        totalRecords += numberObjectsRead;

        {
            std::unique_lock < std::mutex > lck(bmtx);
            if (batches.size() == 0 && !stop) //perhaps it was freed
                continue;

            batches.pop_front();
            freeBuffers.push_back((multiReadGroupObject)->respvalue);
        }
    }

    fprintf(stdout,
            "======== exiting write_batches totalValueLength %lu =======\n",
            totalCountValueLength);
    fflush(stdout);
    consumerStop = true;
}
catch (const KerA::ClientException& e) {
    std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
}
catch (const KerA::Exception& e) {
    std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
}

int main(int argc, char *argv[])
try {
    if (argc != 5) {
        std::cerr << "Usage: %s coordinatorLocator RECORD_COUNT BATCH_STREAM NRECORDS" << argv[0];
    }
    std::cout << "Started client" << std::endl;

    uint32_t recordCount, BATCH_STREAM, NRECORDS;
    std::stringstream s;
    s << argv[2] << ' ' << argv[3] << ' ' << argv[4];
    s >> recordCount >> BATCH_STREAM >> NRECORDS;
    std::cout << "nrecords: " << NRECORDS << '\n';

    uint32_t b;
    for (b = 0; b < CACHE_SIZE + 1; b++) {
        buffers[b].construct();
        freeBuffers.push_back(&buffers[b]);
    }

    Tub<RamCloud> ramcloud;
    ramcloud.construct(argv[1], "test");
    uint64_t tableId1 = ramcloud->createTable("table1", 1, 1); //handle getTableId

    records = 0;

    std::thread tconsume(consume_batches, tableId1, recordCount, NRECORDS);
    tconsume.detach();

    std::thread tpull(pull_batches, tableId1, &ramcloud, recordCount, BATCH_STREAM);
    tpull.detach();

    uint64_t startInsert = Cycles::rdtsc();

    uint64_t startInsertSec = Cycles::rdtsc();
    uint64_t stopInsertSec = Cycles::rdtsc();

    while (recordCount > records)
    {
        stopInsertSec = Cycles::rdtsc();

        double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));

        if (time >= 999995) { //one second passed
            startInsertSec = stopInsertSec;
            fprintf(stdout, ">>>>>>>>>>>>>last %.0f seconds time %.0f %.0f records \n",
                    time, Cycles::toSeconds(stopInsertSec), static_cast<double>(records));
            fflush (stdout);
        } else {
            Cycles::sleep(999995); //1s
        }
    }

    uint64_t stopInsert = Cycles::rdtsc();

    fprintf(stdout, ">>>>>>>>>>>>>total_last %.0f time %.0f records %.0f \n",
            static_cast<double>(Cycles::toMicroseconds(stopInsert - startInsert)),
            Cycles::toSeconds(stopInsert),
            static_cast<double>(records));
    fflush (stdout);

    stop = true;
    while (!pullerStop && !consumerStop) {
        Cycles::sleep(500000); //500ms
    }

    std::cout << "======================ENDING reading=================" << std::endl;
    return 0;
}
catch (const KerA::ClientException& e) {
    std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
}
catch (const KerA::Exception& e) {
    std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
}
