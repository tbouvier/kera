/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Cycles.h"
#include "MultiWriteGroupBatchObjectE.h"

using Part = KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart;

MultiWriteGroupBatchObjectE::MultiWriteGroupBatchObjectE(KerA::Tub<KerA::Buffer>* objects,
                        uint64_t entryId,
                        uint64_t keyHash,
                        uint32_t batchSize)
    : MultiWriteGroupBatchObject(objects, entryId, keyHash, 0)
    , closed(false)
    , creationTime(-1)
    , batchPart(NULL)
    , streamletIdBatchIndex(-1)
{
    // this->objects->get()->reset();
    this->objects->get()->allocFirstChunkInternal(batchSize);  // contiguous region
    this->objects->get()->resetFirstChunk();  // use only batchStream

    this->batchPart = objects->get()->emplaceAppend<Part>(0, 0);
    this->batchPart->length = KerA::sizeof32(Part);  // Initially contains the
                                                     // header.

    // Invalid, should be streamletId > 0.
    this->batchPart->streamletId = 0;
    this->batchPart->countEntries = 0;
}

void
MultiWriteGroupBatchObjectE::reset(uint64_t keyHash, uint64_t streamletId)
{
    this->keyHash = keyHash;
    this->closed = false;
    this->objects->get()->resetFirstChunk();

    this->batchPart = this->objects->get()->emplaceAppend<Part>(0, streamletId);
    this->batchPart->length = KerA::sizeof32(Part);
    this->batchPart->countEntries = 0;
    this->numberObjectsAppended = 0;  // This is taken from response, should
                                      // match nextBatch->batchPart->countEntries

    this->creationTime = KerA::Cycles::toMicroseconds(KerA::Cycles::rdtsc());
}
