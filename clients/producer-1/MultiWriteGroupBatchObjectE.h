/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KERA_MULTIWRITEGROUPBATCHOBJECTE_H
#define KERA_MULTIWRITEGROUPBATCHOBJECTE_H

#include <atomic>

#include "MultiWriteGroupBatchObject.h"
#include "WireFormat.h"

struct MultiWriteGroupBatchObjectE : public KerA::MultiWriteGroupBatchObject {
  public:
    MultiWriteGroupBatchObjectE(KerA::Tub<KerA::Buffer>* objects,
                                uint64_t entryId,
                                uint64_t keyHash,
                                uint32_t batchSize);
    ~MultiWriteGroupBatchObjectE() {}

    void reset(uint64_t keyHash, uint64_t streamletId);

    KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart* batchPart;
    std::atomic<bool> closed;
    uint64_t creationTime;  // initialized when first record is appended to
    uint32_t streamletIdBatchIndex;
};

#endif
