/* Copyright 2020-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <thread>
#include <list>
#include <iostream>
#include <sstream>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "Logger.h"
#include "Common.h"
#include "Cycles.h"
#include "Buffer.h"
#include "ObjectBuffer.h"
#include "Object.h"
#include "ShortMacros.h"
#include "ClientException.h"
#include "RamCloud.h"
#include "Util.h"
#include "Tub.h"
#include "Segment.h"
#include "WireFormat.h"
#include "LogMetadata.h"

struct MultiWriteGroupBatchObjectE;

std::list<MultiWriteGroupBatchObjectE*> batches;
std::map<uint64_t, char*> valuesCachedMap;

std::atomic<uint64_t> records;  // Total records written until now
std::atomic<bool> producerStop;
std::atomic<bool> writerStop;

// How much memory the producer can use to batch records before sending them
// = CACHE_SIZE * BATCH_SIZE
#define CACHE_SIZE 10000

KerA::Tub<KerA::Buffer> buffers[CACHE_SIZE + 1];
std::list<uint32_t> freeBuffers;

struct MultiWriteGroupBatchObjectE : public KerA::MultiWriteGroupBatchObject {
    uint32_t recordsCount;
    uint32_t length;

    MultiWriteGroupBatchObjectE(KerA::Tub<KerA::Buffer>* objects, uint64_t entryId, uint64_t keyHash)
        : MultiWriteGroupBatchObject(objects, entryId, keyHash)
        , recordsCount(0)
        , length(0)
    {}

    MultiWriteGroupBatchObjectE()
        : MultiWriteGroupBatchObject()
        , recordsCount()
        , length()
    {}

    MultiWriteGroupBatchObjectE(const MultiWriteGroupBatchObjectE& other)
        : MultiWriteGroupBatchObject(other)
        , recordsCount(other.recordsCount)
        , length(other.length)
    {}

    MultiWriteGroupBatchObjectE& operator=(const MultiWriteGroupBatchObjectE& other) {
        MultiWriteGroupBatchObject::operator =(other);
        recordsCount = other.recordsCount;
        length = other.length;
        return *this;
    }
};

/**
 * streamId
 * BATCH_SIZE in total bytes per batch
 * RECORD_SIZE in bytes record value
 * NSTREAMLETS number of streamlets
 * NNODES number of nodes
 */
void produce_batches(uint64_t streamId, uint32_t BATCH_SIZE,
        uint32_t RECORD_SIZE, uint32_t NSTREAMLETS, uint32_t NNODES)
{
    try
    {
        const uint32_t streamletId = 1;
        uint32_t streamletSpan = NSTREAMLETS;  // Number of streamlets
        uint64_t tabletRange = 1 + ~0UL / streamletSpan;
        uint64_t keyHash = (streamletId-1) * tabletRange;
        uint64_t entryId = 0;
        // Actually key is null but its header overhead is paid anyway.
        KerA::Key primaryKey(streamId, NULL, 0);

        // Total object length including overhead/headers - minus entryHeader
        KerA::ObjectLength objectLength = KerA::sizeof32(KerA::KeyCount) + KerA::sizeof32(KerA::CumulativeKeyLength) + primaryKey.getStringKeyLength() + RECORD_SIZE + KerA::sizeof32(KerA::Object::HeaderGroup);
        KerA::Segment::EntryHeader entryHeader(KerA::LOG_ENTRY_TYPE_OBJ, objectLength);

        KerA::Tub<KerA::Buffer> entryHeaderBuffer; //overhead per record, initialized once here because key/s (size and number) are fixed
        uint32_t entryHeaderBufferLength = KerA::sizeof32(entryHeader) + entryHeader.getLengthBytes();
        entryHeaderBuffer.construct();
        entryHeaderBuffer.get()->append(&entryHeader, KerA::sizeof32(entryHeader));
        entryHeaderBuffer.get()->append(&objectLength, entryHeader.getLengthBytes());

        KerA::Tub<KerA::SpinLock::Guard> takeCacheLock;

        // Create records, arrange them in a batch reference from external queue
        for (uint32_t i = 0; i < CACHE_SIZE; i++)
        {
            MultiWriteGroupBatchObjectE* nextBatch = NULL; //try reuse an old batch

            KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart* batchPart = NULL;
            if (!nextBatch) {  // no previous batch to reuse
                nextBatch = new MultiWriteGroupBatchObjectE(&buffers[freeBuffers.front()], entryId++, keyHash); //use entryId to discover values for reuse
                //buffers[freeBuffers.front()].get()->allocFirstChunkInternal(BATCH_SIZE+1000);
                batchPart =
                        buffers[freeBuffers.front()].get()->emplaceAppend<KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(nextBatch->recordsCount, nextBatch->length, streamletId);
                nextBatch->length += KerA::sizeof32(KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
                freeBuffers.pop_front();
            }

            // Produce a batch - use batchSize in bytes
            uint32_t valueIndex = 0;

            // Produce while I have space for another record and its overhead
            while ((nextBatch->length + entryHeaderBufferLength + objectLength) < BATCH_SIZE)
            {
                char* value = valuesCachedMap[nextBatch->entryId % CACHE_SIZE] + valueIndex;
                //Util::genRandomString(value, RECORD_SIZE);
                for (int val = 0; val < RECORD_SIZE; ++val) {
                    value[val] = 'x';
                }
                valueIndex += RECORD_SIZE;

                //======== [entryHeaderBuffer + content] our client buffer looks exactly like what we write on server
                nextBatch->objects->get()->append(entryHeaderBuffer.get(), 0, entryHeaderBufferLength);
                KerA::Object::appendHeaderKeysAndValueToBuffer(primaryKey, value, RECORD_SIZE, nextBatch->objects->get(), false);
                nextBatch->length += (entryHeaderBufferLength + objectLength);
                nextBatch->recordsCount += 1;
            }

            //update WriteGroupBatchPart
            batchPart->recordsCount = nextBatch->recordsCount;
            batchPart->length = nextBatch->length;

            batches.push_back(nextBatch);
        }

        //release resources

        std::cout << "======== done produce_batches =======" << std::endl;
    }
    catch (const KerA::ClientException& e) {
        std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
        std::cout << "======== cleaning produce_batches =======" << std::endl;
    }
    catch (const KerA::Exception& e) {
        std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
        std::cout << "======== cleaning produce_batches =======" << std::endl;
    }
}

void write_batches(uint64_t streamId, KerA::Tub<KerA::RamCloud>* ramcloud,
        uint32_t BATCH_SIZE, uint32_t NRECORDS, uint32_t NSTREAMLETS,
        uint32_t NNODES, uint32_t REQUEST_SIZE, uint64_t PRODID, uint64_t recordCount)
{
    try {
        std::cout << "========STARTING writes=======" << std::endl;

        //used for throttling
        uint64_t startInsertSec = KerA::Cycles::rdtsc();
        uint64_t stopInsertSec = KerA::Cycles::rdtsc();

        uint32_t totalRecords = 0;

        for (uint32_t i = 0; i < CACHE_SIZE; i++)
        {
            MultiWriteGroupBatchObjectE* batch = NULL;
            batch = batches.front();

            // Throttle, no more than NRECORDS/second
            stopInsertSec = KerA::Cycles::rdtsc();
            double time = static_cast<double>(
                KerA::Cycles::toMicroseconds(stopInsertSec - startInsertSec)
            );

            if (totalRecords >= NRECORDS && time <= 999999) {  // Check should we throttle
                totalRecords = 0;
                KerA::Cycles::sleep(1000000 - time);  // Up to 1s
                startInsertSec = KerA::Cycles::rdtsc();
            }

            if (time > 999999) {  // One second passed, reset throttle stuff?
                totalRecords = 0;  // Maybe we produced less last second, reset counter
                startInsertSec = stopInsertSec;
            }

            // Take next batch to write
            KerA::MultiWriteGroupBatchObject* requests[] = {batch} ;

            (*ramcloud)->multiWriteGroupBatch(requests, 1, streamId, PRODID);
            records += requests[0]->numberObjectsAppended;  // Taken from response
            totalRecords += requests[0]->numberObjectsAppended;

            batches.pop_front();
            batches.push_back(batch);
        }
        std::cout << "======== done write_batches =======" << std::endl;
        writerStop = false;
    }
    catch (const KerA::ClientException& e) {
        std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
    }
    catch (const KerA::Exception& e) {
        std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
    }
}

int main(int argc, char *argv[]) {
    try {
        if (argc != 11) {
            fprintf(stderr,
                    "Usage: %s coordinatorLocator RECORD_COUNT BATCH_SIZE RECORD_SIZE KEY_SIZE NRECORDS NSTREAMLETS NNODES REQUEST_SIZE PRODID",
                    argv[0]);
            return -1;
        }

        //after NRECORDS/s throttle source producer
        //NSTREAMLETS represents the number of streamlets from 1..NSTREAMLETS
        //NNODES represents the number of nodes on which NSTREAMLETS/NNODES streamlets live
        uint32_t BATCH_SIZE, RECORD_SIZE, KEY_SIZE, NRECORDS, NSTREAMLETS, NNODES, REQUEST_SIZE;
        uint64_t recordCount, PRODID;
        std::stringstream s;
        s << argv[2] << ' ' << argv[3] << ' ' << argv[4] << ' ' << argv[5]
        << ' ' << argv[6] << ' ' << argv[7] << ' ' << argv[8] << ' ' << argv[9] << ' ' << argv[10];
        s >> recordCount >> BATCH_SIZE >> RECORD_SIZE >> KEY_SIZE >> NRECORDS >> NSTREAMLETS >> NNODES >> REQUEST_SIZE >> PRODID;
        std::cout << "streamlets: " << NSTREAMLETS << '\n';

        KerA::Tub<KerA::RamCloud> ramcloud;
        ramcloud.construct(argv[1], "test");
        uint64_t streamId = ramcloud->createTable("table1", NNODES, NSTREAMLETS);

        assert(RECORD_SIZE < BATCH_SIZE);
        for (uint32_t b = 0; b < CACHE_SIZE + 1; b++) {
            buffers[b].construct();
            freeBuffers.push_back(b);

            //entryId % CACHE_SIZE => entry for batch
            char* charValues = new char[BATCH_SIZE];
            memset(charValues, 0, BATCH_SIZE);
            valuesCachedMap[b] = charValues;
        }

        records = 0;
        // This thread is the source of the stream, continuously producing batches
        // of records.
        std::thread tproduce(produce_batches, streamId, BATCH_SIZE, RECORD_SIZE, NSTREAMLETS, NNODES);
        tproduce.join();

        /*
        Tub<SpinLock::Guard> takeCacheLock;
        takeCacheLock.construct(cacheLock);
        bool progress = batches.size() <= CACHE_SIZE;
        takeCacheLock.destroy();
        while(progress){
            Cycles::sleep(1000000); //1s
            takeCacheLock.construct(cacheLock);
            progress = batches.size() <= CACHE_SIZE;
            takeCacheLock.destroy();
        }
        */

        writerStop = true;
        // This thread is responsible to send available batches.
        // linger.ms = 0, i.e., we send a set of full batches of records 
        // (REQUEST_SIZE / BATCH_SIZE) but no more than REQUEST_SIZE (bytes).
        // Once a batch/buffer is sent, it is also cleaned and put back to free
        // buffers.
        std::thread twrite(write_batches, streamId, &ramcloud, BATCH_SIZE, NRECORDS, NSTREAMLETS, NNODES, REQUEST_SIZE, PRODID, recordCount);
        twrite.detach();

        //next print throughput
        fprintf(stdout, "======== counting =======\n");
        fflush (stdout);

        uint64_t startInsert = KerA::Cycles::rdtsc();

        uint64_t startInsertSec = KerA::Cycles::rdtsc();
        uint64_t stopInsertSec = KerA::Cycles::rdtsc();

        uint64_t lastNRecords = records;

        int meta = 10;

        while (recordCount > lastNRecords) {
            uint64_t prevNRecords = lastNRecords;
            lastNRecords = records;
            stopInsertSec = KerA::Cycles::rdtsc();
            double time = static_cast<double>(KerA::Cycles::toMicroseconds(stopInsertSec - startInsertSec));
            fprintf(stdout, ">>> last seconds %.0f time %.0f records %lu total_records %lu \n",
                    time, KerA::Cycles::toSeconds(stopInsertSec), lastNRecords-prevNRecords, lastNRecords);

            if (meta == 0) {  // Print every 10 seconds
                fflush(stdout);
                meta = 10;
            } else {
                meta--;
            }
            KerA::Cycles::sleep(999990);  // 1000ms
        }

        uint64_t stopInsert = KerA::Cycles::rdtsc();
        fprintf(stdout, ">>>>>>>>>>>>>total_last %.0f time %.0f records %.0f \n",
                static_cast<double>(KerA::Cycles::toMicroseconds(stopInsert - startInsert)),
                KerA::Cycles::toSeconds(stopInsert),
                static_cast<double>(records));
        fflush(stdout);

        while (writerStop) {
            KerA::Cycles::sleep(1000000); //1000ms
        }

        while (batches.size() > 0) {
            MultiWriteGroupBatchObjectE* nextBatch = batches.front();
            nextBatch->objects->get()->reset();
            batches.pop_front();
        }

        for (uint32_t b = 0; b < CACHE_SIZE + 1; b++) {
            delete[] valuesCachedMap[b];
        }

        return 0;
    }
    catch (KerA::ClientException& e) {
        std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
        return 1;
    }
    catch (KerA::Exception& e) {
        std::cerr << "KerA exception: " << e.str().c_str() << std::endl;
        return 1;
    }
}


//because CACHE_SIZE is 10000, number records streamed <= CACHE_SIZE * BATCH_SIZE / 120 where recordSize=100, record overhead 20

//./obj.streamletGroupsReplicas/TestWriteClient tcp:host=paravance-68.rennes.grid5000.fr,port=11100 30000000 360000 100 0 10000000 1 1 8000000 1
//streamlets: 1
//======== done produce_batches =======
//======== counting =======
//========STARTING writes=======
//>>> last seconds 0 time 9757 records 0 total_records 0
//>>> last seconds 1000033 time 9758 records 4071650 total_records 4071650
//>>> last seconds 2000048 time 9759 records 4056525 total_records 8128175
//>>> last seconds 3000046 time 9760 records 4056525 total_records 12184700
//>>> last seconds 4000045 time 9761 records 4126100 total_records 16310800
//>>> last seconds 5000045 time 9762 records 4126100 total_records 20436900
//>>> last seconds 6000043 time 9763 records 4247100 total_records 24684000
//>>> last seconds 7000040 time 9764 records 4159375 total_records 28843375
//======== done write_batches =======
//>>> last seconds 8000039 time 9765 records 1406625 total_records 30250000
//>>>>>>>>>>>>>total_last 9000037 time 9766 records 30250000

//./obj.streamletGroupsReplicas/TestWriteClient tcp:host=paravance-68.rennes.grid5000.fr,port=11100 20000000 240000 100 0 10000000 1 1 8000000 1
//streamlets: 1
//======== done produce_batches =======
//======== counting =======
//========STARTING writes=======
//>>> last seconds 0 time 9598 records 0 total_records 0
//>>> last seconds 1000028 time 9599 records 4029984 total_records 4029984
//>>> last seconds 2000042 time 9600 records 4034016 total_records 8064000
//>>> last seconds 3000042 time 9601 records 4034016 total_records 12098016
//>>> last seconds 4000039 time 9602 records 4034016 total_records 16132032
//======== done write_batches =======
//>>> last seconds 5000036 time 9603 records 4027968 total_records 20160000
//>>>>>>>>>>>>>total_last 6000035 time 9604 records 20160000

//./obj.streamletGroupsReplicas/TestWriteClient tcp:host=paravance-68.rennes.grid5000.fr,port=11100 10000000 120000 100 0 10000000 1 1 8000000 1
//streamlets: 1
//======== done produce_batches =======
//======== counting =======
//========STARTING writes=======
//>>> last seconds 0 time 9676 records 0 total_records 0
//>>> last seconds 1000046 time 9677 records 2647008 total_records 2647008
//>>> last seconds 2000060 time 9678 records 2569392 total_records 5216400
//>>> last seconds 3000059 time 9679 records 2676240 total_records 7892640
//======== done write_batches =======
//>>> last seconds 4000059 time 9680 records 2187360 total_records 10080000
//>>>>>>>>>>>>>total_last 5000058 time 9681 records 10080000