# Clients

KerA clients are located in this directory. Each client-level code should be located in a sub-directory with a README, a Makefile and optionnally, a Dockerfile.

## Building a client

### Manually

Run the following command from the top-level directory of the KerA source tree:

```bash
make install-clients
```

This command will build all clients located in the `clients` directory. The output binaries will be placed in `install/bin`.

Alternatively, you can run the following commands which will ouput the binaries in `obj.master/clients` (replace `master` with the current git local branch name):

```bash
make -j12
make clients
```

### With Docker

Build the client image from the top-level directory of the KerA source tree:

```bash
docker build -f clients/Dockerfile . --tag=kera-client
```

This image contains all the native clients

## Running a client

### Manually

```bash
./install/bin/<client-name> <parameters>
```

### With Docker

```bash
docker run kera-client <client-name> <parameters>
```

Not working yet:

Go to the KerA top directory,

```bash
scripts/cluster.py [other_runtime_options] --client=<client_test_path>
```

See scripts/cluster.py for [other_runtime_options]. `<client_test_path>` is, for example, `clients/RAM-462/ram462test`.

## Available clients

The following sub-directories contain client code:

```bash
RAM-462/      A client to QP leak problem (bug RAM-462).
producer-1/   A minimal single-threaded producer.
producer-3/   A complete multi-threaded producer.
consumer-3/   A complete multi-threaded consumer.
```
