# ##############################################################################
# 
# Builder stage
#
# ##############################################################################

FROM debian:buster AS builder

WORKDIR /opt
COPY . /opt/kera

#
# Install dependencies
#

RUN apt-get update \
 && apt-get install --yes \
      apt-transport-https \
      build-essential \
      ca-certificates \
      curl \
      doxygen \
      g++ \
      gdb \
      git \
      libboost-filesystem-dev \
      libboost-program-options-dev \
      libboost-system-dev \
      libibverbs-dev \
      libpcre++-dev \
      libssl-dev \
      libzookeeper-mt-dev \
      procps \
      protobuf-compiler \
      python3 \
      python3-pip \
      software-properties-common \
      unzip \
      wget \
      zip \
 && rm -rf /var/lib/apt/lists/*

#
# Install Python
#

# Make python3 the default python binary
RUN ln -s /usr/bin/python3 /usr/bin/python \
 && ln -s /usr/bin/pip3 /usr/bin/pip

#
# Install cmake
#

#RUN wget https://cmake.org/files/v3.11/cmake-3.11.1.tar.gz \
# && tar -xvf cmake-3.11.1.tar.gz && mv cmake-3.11.1 cmake \
# && cd cmake \
# && ./configure --prefix=/opt/cmake/ \
# && make

RUN curl -sSL https://cmake.org/files/v3.11/cmake-3.11.1-Linux-x86_64.tar.gz | tar -xzC . \
 && mv cmake-3.11.1-Linux-x86_64 cmake

ENV PATH="/opt/cmake/bin/:$PATH"

#
# Compile KerArrow
#

RUN cd /opt/kera/kerarrow \
 && mkdir -p cpp/release \
 && cd cpp/release/ \
 && cmake .. -DCMAKE_BUILD_TYPE=Release -DARROW_PLASMA=on \
 && make -j12 \
 && make install

#
# Compile KerA
#

RUN cd /opt/kera \
 && make -j12 \
 && make install-clients

# ##############################################################################
# 
# Final stage
# No JDK is included in the final image.
#
# ##############################################################################

FROM debian:buster

WORKDIR /opt/kera

#
# Installing binaries
#

COPY --from=builder \
 /opt/kera/install/bin /opt/kera/install/bin

#
# Installing libraries
#
 
COPY --from=builder \
 /opt/kera/kerarrow/cpp/release/release/lib* \
 /opt/kera/install/lib/arrow/

RUN cp /opt/kera/install/lib/arrow/lib* /usr/local/lib/

COPY --from=builder \
 /usr/lib/x86_64-linux-gnu/libzookeeper* \
 /usr/lib/x86_64-linux-gnu/libboost* \
 /usr/lib/x86_64-linux-gnu/libprotobuf* \
 /usr/lib/x86_64-linux-gnu/libpcre* \
 /usr/lib/x86_64-linux-gnu/libssl* \
 /usr/lib/x86_64-linux-gnu/libcrypto* \
 /usr/lib/x86_64-linux-gnu/

RUN mkdir /logs

ENV LD_LIBRARY_PATH="/usr/local/lib/:$LD_LIBRARY_PATH"

RUN ldd /opt/kera/install/bin/producer-1 \
 && ldd /opt/kera/install/bin/producer-3 \
 && ldd /opt/kera/install/bin/consumer-3

COPY --from=builder /opt/kera/clients/docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
