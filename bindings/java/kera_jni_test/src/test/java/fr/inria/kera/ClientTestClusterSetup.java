/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2014 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package fr.inria.kera.test;

import fr.inria.kera.*;
import fr.inria.kera.multiop.*;

import java.lang.reflect.*;
import org.testng.annotations.*;

import static org.testng.AssertJUnit.*;

import java.nio.ByteBuffer;

/**
 * Class that holds methods to create and destroy the test cluster
 * used for the unit tests.
 * 
 * run this as
 * 
 * ./gradlew -Dorg.gradle.daemon=true test --info --tests *ClientTestClusterSetup.createDeleteTable
 */
public class ClientTestClusterSetup {
	public static RAMCloud ramcloud;
	public static TestCluster cluster;
	private long tableId;
	private String key;

	@BeforeSuite
	public void setupClient() {
		System.out.println("--------------------------1");
		cluster = new TestCluster();
		ClientTestClusterSetup.ramcloud =
				new RAMCloud(cluster.getRamcloudClientPointer());
		System.out.println("--------------------------2");
	}

	@AfterSuite
	public void cleanUpClient() {
		//cluster.destroy();
	}

	@Test
	public void createDeleteTable() {
		System.out.println("--------------------------3");
		long tableId = ramcloud.createTable("testTable");
		assertEquals(tableId, 1);
		long readId = ramcloud.getTableId("testTable");
		assertEquals(tableId, readId);
		ramcloud.dropTable("testTable");
		System.out.println("--------------------------4");
	}

	@Test
	public void getRejectRulesBytes() {
		System.out.println("--------------------------5");
		for (int i = 0; i <= 2; i++) {
			RejectRules rules = new RejectRules();
			rules.setGivenVersion(i);
			byte[] expected = {(byte)i,0,0,0,0,0,0,0,0,0,0,0};
			assertArrayEquals(expected, RAMCloud.getRejectRulesBytes(rules));
			rules.rejectIfDoesntExist(true);
			expected[8] = 1;
			assertArrayEquals(expected, RAMCloud.getRejectRulesBytes(rules));
			rules.rejectIfExists(true);
			expected[9] = 1;
			assertArrayEquals(expected, RAMCloud.getRejectRulesBytes(rules));
			rules.rejectIfVersionLeGiven(true);
			expected[10] = 1;
			assertArrayEquals(expected, RAMCloud.getRejectRulesBytes(rules));
			rules.rejectIfVersionNeGiven(true);
			expected[11] = 1;
			assertArrayEquals(expected, RAMCloud.getRejectRulesBytes(rules));
		}
		System.out.println("--------------------------6");
	}

	@Test
	public void writeRequest() {
		System.out.println("--------------------------7");
		ByteBuffer buffer = ByteBuffer.allocateDirect(100);
		byte[] key = "This is the key".getBytes();
		byte[] value = "This is the value".getBytes();
		MultiWriteObject obj = new MultiWriteObject(1, key, value);
		MultiWriteHandler handler = new MultiWriteHandler(buffer, 0, 0);
		Boolean success = (Boolean) invoke(
				handler, "writeRequest",
				new Class[] {ByteBuffer.class, MultiWriteObject.class},
				buffer, obj);
		assertTrue(success);
		assertEquals(key.length + value.length + 26, buffer.position());
		buffer.rewind();
		assertEquals(1, buffer.getLong());

		assertEquals(key.length, buffer.getShort());
		byte[] keyCheck = new byte[key.length];
		buffer.get(keyCheck);
		assertArrayEquals(key, keyCheck);

		assertEquals(value.length, buffer.getInt());
		byte[] valueCheck = new byte[value.length];
		buffer.get(valueCheck);
		assertArrayEquals(value, valueCheck);
		System.out.println("--------------------------8");
	}

	/**
	 * Invoke the specified method on the target object through reflection.
	 *
	 * @param object
	 *      The object to invoke the method on.
	 * @param name
	 *      The name of the method to invoke.
	 * @oaran argClasses
	 *      The classes of the arguments to the method
	 * @param args
	 *      The arguments to invoke with.
	 * @return The result of the invoked method.
	 */
	public static Object invoke(Object object, String name, Class[] argClasses,
			Object... args) {
		Object out = null;
		try {
			Method method = object.getClass().getDeclaredMethod(name, argClasses);
			method.setAccessible(true);
			out = method.invoke(object, args);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return out;
	}
}
