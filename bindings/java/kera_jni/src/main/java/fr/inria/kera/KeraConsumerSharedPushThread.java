/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * KERA fetcher
 * 
 */
public class KeraConsumerSharedPushThread extends Thread {

    static {
        // Load C++ shared library for JNI
        Util.loadLibrary("kera_jni");
    }

	/**
	 * The handover of data and exceptions between the consumer thread and the
	 * task thread
	 */
	private final Handover handover;
	private final Properties keraProperties;
	private final List<Streamlet> subscribedStreamlets;

	/** records throughput per second */
	private final int rt;

	/** Flag to mark the main work loop as alive */
	private volatile boolean running;

	public KeraConsumerSharedPushThread(Handover handover, List<Streamlet> subscribedStreamlets, 
			int rt, Properties keraProperties) {
		super("KeraConsumerSharedPushThread");
		setDaemon(true);
		this.handover = checkNotNull(handover);
		this.subscribedStreamlets = checkNotNull(subscribedStreamlets);
		this.rt = rt;
		this.keraProperties = checkNotNull(keraProperties);
		this.running = true;
	}

	private long clusterHandle;
	
	public KeraConsumerSharedPushThread(Handover handover, List<Streamlet> subscribedStreamlets, 
			int rt, Properties keraProperties, long clusterHandle) {
		super("KeraConsumerSharedPushThread");
		setDaemon(true);
		this.handover = checkNotNull(handover);
		this.subscribedStreamlets = checkNotNull(subscribedStreamlets);
		this.rt = rt;
		this.keraProperties = checkNotNull(keraProperties);
		this.running = true;
		this.clusterHandle = clusterHandle;
	}
	
	@Override
	public void run() {
		// early exit check
		if (!running) {
			return;
		}

		//because I need another handover to share local cache with the actual fetcher
		final Handover handover = this.handover; //between C pull thread and this consumer
		
		String locator = keraProperties.getProperty("LOCATOR");
		String clusterName = keraProperties.getProperty("CLUSTER_NAME");

		KeraBindingsSharedPush bindings = new KeraBindingsSharedPush(this.clusterHandle, handover); //for testing
		
		// start Kera Consumer - C++ thread - to actually pull records and fill the handover with
		KeraHandoverSharedPushPullThread khpt = new KeraHandoverSharedPushPullThread(bindings, 
				this.subscribedStreamlets, this.keraProperties, true);
		khpt.start();
		
		// the latest bulk of records. may carry across the loop if the thread is
		// woken up
		// from blocking on the handover
		ConsumerRecords<byte[], byte[]> records = null;
				
		// main fetch-emit loop
		int count = 0;
		long before = new Date().getTime();
		long now = new Date().getTime();
		try {
			while (running) {		
	
				//todo offset async commit - see flink-kafka strategy
				
				//todo this handover pollNext should give a set of records with their streamlet
				records = handover.pollNext();
				//todo - copy handover data in a local handoverCache/Queue ?
				
				//for each streamlet
				for(Streamlet s:this.subscribedStreamlets) {
					count += records.records(s).size();
				}
								
				if(count >= rt) {
					now = new Date().getTime();
					System.out.println("done " + count + " in " + (now-before));
					before = now;
					count = 0;
				}
				
				//todo emit see Flink Kafka Fetcher
		//				for(ConsumerRecord<byte[],byte[]> rec:records.records(myStreamlet)) {
		//				}
			}
		} catch (Handover.WakeupException e) {
			// fall through the loop
		} catch (Throwable t) {
			// let the main thread know and exit
			// it may be that this exception comes because the main thread closed
			// the handover, in
			// which case the below reporting is irrelevant, but does not hurt
			// either
			handover.reportError(t);
		} finally {
			// make sure the handover is closed if it is not already closed or has
			// an error
			handover.close();

			// todo make sure the KeraConsumer (C thread) is closed - do this in the
			// handover - passing through the shared buffer an object SHUTDOWN that is read by puller
		}
		now = new Date().getTime();
		System.out.println("done " + count + " in " + (now-before));

		// on a clean exit, wait for the runner thread
		try {
			khpt.join();
		}
		catch (InterruptedException e) {
			// may be the result of a wake-up interruption after an exception.
			// we ignore this here and only restore the interruption state
			Thread.currentThread().interrupt();
		}
	}

	public void shutdown() {
		running = false;
		handover.wakeupProducer();
		handover.close();
	}
	
	public static <T> T checkNotNull(T reference) {
		if (reference == null) {
			throw new NullPointerException();
		}
		return reference;
	}
}
