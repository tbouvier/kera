/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import java.util.List;
import java.util.Properties;

/**
 * The thread that runs the {@link Kera Consumer}, connecting to the brokers and
 * polling records. The thread pushes the data into a {@link KeraHandover} to be
 * picked up by the fetcher that will deserialize and emit the records.
 * 
 */
public class KeraHandoverPullThread extends Thread {

    static {
        // Load C++ shared library for JNI
        Util.loadLibrary("kera_jni");
    }
    
	/**
	 * The handover of data and exceptions between the consumer thread and the
	 * task thread
	 */
	private final KeraBindings bindings;
	private final Properties keraProperties;
	private final List<Streamlet> subscribedStreamlets;

	public KeraHandoverPullThread(KeraBindings bindings, List<Streamlet> subscribedStreamlets, Properties keraProperties) {
		super("KeraHandoverPullThread");
		setDaemon(true);
		this.bindings = checkNotNull(bindings);
		this.subscribedStreamlets = checkNotNull(subscribedStreamlets);
		this.keraProperties = checkNotNull(keraProperties);
	}

	@Override
	public void run() {
		// this call blocks: KeraHandoverPullThread is a wrapper to main C thread that plays with handover
		bindings.startMultiReadPullThread(keraProperties, subscribedStreamlets);
	}

	public static <T> T checkNotNull(T reference) {
		if (reference == null) {
			throw new NullPointerException();
		}
		return reference;
	}
}
