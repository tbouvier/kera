/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 package fr.inria.kera;

import java.io.Serializable;

public final class Streamlet implements Serializable {
	private int hash = 0;
	private final String stream;
	private final int streamletId;
	private final boolean assigned; //used by local:true if part of index 

    public Streamlet(String stream, int streamletId) 
    {
		this.stream = stream;
		this.streamletId = streamletId;
		this.assigned = true;
    }

    public Streamlet(String stream, int streamletId, boolean assigned) 
    {
		this.stream = stream;
		this.streamletId = streamletId;
		this.assigned = assigned;
    }
    
    public int hashCode() {
        if (this.hash != 0) {
            return this.hash;
        } else {
            final int prime = 31;
			int result = 1;
			result = prime * result + streamletId;
			result = prime * result + ((stream == null) ? 0 : stream.hashCode());
			this.hash = result;
			return result;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        } else {
        	Streamlet other = (Streamlet)obj;
            if (this.streamletId != other.streamletId) {
                return false;
            } else {
                if (this.stream == null) {
                    if (other.stream != null) {
                        return false;
                    }
                } else if (!this.stream.equals(other.stream)) {
                    return false;
                }
                return true;
            }
        }
    }
    
    public String stream() { return this.stream; }
    public int streamletId() { return this.streamletId; }
    public boolean isAssigned() { return this.assigned; }

    public String toString() {
        return this.stream() + "-" + this.streamletId + "-" + this.assigned;
    }
}
