/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 package fr.inria.kera.multiop;

import java.nio.ByteBuffer;

/**
 * A class that implements the Java bindings to the multi-read operation.
 */
public class MultiReadGroupHandler extends MultiOpGroupHandler<MultiReadGroupObject> {
    /**
     * Constructs a MultiReadHandler object
     */
    public MultiReadGroupHandler(ByteBuffer byteBuffer,
                            long byteBufferPointer,
                            long ramcloudClusterHandle) {
        super(byteBuffer, byteBufferPointer, ramcloudClusterHandle);
        setBatchLimit(200);
    }

    @Override
    protected boolean writeRequest(ByteBuffer buffer, MultiReadGroupObject request) {
        byte[] key = request.getKeyBytes();
        if (buffer.position() + 10 + key.length >= buffer.capacity()) {
            return false;
        }
        //follow ReadGroupPart
        
        buffer.putLong(request.getTableId())
        		.putLong(request.getGroupSegmentInfo().getGroupId())
				.putLong(request.getGroupSegmentInfo().getSegmentId())
				.putInt(request.getGroupSegmentInfo().getOffset())
				.putInt(request.getGroupSegmentInfo().getMaxObjects())
				.putInt(request.getGroupSegmentInfo().getMaxResponseLength())				
                .putShort((short) key.length)
                .put(key);
        return true;
    }

    @Override
    protected void readResponse(ByteBuffer buffer, MultiReadGroupObject response) {
        long version = buffer.getLong();
        response.setVersion(version);
        int valueLength = buffer.getInt();
        byte[] value = new byte[valueLength];
        buffer.get(value);
        response.setValueBytes(value);
    }

    @Override
    protected void callCppHandle(long byteBufferPointer) {
        cppMultiReadGroup(byteBufferPointer);
    }
}
