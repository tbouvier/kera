/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

/**
 * Base class multi op. It encapsulates the entire object, including the key,
 * value.
 *
 * It mostly exists because Java doesn't support primitive out parameters or
 * multiple return values, and we don't know the object's size ahead of time, so
 * passing in a fixed-length array would be problematic.
 */
public class KeraObject {
    /**
     * The key of this object as a byte array.
     */
    private byte[] key;
    /**
     * The value of this object as a byte array.
     */
    private byte[] value;

    /**
     * Constructs a new KeraObject.
     *
     * @param key
     *            The key of the object, a variable length byte array.
     * @param value
     *            The value of the object, a variable length byte array.
     */
    public KeraObject(byte[] key, byte[] value) {
        this.key = key;
        this.value = value;
    }

    /**
     * KeraObject default contructor.
     */
    public KeraObject() {
    }

    /**
     * Get the key of the object.
     *
     * @return The key of the object as a byte array.
     */
    public byte[] getKeyBytes() {
        return key;
    }

    /**
     * Set the key of the object.
     *
     * @param key
     *      The key of the object as a byte array.
     */
    public void setKeyBytes(byte[] key) {
        this.key = key;
    }

    /**
     * Get the value of the object.
     *
     * @return The value of the object as a byte array.
     */
    public byte[] getValueBytes() {
        return value;
    }

    /**
     * Set the value of the object.
     *
     * @param value
     *      The value of the object as a byte array.
     */
    public void setValueBytes(byte[] value) {
        this.value = value;
    }

    /**
     * Get the key of the object.
     *
     * @return The key of the object as a String.
     */
    public String getKey() {
        return new String(key);
    }

    /**
     * Set the key of the object.
     *
     * @param key
     *      The key of the object as a String.
     */
    public void setKey(String key) {
        this.key = key.getBytes();
    }

    /**
     * Get the value of the object.
     *
     * @return The value of the object as a String.
     */
    public String getValue() {
        return new String(value);
    }

    /**
     * Set the value of the object.
     *
     * @param value
     *      The value of the object as a String.
     */
    public void setValue(String value) {
        this.value = value.getBytes();
    }

    @Override
    public String toString() {
        return String.format("CloudObject[key: %s, value: %s]",
                getKey(), getValue());
    }
}
