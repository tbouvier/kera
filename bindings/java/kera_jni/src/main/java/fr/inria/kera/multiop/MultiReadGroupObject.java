/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera.multiop;

import fr.inria.kera.*;

/**
 * RAMCloudObject used for multi-readGroup operations.
 */
public class MultiReadGroupObject extends MultiOpObject {
	
	private int numberObjectsRead; //init in response
	private GroupSegmentInfo groupSegmentInfo;
	private int nextOffset; //init in response
	private boolean isSegmentClosed; //init in response
	private int numberOfObjectEntries; //init in response
			
    /**
     * Constructor for multi-readGroup requests.
     *
     * @param tableId
     *      The ID of the table to find this object in.
     * @param key
     *      The key used to determine the Segment's Master.
     * @param groupSegmentInfo
     */
    public MultiReadGroupObject(long tableId, byte[] key, GroupSegmentInfo groupSegmentInfo) {
        super(tableId, key, null, -1L, Status.STATUS_OK);
        this.groupSegmentInfo = groupSegmentInfo;
    }
    
	public int getNumberObjectsRead() {
		return this.numberObjectsRead;
	}
	
	public void setNumberObjectsRead(int numberObjectsRead) {
		this.numberObjectsRead = numberObjectsRead;
	}
		
	public GroupSegmentInfo getGroupSegmentInfo() {
		return this.groupSegmentInfo;
	}
	
	public void setGroupSegmentInfo(GroupSegmentInfo groupSegmentInfo) {
		this.groupSegmentInfo = groupSegmentInfo;
	}
	
	public int getNextOffset() {
		return this.nextOffset;
	}
	
	public void setNextOffset(int nextOffset) {
		this.nextOffset = nextOffset;
	}
	
	public boolean getIsSegmentClosed() {
		return this.isSegmentClosed;
	}
	
	public void setIsSegmentClosed(boolean isSegmentClosed) {
		this.isSegmentClosed = isSegmentClosed;
	}
	
	public int getNumberOfObjectEntries() {
		return this.numberOfObjectEntries;
	}
	
	public void setNumberOfObjectEntries(int numberOfObjectEntries) {
		this.numberOfObjectEntries = numberOfObjectEntries;
	}

}
