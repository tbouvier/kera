/**
 * KerA Java bindings build script v3
 *
 * https://blog.gradle.org/introducing-the-new-cpp-plugins
 * https://docs.gradle.org/current/userguide/building_cpp_projects.html
 * https://github.com/eskatos/jni-library-sample
 * https://github.com/gradle/native-samples/blob/master/cpp/library-with-tests/build.gradle
 */

import java.io.*
import org.gradle.internal.jvm.Jvm

plugins {
    `cpp-library`
    `java-library`
}

val keraDir = "../../.."
val keraSrc = "src"

val currentDir: String? by project

val jniImplementation by configurations.creating

configurations.matching {
    it.name.startsWith("cppCompile") ||
        it.name.startsWith("nativeLink") ||
        it.name.startsWith("nativeRuntime")
}.all {
    extendsFrom(jniImplementation)
}

tasks.register<Copy>("copyHeaders") {
    from(
        "$keraDir/$keraSrc",
        "$keraDir/${findKeraBin()}",
        "$keraDir/kerarrow/cpp/src",
        "$keraDir/gtest/include"
    )
    include("**/*.h")
    into("$buildDir/headers")
}

tasks.withType<CppCompile>().configureEach {
    dependsOn("copyHeaders")
}

val jniHeaderDirectory = layout.buildDirectory.dir("jniHeaders")

tasks.compileJava {
    outputs.dir(jniHeaderDirectory)
    options.compilerArgumentProviders.add(CommandLineArgumentProvider {
        listOf("-h", jniHeaderDirectory.get().asFile.canonicalPath)
    })
}

library {
    // Private headers are stored in src/main/headers.
    // Public headers are stored build/headers.
    publicHeaders.from("$buildDir/headers")

    binaries.configureEach {
        compileTask.get().dependsOn(tasks.compileJava)

        compileTask.get().compilerArgs.addAll(compileTask.get().toolChain.map {
            if (it is Gcc || it is Clang) listOf("--std=c++11", "-g", "-msse4.2")
            else emptyList()
        })

        compileTask.get().compilerArgs.addAll(jniHeaderDirectory.map {
            listOf("-I", it.asFile.canonicalPath)
        })

        val javaHome = Jvm.current().javaHome.canonicalPath
        compileTask.get().compilerArgs.addAll(compileTask.get().targetPlatform.map {
            listOf("-I", "$javaHome/include") + when {
                it.operatingSystem.isMacOsX -> listOf("-I", "$javaHome/include/darwin")
                it.operatingSystem.isLinux -> listOf("-I", "$javaHome/include/linux")
                else -> emptyList()
            }
        })
    }
}

// Gradle populates library.binaries in afterEvaluate, so we can't access it earlier
afterEvaluate {
    library.binaries.get()
        .filterIsInstance<CppSharedLibrary>()
        .forEach { binary ->
            binary.linkTask.get().linkerArgs.add("-L../../../obj.${getCurrentGitBranch()}")
            binary.linkTask.get().linkerArgs.add("-lkera")
        }
}

tasks.jar {
    from(library.developmentBinary.flatMap {
        (it as CppSharedLibrary).linkFile
    })
}

fun findKeraBin(): String {
    val currentBranch = getCurrentGitBranch()

    if (currentDir == null) {
        if (currentBranch.equals("HEAD")) return "obj"
        return "obj.$currentBranch"
    } else {
        if (currentBranch.equals("HEAD")) return "$currentDir/obj"
        return "$currentDir/obj.$currentBranch"
    }
}

fun getCurrentGitBranch(): String? {
    return executeCommand("git rev-parse --abbrev-ref HEAD")[0]
}

fun executeCommand(command: String): List<String> {
    val proc = Runtime.getRuntime().exec(command)
    val lines = proc.inputStream.bufferedReader().use(BufferedReader::readLines)
    proc.waitFor()
    return lines
}
