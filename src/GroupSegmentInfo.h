/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_GROUPSEGMENTINFO_H
#define RAMCLOUD_GROUPSEGMENTINFO_H

#include "Minimal.h"

namespace KerA {

struct GroupSegmentInfo
{
    uint32_t streamletId;
    uint64_t groupId;
    uint64_t segmentId;
    uint32_t offset;                // last object position to read from
                                    // exclusive, ignored if -1
    uint32_t maxObjects;            // pull max number of objects from this
                                    // segment, ignored if 0
    uint32_t maxResponseLength;     // used in combination with maxObjects,
                                    // whichever is first reached; == batchSize
};

}

#endif