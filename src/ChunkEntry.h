/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_CHUNKENTRY_H
#define RAMCLOUD_CHUNKENTRY_H

#include "Common.h"
#include "Crc32C.h"

namespace KerA {

/**
 * describes a Chunk batch of records, needed by virtual log
 * checksum is computed on client and verified on server
 *
 */
class ChunkEntry {
  public:
	ChunkEntry(uint64_t producerId, uint64_t streamId);

    Crc32C::ResultType getChecksum() {
    		return header.checksum;
    }

    /// next params are initialized or updated by producer client
    uint64_t producerId;

    /// updated by producer client after every record appended to Chunk buffer
    /// how many entries
    uint32_t countEntries;
    /// what follows this ChunkEntry in the objects buffer given by producer
    uint32_t chunklength;
    /// used to determine offset of last valid entry of this chunk
    //uint32_t lengthLastEntry;

    uint64_t streamId;
    /// streamId is part of the virtual segment
    /// as given by producer to define chunk offset
    uint32_t streamletId;
    /// next params are updated after append
    uint64_t groupId;
    /// logical segment id
    uint64_t segmentId;

    /**
     * When serialized in a buffer, the log digest starts with this header.
     */
    struct Header {
        Crc32C::ResultType checksum;
    } __attribute__((__packed__));

    /// Copy of the current header. Updated for each Chunk Record
    Header header;
    
	void init(ChunkEntry* orig);

    DISALLOW_COPY_AND_ASSIGN(ChunkEntry);
};

} // namespace KerA

#endif // RAMCLOUD_CHUNKENTRY_H
