/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "WriteGroupBatchMultiRpcWrapper.h"
#include "Object.h"
#include "ShortMacros.h"

namespace KerA {

/**
 * Constructor for MultiWrite objects: initiates one or more RPCs for a
 * multiWrite operation, but returns once the RPCs have been initiated,
 * without waiting for any of them to complete.
 *
 * \param ramcloud
 *      The RAMCloud object that governs this operation.
 * \param requests
 *      Each element in this array describes one object to write.
 * \param numRequests
 *      Number of elements in \c requests.
 */
WriteGroupBatchMultiRpcWrapper::WriteGroupBatchMultiRpcWrapper(RamCloud* ramcloud,
                        MultiWriteGroupBatchObject* const requests[],
                        uint32_t numRequests,
                        uint64_t tableId,
                        uint64_t producerId)
    : GroupMultiRpcWrapper(ramcloud,
                   type,
                   reinterpret_cast<MultiOpGroupObject* const *>(requests),
                   numRequests,
                   tableId,
                   producerId)
{
    startRpcs();
}

/**
 * Append a given MultiWriteObject to a buffer.
 *
 * It is the responsibility of the caller to ensure that the
 * MultiOpObject passed in is actually a MultiWriteObject.
 *
 * \param request
 *      MultiWriteObject request to append
 * \param buf
 *      Buffer to append to
 */
void
WriteGroupBatchMultiRpcWrapper::appendRequest(MultiOpGroupObject* request,
                                              Buffer* buf)
{
    MultiWriteGroupBatchObject* req = reinterpret_cast<MultiWriteGroupBatchObject*>(request);

    // Add the current objects to the list of those being written by this RPC.
    //!moved WriteGroupBatchPart to client producing this batch - so we have a single chunk in req->objects->get()
    buf->append(req->objects->get(), 0, req->objects->get()->size()); //creates a chunk for external copy if size larger than 500B
}

/**
 * Read the MultiWrite response in the buffer given an offset
 * and put the response into a MultiWriteObject. This modifies
 * the offset as necessary and checks for missing data.
 *
 * It is the responsibility of the caller to ensure that the
 * MultiOpObject passed in is actually a MultiWriteObject.
 *
 * \param request
 *      MultiWriteObject where the interpreted response goes
 * \param buf
 *      Buffer to read the response from
 * \param respOffset
 *      Offset into the buffer for the current position
 *              which will be modified as this method reads.
 *
 * \return
 *      true if there is missing data
 */
bool
WriteGroupBatchMultiRpcWrapper::readResponse(MultiOpGroupObject* request,
                                             Buffer* buf,
                                             uint32_t* respOffset)
{
	MultiWriteGroupBatchObject* req =
            reinterpret_cast<MultiWriteGroupBatchObject*>(request);

    const WireFormat::MultiOpGroup::Response::WriteGroupBatchPart* part =
        buf->getOffset<WireFormat::MultiOpGroup::Response::WriteGroupBatchPart>(*respOffset);
    if (part == NULL) {
        TEST_LOG("missing Response::Part");
        return true;
    }
    *respOffset += sizeof32(*part);
    req->status = part->status;
    req->numberObjectsAppended = part->numberObjectsAppended;
    return false;
}

} // end KerA

