/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2011-2016 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "TestUtil.h"
#include "MacAddress.h"

namespace KerA {

TEST(MacAddressTest, constructorRaw) {
    uint8_t raw[] = {0xde, 0xad, 0xbe, 0xef, 0x98, 0x76};
    EXPECT_EQ("de:ad:be:ef:98:76",
              MacAddress(raw).toString());
}

TEST(MacAddressTest, constructorString) {
    EXPECT_EQ("de:ad:be:ef:98:76",
              MacAddress("de:ad:be:ef:98:76").toString());
}

TEST(MacAddressTest, constructorRandom) {
    for (uint32_t i = 0; i < 100; ++i) {
        MacAddress r1(MacAddress::RANDOM);
        // make sure it's a locally administered unicast address
        EXPECT_EQ(2, r1.address[0] & 0x03);
        MacAddress r2(MacAddress::RANDOM);
        EXPECT_NE(r1.toString(),
                  r2.toString());
    }
}

TEST(MacAddressTest, toString) {
    // tested sufficiently in constructor tests
}

TEST(MacAddressTest, isNull) {
    uint8_t raw[] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
    MacAddress r(MacAddress::RANDOM);
    EXPECT_TRUE(MacAddress(raw).isNull());
    EXPECT_FALSE(r.isNull());
}

}  // namespace KerA
