/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_CHUNKENTRYDIGEST_H
#define RAMCLOUD_CHUNKENTRYDIGEST_H

#include "Common.h"
#include "Crc32C.h"
#include "ChunkEntry.h"
//#include "Segment.h"

namespace KerA {

class Segment;

/// Exception thrown when deserializing a corrupt digest.
struct ChunkEntryDigestException : public Exception {
	ChunkEntryDigestException(const CodeLocation& where, std::string msg)
            : Exception(where, msg) {}
};

/**
 * describes a Chunk batch of records, needed by virtual log
 * checksum is computed on client and verified on server
 *
 */
struct ChunkEntryDigest {
//  public:

	ChunkEntryDigest() :
		    physicalSegmentId(0UL),
			offset(0),
			length(0)
	{
	}

    inline void
    appendToBuffer(Buffer& buffer)
    {
		buffer.appendCopy(&physicalSegmentId, sizeof32(physicalSegmentId));
		buffer.appendCopy(&offset, sizeof32(offset));
		buffer.appendCopy(&length, sizeof32(length));
    }

    //this way the physical segment pointer can be invalidated or updated
    //when the physical segment moves from memory to disk and back
    uint64_t physicalSegmentId; //see VirtualSegment#segmentsMap to obtain a segment reference

    //next are updated in Segment::copyInHeadFromBuffer
    uint32_t offset; //points into physical segment
    uint32_t length; //including SegmentHeader&length&chunk&entries

//    DISALLOW_COPY_AND_ASSIGN(ChunkEntryDigest);
} __attribute__((packed));

struct CEDSegment {

	CEDSegment() :
		    ced(),
			chunk(NULL)
	{
	}

	inline void reset() {
		chunk = NULL;
		ced.offset = 0;
		ced.length = 0;
		ced.physicalSegmentId = 0UL;
	}

	ChunkEntryDigest ced;
	ChunkEntry* chunk;
};

struct CEDChunkEntry{
	ChunkEntryDigest* ced;
	ChunkEntry* chunk;
};

struct CEDOffset{
	CEDOffset(ChunkEntry* c) :
		streamletId(c->streamletId),
		groupId(c->groupId),
		segmentId(c->segmentId)
	{
	}
	    uint32_t streamletId;
	    uint64_t groupId;
	    uint64_t segmentId;

};

} // namespace KerA

#endif // RAMCLOUD_CHUNKENTRYDIGEST_H
