/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIREMOVEOBJECT_H
#define RAMCLOUD_MULTIREMOVEOBJECT_H

#include "Minimal.h"
#include "MultiOpObject.h"
#include "RejectRules.h"

namespace KerA {

/**
 * Objects of this class are used to pass parameters into \c multiRemove.
 */
struct MultiRemoveObject : public MultiOpObject {

    /**
     * The RejectRules specify when conditional removes should be aborted.
     */
    const RejectRules* rejectRules;

    /**
     * The version number of the object just before removal is returned here.
     */
    uint64_t version;

    MultiRemoveObject(uint64_t tableId, const void* key, uint16_t keyLength,
                      const RejectRules* rejectRules = NULL)
        : MultiOpObject(tableId, key, keyLength)
        , rejectRules(rejectRules)
        , version()
    {}

    MultiRemoveObject()
        : rejectRules()
        , version()
    {}

    MultiRemoveObject(const MultiRemoveObject& other)
        : MultiOpObject(other)
        , rejectRules(other.rejectRules)
        , version(other.version)
    {}

    MultiRemoveObject& operator=(const MultiRemoveObject& other) {
        MultiOpObject::operator =(other);
        rejectRules = other.rejectRules;
        version = other.version;
        return *this;
    }
};

}

#endif