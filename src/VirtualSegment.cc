/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2009-2015 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "Common.h"
#include "BitOps.h"
#include "Crc32C.h"
#include "CycleCounter.h"
#include "VirtualSegment.h"
#include "ShortMacros.h"
#include "LogEntryTypes.h"
#include "TestLog.h"
#include "SegmentManager.h"

namespace KerA {

//1st entry: each VS contains a header with {masterId, logId, streamId, logicalSegmentId, segmentSize}
//2nd entry: each VS contains a digest with {prevSegmentId}
//
//next entries are ChunkEntryDigest with its chunk (set of records)
//segment checksum covers ChunkEntryDigest's chunk entry and its checksum result

/**
 * Construct a segment using Segment::DEFAULT_SEGMENT_SIZE bytes dynamically
 * allocated on the heap. This constructor is useful, for instance, when a
 * temporary segment is needed to move data between servers.
 */
VirtualSegment::VirtualSegment(uint64_t masterId, uint64_t logId,
		uint64_t streamId, uint64_t segmentId, uint32_t segmentSize,
		LogSegment* vlogHead, VirtualSegment* prevHead, SegmentManager* segmentManager)
    : logId(logId),
	  streamId(streamId),
	  prevHead(prevHead),
	  replicatedSegment(NULL),
	  segmentManager(segmentManager),
	  syncedLength(0),
	  vlogHead(vlogHead),
	  vlogHeadOffset(0), //initialized at first addChunk
	  vlogHeadLength(0),
	  vlogNextHead(NULL),
	  vlogNextHeadOffset(0),
	  chunksCount(0),
	  chunkLock("VirtualSegment::chunkLock"),
	  segmentsHeadMap(),
//	  durableChunkIndex(0),
	  durableChunkOffset(0),
      closedCommitted(false),
      closed(false),
	  syncToHead(false),
	  openLen(0),
	  id(segmentId),
	  segmentSize(segmentSize),
      head(0),
      checksum()
{

	//add header
	VirtualSegmentHeader header(masterId, logId, streamId, segmentId, segmentSize);

	uint32_t headerLength = sizeof32(VirtualSegmentHeader); //36B
	Segment::EntryHeader entryHeader(LOG_ENTRY_TYPE_SEGHEADER, headerLength);
	//entry header
	vlogHead->appendCopy(&entryHeader);
	head += sizeof32(entryHeader);
	checksum.update(&entryHeader, sizeof(entryHeader));
	//entry length
	vlogHead->appendCopy(&headerLength);
	head += sizeof(headerLength);
	checksum.update(&headerLength, sizeof(headerLength));
	//actual segment header
	vlogHead->appendCopy(&header);
	head += headerLength;

	VirtualLogDigest digest;

	if(prevHead != NULL) {
		digest.addSegmentId(prevHead->id);
	}
	digest.addSegmentId(id);

	//add digest
    uint32_t length = sizeof32(digest);

	Segment::EntryHeader entryHeaderL(LOG_ENTRY_TYPE_LOGDIGEST, length);
	//entry header
	vlogHead->appendCopy(&entryHeaderL);
	head += sizeof32(entryHeaderL);
	checksum.update(&entryHeaderL, sizeof(entryHeaderL));
	//entry length
	vlogHead->appendCopy(&length);
	head += sizeof(length);
	checksum.update(&length, sizeof(length));
	//actual segment header
	vlogHead->appendCopy(&digest);
	head += length;

	openLen = head; //openLen points to offset of first chunk
	durableChunkOffset = openLen;
	vlogHeadOffset = openLen; //some vlogs may not have yet a chunk but still ready/synced

}

//todo move vlogHead->appendChunk in Log, replace with updateHeadAndChecksum

void
VirtualSegment::addChunk(CEDSegment* chunkS, uint32_t lastHeadOffset, LogSegment* newHead)
{
	Crc32C::ResultType chunkChecksum = chunkS->chunk->header.checksum;
	checksum.update(&chunkChecksum, sizeof32(chunkChecksum));
	ChunkEntryDigest* chunk = &chunkS->ced;

	//chunk's buffer only gets accessed after getAppendedLength is called with Log#appendLock which also protects us here
	if (NULL == newHead) {
		if (vlogNextHeadOffset > 0) { //we actually have a new head
//			uint32_t offset =
			vlogNextHead->appendCopy(chunk);
//			fprintf(stdout, ">>> addChunk nextHead %u %u %u %u %u \n", offset, unsigned(vlogHeadOffset),
//					unsigned(vlogHeadLength), chunksCount * sizeof32(ChunkEntryDigest), unsigned(vlogNextHeadOffset));
//			fflush (stdout);
		} else {
			//Log::appendChunk makes sure there is enough space for this chunk
			uint32_t offset = vlogHead->appendCopy(chunk);

//			fprintf(stdout, ">>> addChunk next head %u %u %u \n", offset, unsigned(vlogHeadOffset), (chunksCount+1) * sizeof32(ChunkEntryDigest));
//			fflush (stdout);

			if (vlogHeadOffset == openLen) {
				vlogHeadOffset = offset; //at vlogHeadOffset-openLen we have the header&digest of this virtual segment
//				fprintf(stdout, ">>> addChunk firstHead %u %u %u %u %u \n", offset, unsigned(vlogHeadOffset),
//						unsigned(vlogHeadLength), chunksCount * sizeof32(ChunkEntryDigest), unsigned(vlogNextHeadOffset));
//				fflush (stdout);
			}
		}
	} else {
		vlogNextHead = newHead;
		uint32_t offset = vlogNextHead->appendCopy(chunk);
		assert(vlogNextHeadOffset == 0);
		if (vlogNextHeadOffset == 0) { //not yet initialized
			vlogNextHeadOffset = offset;
		}
//		fprintf(stdout, ">>> addChunk next head %u %u %u \n", unsigned(vlogHeadOffset), lastHeadOffset, (chunksCount+1) * sizeof32(ChunkEntryDigest));
//		fflush (stdout);

		assert (lastHeadOffset - vlogHeadOffset == chunksCount * sizeof32(ChunkEntryDigest));
		vlogHeadLength = chunksCount * sizeof32(ChunkEntryDigest); // starting at vlogHeadOffset in vlogHead
	}
	chunksCount++;
	head += chunk->length; //how much was virtually added to this vSegment

//	fprintf(stdout, ">>> chunksCount and head %u %u %u %u \n", chunksCount, unsigned(head), unsigned(vlogHeadOffset), unsigned(vlogNextHeadOffset));
//	fflush (stdout);

}

/**
 * Check whether or not the segment has sufficient space to append a
 * given number of bytes. This method is used when the caller already
 * has all the bytes required for one or many log entries.
 *
 * \param length
 *      Number of bytes that need to be appended to this segment
 * \return
 *      True if the segment has enough space to fit 'length' number of bytes,,
 *      false otherwise.
 */
bool
VirtualSegment::hasSpaceFor(uint32_t length)
{
//	Segment::EntryHeader entryHeader(LOG_ENTRY_TYPE_INVALID, length);
//	uint32_t totalBytesNeeded = sizeof32(Segment::EntryHeader) + entryHeader.getLengthBytes() + length;

	//since ChunkEntryDigest gives the buffer length with entries already filled
	return length <= getFreeSpace();
}

uint32_t
VirtualSegment::getFreeSpace()
{
	 return closed ? 0 : (segmentSize - head);
}

/**
 * Close the segment, making it permanently immutable. Closing it will cause all
 * future append operations to fail.
 *
 * Note that this is only soft state. Neither the contents of the segment, nor
 * the certificate indicate closure. Backups have their own notion of closed
 * segments, which is propagated by the ReplicatedSegment class.
 */
void
VirtualSegment::close()
{
    closed = true;
}

/**
 * Return the total number of bytes appended to the virtual segment. Calling this method
 * before and after an append will indicate exactly how many bytes were consumed
 * in storing the appended entry, including metadata.
 *
 * A SegmentCertificate which can be used to validate the integrity of the
 * segment's metadata is optionally passed back by value in the 'certificate'
 * parameter.  A copy must be done since the certificate will change on the
 * next append operation.
 *
 * This method is mostly used by ReplicatedSegment to find out how much data
 * needs to be replicated and to provide backups with a means of verifying the
 * metadata integrity of segments and step their replicated version from one
 * consistent snapshot of the segment to another as more entries are appended.
 *
 * \param[out] certificate
 *      The certificate entry will be copied out here.
 * \return
 *      The total number of bytes appended to the segment.
 */
uint32_t
VirtualSegment::getAppendedLength(SegmentCertificate* certificate)
{
	//protected by Log::appendLock (needed when virtual segment is open)
    if (certificate != NULL) {
    	// no need to protect from multi writers calling addChunk 
    	//r/w take appendLock on Log before reaching this point

        certificate->segmentLength = head;
        Crc32C certificateChecksum = checksum;
        certificateChecksum.update(
            certificate, static_cast<unsigned>
            (sizeof(*certificate) - sizeof(certificate->checksum)));
        certificate->checksum = certificateChecksum.getResult();

    }
    return head;
}
//todo rename chunkIndex into chunkOffset, rename lastChunkOffset into lastChunkLength
//chunkIndex gives offset into chunksBuffer for last replicated ChunkEntryDigest
//each replica invokes appendToBuffer, providing *lastChunkOffset, *chunkIndex
//protected by Log syncLock
void
VirtualSegment::appendToBuffer(Buffer& buffer, uint32_t offset, uint32_t length,
		uint32_t* chunkIndex, uint32_t* lastChunkOffset)
{
	//lastChunkOffset always points to the next chunk to be replicated,
	//unless replica needs to go back a few chunks due to backup errors
	//check continuous offset wrt lastChunkOffset
	assert(chunkIndex != NULL && lastChunkOffset != NULL);

	if(offset > *lastChunkOffset) { //ok, each replica manages its lastChunkOffset hint
		throw FatalError(HERE, "Offset should not skip chunks, replicatedSegment in error");
	}

//    LOG(NOTICE, "appendToBuffer offset %u length %u size %u ", offset, length, buffer.size());
	uint32_t offsetAndLength = offset + length;

	//there is still a chance for one of the physical segments to be durably closed then released by PM
	//this method may be retried in case of errors on normal case replication
	//however, only syncTo is responsible to update physical segments' durable head parameter
	//no risk to lose a physical segment reference on normal case replication since they are released only if the virtual segment is durably closed
	//on recovery we check the invalid flag and invalidate local flags for durably close this virtual segment
	// after missing segments are fetched they cannot be released
	//unless the virtual segment is durably closed again

	if(offset >= openLen) {
//		assert(chunksCount*sizeof32(ChunkEntryDigest) >= *chunkIndex);
		//replicating chunks up to offset+length
		// lastChunkOffset represents its chunk's head
		// and it's a hint to avoid iterating all chunks

		// determine the chunk from where the append should happen
		// check if we should go back to a previous chunk - this path is not tested todo:
		//on recovery/re-replicate we have to reset state todo
		if(offset < *lastChunkOffset) {
			fprintf(stdout, ">>> repl failed offset lastChunkOffset %u %u time \n", offset, *lastChunkOffset);
			fflush (stdout);

			// previous replica write probably failed
			// going back through chunks until lastChunkOffset - chunk->length == offset
			//safe to access chunksBuffer after getAppendedLength called with Log#appendLock
			while(*lastChunkOffset>openLen && *chunkIndex > 0) {
				//go back one chunk entry
				//chunkIndex is offset to next to be replicated chunk - may be invalid
				*chunkIndex -= sizeof32(ChunkEntryDigest);

				if (*chunkIndex + vlogHeadOffset + sizeof32(ChunkEntryDigest) < segmentSize) {
					ChunkEntryDigest ced = vlogHead->getAtOffset(*chunkIndex + vlogHeadOffset);
					*lastChunkOffset -= ced.length;
				} else { //move to nextHead
					ChunkEntryDigest ced = vlogNextHead->getAtOffset(*chunkIndex + vlogNextHeadOffset - vlogHeadLength);
					*lastChunkOffset -= ced.length;
				}
				if(*lastChunkOffset == offset || *chunkIndex == 0) {
					break; //found first chunk to append
				}
			}
		}
		if(*lastChunkOffset != offset) {
			throw FatalError(HERE, "Chunks should be atomically replicated");
		}

		//do appends starting from chunkIndex inclusive
		//go forward through chunks until lastChunkOffset + chunk->length == offset + length

		Segment* segment = NULL;
		segmentsHeadMap.clear();

		while(*lastChunkOffset < offsetAndLength) {
			//safe to access chunksBuffer after getAppendedLength called with Log#appendLock
			if (*chunkIndex + vlogHeadOffset + sizeof32(ChunkEntryDigest) < segmentSize) {
				ChunkEntryDigest ced = vlogHead->getAtOffset(*chunkIndex + vlogHeadOffset);
				*lastChunkOffset += ced.length; //just appended
				{
					// need to protect segmentsMap from segmentManager access
				    uint16_t bucket = ced.physicalSegmentId % 1024; //the physical segment id
					SpinLock::Guard _(segmentManager->segmentsLocks[bucket]);
					if (contains(segmentManager->segmentsMap[bucket], ced.physicalSegmentId)) {
						segment = segmentManager->segmentsMap[bucket][ced.physicalSegmentId];
					}
				}
				segment->appendToBuffer(buffer, ced.offset, ced.length);

//				SpinLock::Guard _(chunkLock);
				if(contains(segmentsHeadMap, ced.physicalSegmentId)) {
					segmentsHeadMap[ced.physicalSegmentId] += ced.length;
				} else {
					segmentsHeadMap[ced.physicalSegmentId] = ced.length;
				}

			} else {
//				fprintf(stdout, ">>> replicate chunkIndex vlogNextHeadOffset vlogHeadLength %u %u %u\n", *chunkIndex, vlogNextHeadOffset, vlogHeadLength);
//				fflush (stdout);
				ChunkEntryDigest ced = vlogNextHead->getAtOffset(*chunkIndex + vlogNextHeadOffset - vlogHeadLength);
				*lastChunkOffset += ced.length; //just appended
				{
					// need to protect segmentsMap from segmentManager access
				    uint16_t bucket = ced.physicalSegmentId % 1024; //the physical segment id
					SpinLock::Guard _(segmentManager->segmentsLocks[bucket]);
					if (contains(segmentManager->segmentsMap[bucket], ced.physicalSegmentId)) {
						segment = segmentManager->segmentsMap[bucket][ced.physicalSegmentId];
					}
				}
				segment->appendToBuffer(buffer, ced.offset, ced.length);

//				SpinLock::Guard _(chunkLock);
				if(contains(segmentsHeadMap, ced.physicalSegmentId)) {
					segmentsHeadMap[ced.physicalSegmentId] += ced.length;
				} else {
					segmentsHeadMap[ced.physicalSegmentId] = ced.length;
				}

			}
			*chunkIndex += sizeof32(ChunkEntryDigest); //move to next chunk
		}

		assert(*lastChunkOffset == offsetAndLength);

		if(*lastChunkOffset > offsetAndLength) {
			throw FatalError(HERE, "Chunks should be atomically replicated");
		}

	}
	//when a backup replica segment is created, header and digest are atomically replicated
	else if(offset == 0) {
		//update chunk hint for next chunk to be replicated
		*chunkIndex = 0;
		*lastChunkOffset = openLen; //chunk's head

//		fprintf(stdout, ">>> replicate vlogHeadOffset vlogNextHeadOffset %u %u %u %u \n",
//				(unsigned)vlogHeadOffset, openLen, (unsigned)vlogNextHeadOffset, (unsigned)vlogHeadLength);
//		fflush (stdout);

		vlogHead->appendToBuffer(buffer, vlogHeadOffset-openLen, openLen);

		if(length > openLen) {
			//append rest of the buffer
			this->appendToBuffer(buffer, openLen, length-openLen, chunkIndex, lastChunkOffset);
		} else if(length < openLen) {
			throw FatalError(HERE, "Header and digest of this chunk should be atomically replicated");
		}
	} else {
		throw FatalError(HERE, "Header and digest of this chunk should be atomically replicated");
	}
}

//protected by Log syncLock
//segmentsHeadMap corresponds to syncedLength that equals appendedLength previously used by replicatedSegment->sync
//no need to check again each chunk
void
VirtualSegment::syncTo(uint32_t syncedLength) {

	if(durableChunkOffset >= syncedLength || syncToHead || syncedLength <= openLen) {
		return; //no work to do
	}

	//durableChunkOffset grows with the sum of all values from segmentsHeadMap
	//updating segments' durable head

	Segment* segment = NULL;

	for (std::unordered_map<uint64_t, uint32_t>::iterator it=segmentsHeadMap.begin(); it!=segmentsHeadMap.end(); ++it) {
		durableChunkOffset += it->second;
		uint64_t physicalSegmentId = it->first;
	    uint16_t bucket = physicalSegmentId % 1024; //the physical segment id
		SpinLock::Guard _(segmentManager->segmentsLocks[bucket]);
		if (contains(segmentManager->segmentsMap[bucket], physicalSegmentId)) {
			segment = segmentManager->segmentsMap[bucket][physicalSegmentId];
			segment->updateDurableHead(it->second);
		}
	}

	if(durableChunkOffset != syncedLength) {
		throw FatalError(HERE, "Chunks should be atomically replicated");
	}

	if(closed && durableChunkOffset == head) {
		//enable PM release of associated physical segments if possible
		syncToHead = true;
	}

}

} // namespace
