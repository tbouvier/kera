/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_NAMESPACE_H
#define RAMCLOUD_NAMESPACE_H

#include "SpinLock.h"

namespace KerA {


/**
 * manages multiple streams
 */
class Namespace {
public:

	Namespace(uint64_t nextLogId);
	~Namespace() {
	}
	uint64_t getNextLogId();

private:
	SpinLock nlock;
	uint64_t nextLogId;

    DISALLOW_COPY_AND_ASSIGN(Namespace);
};

} // namespace

#endif // !RAMCLOUD_NAMESPACE_H
