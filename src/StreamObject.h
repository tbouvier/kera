/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_STREAMOBJECT_H
#define RAMCLOUD_STREAMOBJECT_H

#include "Buffer.h"
#include "Key.h"

namespace KerA {

typedef uint8_t KeyCount;   // the number of keys in an object
typedef KeyCount KeyIndex;  // the position of this key: 0 to KeyCount - 1
typedef KeyLength CumulativeKeyLength;
typedef uint32_t ObjectLength;

// A convenience macro to get to the starting of the first key in an object
#define KEY_INFO_LENGTH(x) \
    (sizeof32(KeyCount) + (x) * sizeof32(CumulativeKeyLength))


// defined in RamCloud.h Forward declaring here to avoid a circular
// dependency between Object.h and RamCloud.h
struct KeyInfo;
struct KeyOffsets;
class Crc32C;

// Represents the number of keys and the cumulative key length values
//struct KeyOffsets
//{
//    KeyCount numKeys;                        // number of keys
//    CumulativeKeyLength cumulativeLengths[]; // starting of the cumulative key
//                                             // length values.
//} __attribute__((packed));

/**
 * This class defines the format of an object stored in the log and provides
 * methods to easily construct new ones to be appended and interpret ones that
 * have already been written.
 *
 * In other words, this code centralizes the format and parsing of objects
 * (essentially serialization and deserialization). Different constructors
 * serve these two purposes.
 *
 * Each object contains one or more variable-length keys, a variable-length
 * binary blob of data, and a few other pieces of metadata (such as table
 * id and version).  When stored in the log, an object has the following
 * layout:
 *
 * +---------------+--------+---------------------------+----------+----------+
 * | Object Header | # keys | Cumulative Key Lengths .. | Keys ... | Data ... |
 * +---------------+--------+---------------------------+----------+----------+
 *                 <--------------------- keysAndValue ----------------------->
 *
 * "Object Header" is a structure of type Header, defined below.
 * "# keys" is typedefined below as KeyCount
 * "CumulativeKeyLength" is typedefined below as CumulativeKeyLength
 *
 * Everything except the header and the number of keys is of variable length.
 * If the cumulative key length values are 16 bits each, then
 * the constraint is that the total length of all the keys in an object has
 * to be <= 64K.
 * The length of any key can be calculated using these equations,
 *
 * Length_0 = CumulativeKeyLength_0
 * Length_i = max(0, CumulativeKeyLength_i - CumulativeKeyLength_i-1) for i >= 1
 *
 * If Key_i is not present, CumulativeKeyLength_i = CumulativeKeyLength_i-1.
 * Consequently, Length_i = 0
 */
class StreamObject {
  public:

	StreamObject(uint64_t tableId, Buffer& keysAndValueBuffer);
	StreamObject(uint64_t tableId, uint64_t version, uint32_t timestamp,
           Buffer& keysAndValueBuffer, uint32_t startDataOffset = 0, uint32_t length = 0);
    explicit StreamObject(Buffer& buffer, uint32_t offset = 0, uint32_t length = 0);
    void assembleForLog(Buffer& buffer, bool useNewHeader);

    void reset(uint32_t startDataOffset, uint32_t length);
    void appendValueToBuffer(Buffer* buffer);
    static void appendKeysAndValueToBuffer(
            uint64_t tableId, KeyCount numKeys, KeyInfo *keyList,
            const void* value, uint32_t valueLength, Buffer* request,
            uint32_t *length = NULL);
    static void appendKeysAndValueToBuffer(
            Key& key, const void* value, uint32_t valueLength,
            Buffer* buffer, bool appendCopy = false, uint32_t *length = NULL);
    static void appendHeaderKeysAndValueToBuffer(
            Key& key, const void* value, uint32_t valueLength,
            Buffer* buffer, bool appendCopy = false, uint32_t *length = NULL);
	static uint32_t getLengthHeaderKeysAndValue(Key& key, uint32_t valueLength);
    void appendKeysAndValueToBuffer(Buffer& buffer);

    bool fillKeyOffsets();

    const void* getKey(KeyIndex keyIndex = 0, KeyLength *keyLength = NULL);
    KeyLength getKeyLength(KeyIndex keyIndex = 0);
    const void* getKeysAndValue();
    KeyCount getKeyCount();
    const void* getValue(uint32_t *valueLength = NULL);
    bool getValueOffset(uint32_t *offset);
    uint32_t getValueLength();

    uint32_t getKeysAndValueLength();
    uint32_t getSerializedLength();

    bool checkIntegrity();

    /**
     * This data structure defines the format of an object header stored in a
     * master server's log.
     * KERAI object header has always timestamp - use this field
     */
    class HeaderGroup {
      public:

        HeaderGroup()
         : checksum(0),
		   magic(0),
		   attributes(0)
        {
        }

        /// CRC32C checksum covering everything but this field, including the
        /// keys and the value.
        uint32_t checksum;

        /// Object creation/modification timestamp. WallTime.cc is the clock.
//        uint64_t timestamp; //we keep this to reflect the memory overhead given in Kafka by record's offset

        uint8_t magic; //similar to Kafka - format changes
        uint8_t attributes; //similar to Kafka - compression/timestamp encoding

        /// Following this class will be the number of keys, key lengths,
        /// the keys and finally the value. This member is only here to denote
        /// this.
        char keysAndData[0];
	} __attribute__((__packed__));
	static_assert(sizeof(HeaderGroup) == 6,
		"Unexpected serialized Object size");

    void applyChecksum(Crc32C *crc);

	static uint32_t computeChecksum(const StreamObject::HeaderGroup* object,
									uint32_t totalLength);
	uint32_t computeChecksumGroup();

	/// Copy of the object header that is in, or will be written to, the log.
	//since we have a ChunkEntry responsible for checksum and other magic/attributes, this is not required
	//the ChunkEntry's checksum applies over all records (entry, keys and values)
//	HeaderGroup headerGroup;

    /// Length that includes the number of keys, the key lengths, the keys
    /// and the value. This isn't stored in Header since it can be computed
    /// as needed.
    uint32_t keysAndValueLength;

    /// If the keys and value for the object all lie in a single
    /// contiguous region of memory, this will point there, otherwise this
    /// will point to NULL.
    const void* keysAndValue;

    /// If the keys and value for the object are stored in a Buffer,
    /// this will point to that buffer, otherwise this points to NULL.
    Buffer* keysAndValueBuffer;

    /// The byte offset in the keysAndValueBuffer where keysAndValue start
    uint32_t keysAndValueOffset;

    /// Pointer to a contiguous memory region that contains the number of
    /// keys and the cumulative key length values. This will be NULL until
    /// the first call to getKey(), getKeyLength(), getValue() or
    /// getValueOffset()
    const KeyOffsets *keyOffsets;

    DISALLOW_COPY_AND_ASSIGN(StreamObject);
};

} // namespace KerA

#endif
