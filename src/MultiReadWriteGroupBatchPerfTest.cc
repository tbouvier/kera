/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "TestUtil.h"
#include "MockCluster.h"
#include "MultiReadGroup.h"
#include "MultiWriteGroup.h"
#include "RawMetrics.h"
#include "ServerMetrics.h"
#include "ShortMacros.h"
#include "RamCloud.h"
#include "Util.h"
#include "StreamObject.h"
#include "MultiWriteGroupBatch.h"
#include <thread>

namespace KerA {

static bool
antiGetEntryFilter(string s)
{
    return s != "getEntry";
}

class MultiReadWriteGroupBatchPerfTest : public ::testing::Test {
  public:
    TestLog::Enable logEnabler;
    Context context;
    MockCluster cluster;
    Tub<RamCloud> ramcloud;
    Tub<RamCloud> ramcloudWrite;
    uint64_t tableId1;
    BindTransport::BindSession* session1;
    Tub<ObjectBuffer> keyvalues[9];
    Tub<Buffer> respvalues[9];
    MultiReadGroupObject objects[8];

    Tub<MultiReadGroupObject> multiReadGroupObject;
    Tub<MultiReadGroupObject> multiReadGroupObject2;
    GroupSegmentInfo groupList[1];
    Tub<MultiWriteGroupBatchObject> multiWriteGroupBatchObject;

  public:
    MultiReadWriteGroupBatchPerfTest()
        : logEnabler(antiGetEntryFilter)
        , context()
        , cluster(&context)
        , ramcloud()
        , ramcloudWrite()
        , tableId1(-1)
        , session1(NULL)
        , keyvalues()
        , respvalues()
        , objects()
        , multiReadGroupObject()
        , multiReadGroupObject2()
        , groupList()
        , multiWriteGroupBatchObject()
    {
        Logger::get().setLogLevels(KerA::DEBUG);

        ServerConfig config = ServerConfig::forTesting();
        config.services = {WireFormat::MASTER_SERVICE};
        config.localLocator = "mock:host=master1";
        config.numberSegmentsPerGroup = 2;
        config.numberActiveGroupsPerStreamlet = 2;
        config.master.numReplicas = 0;
        config.master.logBytes = 1512 * 1024 * 1024UL;
        config.master.allowLocalBackup = false;
        config.master.disableLogCleaner = true;
        config.master.disableInMemoryCleaning = true;

        config.maxCores = 2;
        config.maxObjectKeySize = 512;
        config.maxObjectDataSize = 1024;
        config.segmentSize = 8*1024*1024;
        config.segletSize = 8*1024*1024;

        cluster.addServer(config);

//        config.localLocator = "mock:host=master2";
//        cluster.addServer(config);

        ramcloud.construct(&context, "mock:host=coordinator");
        ramcloudWrite.construct(&context, "mock:host=coordinator");

        // Write some test data to the servers.
        tableId1 = ramcloud->createTable("table1", 1, 2);

        // Get pointers to the master sessions.
        Transport::SessionRef session =
                ramcloud->clientContext->transportManager->getSession(
                "mock:host=master1");
        session1 = static_cast<BindTransport::BindSession*>(session.get());

        groupList[0].groupId = 1;
        groupList[0].segmentId = 1;
//        groupList[0].valueOnly = false;
        groupList[0].offset = 0;
        groupList[0].maxObjects = 100;

    }

    DISALLOW_COPY_AND_ASSIGN(MultiReadWriteGroupBatchPerfTest);
};

#define BATCH_SIZE 1000
#define KEY_SIZE 16
#define LARGE_VALUE_WITH_KEY 90
#define LARGE_VALUE_NO_KEY 100

#define SMALL_VALUE_WITH_KEY 10
#define SMALL_VALUE_NO_KEY 20

// overhead with key = 17B
// overhead no key = 14B

#define NR_SEGMENTS_LARGE_VALUE_WITH_KEY 14
#define NR_SEGMENTS_LARGE_VALUE_NO_KEY 14

#define NR_SEGMENTS_SMALL_VALUE_WITH_KEY 5
#define NR_SEGMENTS_SMALL_VALUE_NO_KEY 5

#define BATCH_STREAM 1000
#define RECORD_COUNT 10000000

#define PRODID 1


static void multiwrite(uint64_t* tableId1, Tub<RamCloud>* ramcloud)
{
	const uint32_t recordCount = RECORD_COUNT;
	const int recordSizeB = LARGE_VALUE_NO_KEY;
    const uint64_t producerIdToPartition = PRODID;
    const uint32_t streamletId = 1;
    const uint64_t entryId = 1;
    Key primaryKey(*tableId1, NULL, 0);

	uint32_t streamletSpan = 1;
	uint64_t tabletRange = 1 + ~0UL / streamletSpan;
	uint64_t keyHash = (streamletId-1) * tabletRange;

	uint64_t writeOverhead = 0;
	uint64_t startWriteOverhead = Cycles::rdtsc();
	uint64_t endWriteOverhead = Cycles::rdtsc();
	uint32_t i, j, j2, length = 0, length2 = 0;

	//first group batch and value source
    Tub<Buffer> objreq;
    objreq.construct();

    //total object length including overhead/headers - minus entryHeader
	ObjectLength objectLength = KEY_INFO_LENGTH(1) + primaryKey.getStringKeyLength() + recordSizeB + sizeof32(Object::HeaderGroup);
	Segment::EntryHeader entryHeader(LOG_ENTRY_TYPE_OBJ, objectLength);

	Tub<Buffer> entryHeaderBuffer; //overhead per record, initialized once because key/s (size and number) are fixed
	uint32_t entryHeaderBufferLength = sizeof32(entryHeader) + entryHeader.getLengthBytes();
	entryHeaderBuffer.construct();
	entryHeaderBuffer.get()->append(&entryHeader, sizeof32(entryHeader));
	entryHeaderBuffer.get()->append(&objectLength, entryHeader.getLengthBytes());

//	uint32_t dataLength = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart)+BATCH_SIZE * (objectLength+entryHeaderBufferLength);
//    objreq.get()->allocFirstChunkInternal(dataLength);


	char* charValues = new char[BATCH_SIZE * recordSizeB];
	memset(charValues, 0, BATCH_SIZE * recordSizeB);

    Tub<MultiWriteGroupBatchObject> multiWriteGroupBatchObject;
    multiWriteGroupBatchObject.construct(&objreq, entryId, keyHash);
    uint32_t recordsCount = 0;
    WireFormat::MultiOpGroup::Request::WriteGroupBatchPart* batchPart =
    		objreq.get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(recordsCount, length, streamletId);
	length += sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);

	//second group batch
    Tub<Buffer> objreq2;
    objreq2.construct();

	ObjectLength objectLength2 = Object::getLengthHeaderKeysAndValue(primaryKey, recordSizeB);
	Segment::EntryHeader entryHeader2(LOG_ENTRY_TYPE_OBJ, objectLength2);

	Tub<Buffer> entryHeaderBuffer2; //overhead per record, initialized once because key/s (size and number) are fixed
	uint32_t entryHeaderBufferLength2 = sizeof32(entryHeader2) + entryHeader2.getLengthBytes();
	entryHeaderBuffer2.construct();
	entryHeaderBuffer2.get()->append(&entryHeader2, sizeof32(entryHeader2));
	entryHeaderBuffer2.get()->append(&objectLength2, entryHeader2.getLengthBytes());

//	uint32_t dataLength2 = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart)+BATCH_SIZE * (objectLength2+entryHeaderBufferLength2);
//    objreq2.get()->allocFirstChunkInternal(dataLength2);

	char* charValues2 = new char[BATCH_SIZE * recordSizeB];
	memset(charValues2, 0, BATCH_SIZE * recordSizeB);

    Tub<MultiWriteGroupBatchObject> multiWriteGroupBatchObject2;
    multiWriteGroupBatchObject2.construct(&objreq2, entryId, keyHash);
    uint32_t recordsCount2 = 0;
    WireFormat::MultiOpGroup::Request::WriteGroupBatchPart* batchPart2 =
    		objreq2.get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(recordsCount2, length2, streamletId);
	length2 += sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);

	MultiWriteGroupBatchObject* requests1[] = {multiWriteGroupBatchObject.get()};
	MultiWriteGroupBatchObject* requests2[] = {multiWriteGroupBatchObject2.get()};

	uint64_t startInsert = Cycles::rdtsc();
	uint64_t overheadRandomString = 0;
    for (i = 0; i < static_cast<uint32_t>(recordCount/2); i++) {
        j = i % BATCH_SIZE; j2 = i % BATCH_SIZE;
        recordsCount++; recordsCount2++;

        uint64_t startGenValue = Cycles::rdtsc();
        char* value = charValues + j * recordSizeB;
        char* value2 = charValues2 + j2 * recordSizeB;
//        Util::genRandomString(value, recordSizeB);
        for (int val = 0; val < recordSizeB; ++val) {
        	value[val] = 'x'; value2[val] = 'y';
		}
        uint64_t endGenValue = Cycles::rdtsc();
        overheadRandomString += (endGenValue - startGenValue);

        //======== [Entry + length + content] our client buffer looks exactly like what we write on server
        objreq.get()->append(entryHeaderBuffer.get(), 0, entryHeaderBufferLength);
		Object::appendHeaderKeysAndValueToBuffer(primaryKey, value, recordSizeB, objreq.get(), false);
		length += (sizeof32(entryHeader) + entryHeader.getLengthBytes() + objectLength);
		//=========

        //======== [Entry + length + content] our client buffer looks exactly like what we write on server
		objreq2.get()->append(entryHeaderBuffer2.get(), 0, entryHeaderBufferLength2);
		Object::appendHeaderKeysAndValueToBuffer(primaryKey, value2, recordSizeB, objreq2.get(), false);
		length2 += (sizeof32(entryHeader2) + entryHeader2.getLengthBytes() + objectLength2);
		//=========

        // Do the write and recycle the objects
        if (j == BATCH_SIZE - 1 || j2 == BATCH_SIZE - 1) {
        	startWriteOverhead = Cycles::rdtsc();
        	j++; j2++;

        	batchPart->length = length;
        	batchPart->recordsCount = recordsCount;
        	batchPart2->length = length2;
			batchPart2->recordsCount = recordsCount2;

			MultiWriteGroupBatch mwreq1(ramcloud->get(), requests1, 1, *tableId1, producerIdToPartition);

			MultiWriteGroupBatch mwreq2(ramcloud->get(), requests2, 1, *tableId1, producerIdToPartition+1);

			while(!mwreq2.isReady() && mwreq1.isReady()) {

			}

			//        	(*ramcloud)->multiWriteGroupBatch(requests, 2, *tableId1, producerIdToPartition, "object1-1", 9);

        	endWriteOverhead = Cycles::rdtsc();
        	writeOverhead += (endWriteOverhead - startWriteOverhead);

            memset(charValues, 0, BATCH_SIZE * recordSizeB);
            recordsCount = 0; //reset
			length = 0; //reset
			objreq.get()->reset(); //FirstChunk();
			objreq.get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(recordsCount, length, streamletId);
			length += sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);

            memset(charValues2, 0, BATCH_SIZE * recordSizeB);
            recordsCount2 = 0; //reset
			length2 = 0; //reset
			objreq2.get()->reset(); //FirstChunk();
			objreq2.get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(recordsCount2, length2, streamletId);
			length2 += sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
        }
    }

	uint64_t stopInsert = Cycles::rdtsc();

    delete[] charValues;
    delete[] charValues2;

	RAMCLOUD_LOG(NOTICE,
	                    ">>>>>>>>>>>>>Actual INSERT OPS %.0f OverheadValue %.0f OVERHEAD_MULTIWRITE %.0f",
	                    static_cast<double>(Cycles::toMicroseconds(stopInsert - startInsert)),
						static_cast<double>(Cycles::toMicroseconds(overheadRandomString)),
						static_cast<double>(Cycles::toMicroseconds(writeOverhead))
						);

}

TEST_F(MultiReadWriteGroupBatchPerfTest, concurrent_multi_reads_multi_writes) {
	RAMCLOUD_LOG(NOTICE, "======================STARTING concurrent_reads_writes===============================\n\n\n");

	std::thread mwrite(multiwrite, &tableId1, &ramcloudWrite);
	mwrite.join();
//	usleep(1000000); //sleep 1s

	const uint32_t recordCount = RECORD_COUNT;
	uint32_t totalNumberObjectsRead = 0;
	uint64_t totalCountValueLength = 0;

	uint64_t startRead1000k = Cycles::rdtsc();

	//current stream offset to start read from  KERAI enhance with groupId, streamId
	SegmentIdAndOffset lastOffset;
	lastOffset.segmentId = INVALID_SEGMENT_ID;
	lastOffset.offset = INVALID_SEGMENT_OFFSET;

	uint32_t streamletId = 1;
	uint64_t readerId = 1;
	while(totalNumberObjectsRead < recordCount/2) {
		//1. get next available groupId
		uint32_t streamletSpan = 1; //number of streamlets
		uint64_t tabletRange = 1 + ~0UL / streamletSpan;
		uint64_t keyHash = (streamletId-1) * tabletRange;
		uint64_t nextGroupId = ramcloud->getNextAvailableGroupId(tableId1, streamletId, readerId, keyHash);

		//2. if groupId is not valid, wait and try again
		if(INVALID_GROUP_ID == nextGroupId) {
			usleep(10000); //sleep 10ms
			continue;
		}

		//3. at this point we have a valid group not yet processed
		bool isGroupProcessed = false;
//		RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered groupId %lu", nextGroupId);

		while(!isGroupProcessed && totalNumberObjectsRead < recordCount/2) {
			uint64_t readerId = -1;
			//4. determine if group offset should be used
			bool useGroupOffset = lastOffset.segmentId != INVALID_SEGMENT_ID; // && lastOffset.offset != INVALID_SEGMENT_OFFSET;

			//5. get current group offset - useful for crashed consumers
			if(!useGroupOffset) { //try get one from broker
				Buffer offsetResponseBuffer;
				ramcloud->getGroupOffset(tableId1, keyHash, readerId, nextGroupId, &offsetResponseBuffer);
				const WireFormat::GetGroupOffset::Response* currentGroupOffset =
						offsetResponseBuffer.getOffset<WireFormat::GetGroupOffset::Response>(0);

				lastOffset.segmentId = currentGroupOffset->segmentId;
				lastOffset.offset = currentGroupOffset->offset;

				useGroupOffset = lastOffset.segmentId != INVALID_SEGMENT_ID; // && lastOffset.offset != INVALID_SEGMENT_OFFSET;
			}
//			RAMCLOUD_LOG(NOTICE, ">>> Current Group Offset: %lu %u", lastOffset.segmentId, lastOffset.offset);

			//6. get next available segments by groupId and its current offset
			Buffer responseBufferSegments;
			//should return segments starting from provided offset >= lastOffset.segmentId
			uint32_t groupSegmentsCount = ramcloud->getSegmentsByGroupId(tableId1, streamletId, keyHash, nextGroupId,
					useGroupOffset, lastOffset.segmentId, lastOffset.offset, -1, &responseBufferSegments);
//			RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered number of segments %u", groupSegmentsCount);

			//7. identify segments in responseBufferSegments
			uint32_t respOffset = 0;
			//first object in the response
			const WireFormat::GetSegmentsByGroupId::Response* respHdr =
					responseBufferSegments.getOffset<WireFormat::GetSegmentsByGroupId::Response>(respOffset);
			respOffset =+ sizeof32(*respHdr);

			assert(respHdr->length == groupSegmentsCount);
			Tub<SegmentIdAndOffset> segments[groupSegmentsCount];
			for (size_t i=0; i<groupSegmentsCount; i++) {
				const SegmentIdAndOffset* next = responseBufferSegments.getOffset<SegmentIdAndOffset>(respOffset);
				segments[i].construct(next->segmentId);
//				RAMCLOUD_LOG(NOTICE, ">>> Next Segment id: %lu", segments[i].get()->segmentId);
			    respOffset += sizeof32(*next);
			}

			//pull data from each segment until each one is processed
			for (size_t i=0; i<groupSegmentsCount; i++) {
				bool segmentProcessedAndClosed = false;
				uint32_t numberObjectsRead = 0; //per request
				uint32_t numberObjectsReadPerSegment = 0; //total read per segment
				uint64_t countValueLength=0; //computed sum of value length bytes

				//time total read per segment
//				uint64_t startRead = Cycles::rdtsc();

				lastOffset.segmentId = segments[i].get()->segmentId;
				lastOffset.offset = 0;
				while(!segmentProcessedAndClosed && totalNumberObjectsRead < recordCount/2) {
					//send request and wait
					groupList[0].maxObjects = BATCH_STREAM;
					groupList[0].segmentId = lastOffset.segmentId;
					groupList[0].groupId = nextGroupId;
					groupList[0].offset = lastOffset.offset;
			        groupList[0].streamletId = streamletId;
			        groupList[0].maxResponseLength = BATCH_STREAM*120; //equivalent 1000 objects

			        uint32_t numResponses = 1;
//					Tub<Buffer> rpcResponseArray[numResponses];
//					for(uint32_t ri=0; ri<numResponses; ri++) {
//						rpcResponseArray[ri].construct();
//					}
//					Buffer *rpcResponses[] = {rpcResponseArray[0].get()};
					multiReadGroupObject.construct(tableId1, keyHash, &respvalues[0], groupList);
					MultiReadGroupObject* requests[] = {multiReadGroupObject.get()};
					uint32_t maxStreamletFetchBytes = 1000000; // configurable; up to this n bytes per streamlet
					MultiReadGroup request(ramcloud.get(), requests, 1, maxStreamletFetchBytes, NULL, numResponses, false);
					request.wait();
					ASSERT_TRUE(request.isReady());

					//get and process objects from response

					Buffer* responseBuffer = multiReadGroupObject.get()->respvalue->get(); //multiReadGroupObject.get()->respvalue->get();
					numberObjectsRead = multiReadGroupObject.get()->numberObjectsRead;

					totalNumberObjectsRead += numberObjectsRead;
					numberObjectsReadPerSegment += numberObjectsRead;

					//condition to exit segment processing
					if (multiReadGroupObject.get()->isSegmentClosed) {
						segmentProcessedAndClosed = (numberObjectsReadPerSegment == multiReadGroupObject.get()->numberOfObjectEntries);
					}

					ASSERT_TRUE(numberObjectsRead <= multiReadGroupObject.get()->segmentInfo->maxObjects);

					if(numberObjectsRead == static_cast<uint32_t>(0)) {
						usleep(10000); //sleep 10ms - give some time to producers => config. setting equivalent linger.ms in Kafka
						continue;
					} else {
						lastOffset.offset = multiReadGroupObject.get()->nextOffset; //position of last read object in current segment
						assert(lastOffset.offset != static_cast<uint32_t>(0));
					}

					uint32_t respOffset = 0;
					uint32_t totalLength = *responseBuffer->getOffset<uint32_t>(respOffset);
					respOffset += sizeof32(uint32_t); // length
					SegmentCertificate sc; // take this from responseBuffer as well
					sc.segmentLength = totalLength;
					SegmentIterator it(responseBuffer->getRange(respOffset, totalLength), totalLength, sc);

					while (!it.isDone()) {

						respOffset += it.getEntryOffset();
						//see HeaderGroup sizeof32(headerGroup) == 6
						StreamObject obj0(tableId1, 1, 0, *responseBuffer, respOffset+sizeof32(Object::HeaderGroup), it.getLength()-sizeof32(Object::HeaderGroup));
//						RAMCLOUD_LOG(NOTICE, ">>> Done request: key=%s value=%s totalValueLength=%lu",
//							string(reinterpret_cast<const char*>(obj0.getKey()), obj0.getKeyLength()).c_str(),
//							string(reinterpret_cast<const char*>(obj0.getValue()), obj0.getValueLength()).c_str(),
//							countValueLength
//							);
						countValueLength += obj0.getValueLength();
						respOffset += it.getLength();
						it.next();
					}

				} //end segment processing

				totalCountValueLength += countValueLength;
//				uint64_t stopRead = Cycles::rdtsc();
//				RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>>Actual READ OPS segment %lu objects read %u valueLength %lu time %.0f",
//						lastOffset.segmentId, numberObjectsReadPerSegment, countValueLength,
//						static_cast<double>(Cycles::toMicroseconds(stopRead - startRead)));

//				RAMCLOUD_LOG(NOTICE, "updateGroupOffset %lu %lu %u", nextGroupId, lastOffset.segmentId, lastOffset.offset);
				isGroupProcessed = ramcloud->updateGroupOffset(tableId1, keyHash, streamletId,
		    			readerId, nextGroupId, false, -1, -1, -1, -1,
						lastOffset.segmentId, lastOffset.offset, -1, -1, NULL);  //do I need any answer like group is processed?

			} //end segments processing

		} //end group processing

	} //end main loop

	//mwrite.join();

	uint64_t stopRead1000K = Cycles::rdtsc();
	RAMCLOUD_LOG(NOTICE,
						">>>>>>>>>>>>>>Actual READ OPS segments objects read expected %u total %u time %.0f totalValueLength %lu",
						recordCount, totalNumberObjectsRead,
						static_cast<double>(Cycles::toMicroseconds(stopRead1000K - startRead1000k)), totalCountValueLength);

	RAMCLOUD_LOG(NOTICE, "======================ENDING concurrent_reads_writes===============================\n\n\n");
}

TEST_F(MultiReadWriteGroupBatchPerfTest, multiwritegroupbatch_multireadbatch_producerid_hugeobjects_noKey_maxsize) {

	RAMCLOUD_LOG(NOTICE, "======================STARTING multiwritegroupbatch_multireadbatch_producerid_hugeobjects_noKey_maxsize===============================\n\n\n");

	const uint32_t recordCount = RECORD_COUNT; //how many records we write-stream
	int recordSizeB = LARGE_VALUE_NO_KEY; //size of record-value
    uint64_t producerIdToPartition = PRODID;
    uint32_t i, j;

	//values
	char* charValues = new char[BATCH_SIZE * recordSizeB];
	memset(charValues, 0, BATCH_SIZE * recordSizeB);

	uint64_t writeOverhead = 0;
	uint64_t startWriteOverhead = Cycles::rdtsc();
	uint64_t endWriteOverhead = Cycles::rdtsc();

    Key primaryKey(tableId1, NULL, 0);
    //total object length including overhead/headers - minus entryHeader
	ObjectLength objectLength = KEY_INFO_LENGTH(1) + primaryKey.getStringKeyLength() + recordSizeB + sizeof32(Object::HeaderGroup);
	Segment::EntryHeader entryHeader(LOG_ENTRY_TYPE_OBJ, objectLength);

	Tub<Buffer> entryHeaderBuffer; //overhead per record, initialized once because key/s (size and number) are fixed
	uint32_t entryHeaderBufferLength = sizeof32(entryHeader) + entryHeader.getLengthBytes();
	entryHeaderBuffer.construct();
	entryHeaderBuffer.get()->append(&entryHeader, sizeof32(entryHeader));
	entryHeaderBuffer.get()->append(&objectLength, entryHeader.getLengthBytes());

	//shared buffer for multi-write
    Tub<Buffer> objreq;
    objreq.construct();

    uint32_t dataLength = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart)+BATCH_SIZE * (objectLength+entryHeaderBufferLength);
    objreq.get()->allocFirstChunkInternal(dataLength);

    uint32_t length = 0;
	uint32_t recordsCount = 0;
    uint32_t streamletId = 1;
    uint64_t readerId = 1;
    uint64_t entryId = 1;

	uint32_t streamletSpan = 1;
	uint64_t tabletRange = 1 + ~0UL / streamletSpan;
	uint64_t keyHash = (streamletId-1) * tabletRange;

	multiWriteGroupBatchObject.construct(&objreq, entryId, keyHash);
	MultiWriteGroupBatchObject* requests[] = {multiWriteGroupBatchObject.get()};

	WireFormat::MultiOpGroup::Request::WriteGroupBatchPart* batchPart =
			objreq.get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(recordsCount, length, streamletId);
	length += sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);

	uint64_t startInsert = Cycles::rdtsc();
	uint64_t overheadRandomString = 0;
	bool alternateProducer = false;
    for (i = 0; i < static_cast<uint32_t>(recordCount); i++) {
        j = i % BATCH_SIZE;
        recordsCount++;

        uint64_t startGenValue = Cycles::rdtsc();
        char* value = charValues + j * recordSizeB;
//        Util::genRandomString(value, recordSizeB);
        for (int val = 0; val < recordSizeB; ++val) {
			value[val] = 'x';
		}
        uint64_t endGenValue = Cycles::rdtsc();
        overheadRandomString += (endGenValue - startGenValue);

        //======== [Entry + length + content] our client buffer looks exactly like what we write on server
		objreq.get()->append(entryHeaderBuffer.get(), 0, entryHeaderBufferLength);
		Object::appendHeaderKeysAndValueToBuffer(primaryKey, value, recordSizeB, objreq.get(), false);
		length += (entryHeaderBufferLength + objectLength);
		//=========

        // Do the write and recycle the objects
        if (j == BATCH_SIZE - 1) {
        	startWriteOverhead = Cycles::rdtsc();
        	j++;
        	batchPart->length = length;
        	batchPart->recordsCount = recordsCount;
        	if(!alternateProducer) {
        		producerIdToPartition += 1;
        		ramcloud->multiWriteGroupBatch(requests, 1, tableId1, producerIdToPartition);
        		alternateProducer = true;
        	} else {
        		producerIdToPartition -= 1;
				ramcloud->multiWriteGroupBatch(requests, 1, tableId1, producerIdToPartition);
				alternateProducer = false;
        	}
        	endWriteOverhead = Cycles::rdtsc();
        	writeOverhead += (endWriteOverhead - startWriteOverhead);

            memset(charValues, 0, BATCH_SIZE * recordSizeB);
            recordsCount = 0; //reset
			length = 0; //reset
			//ideally reset buffer should keep chunks (reuse) but zero-ed them
			objreq.get()->resetFirstChunk();
//			memset(data, 0, dataLength);
//			objreq.get()->allocFirstChunk(dataLength, data);
//			objreq.get()->allocFirstChunkInternal(dataLength);

			objreq.get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(recordsCount, length, streamletId);
			length += sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
        }
    }

	uint64_t stopInsert = Cycles::rdtsc();

    delete[] charValues;

	RAMCLOUD_LOG(NOTICE,
	                    ">>>>>>>>>>>>>Actual INSERT OPS %.0f OverheadValue %.0f OVERHEAD_MULTIWRITE %.0f",
	                    static_cast<double>(Cycles::toMicroseconds(stopInsert - startInsert)),
						static_cast<double>(Cycles::toMicroseconds(overheadRandomString)),
						static_cast<double>(Cycles::toMicroseconds(writeOverhead))
						);

	uint64_t startRead1000k = Cycles::rdtsc();

	//identify next available groupId
	keyHash = (streamletId-1) * tabletRange;
//	uint64_t nextGroupId = ramcloud->getNextAvailableGroupId(tableId1, streamletId, readerId, keyHash);
//	RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered groupId %lu", nextGroupId );

	Buffer groupsBuffer;
	ramcloud->getNextAvailableGroupSegmentIds(tableId1, streamletId, readerId, keyHash, 1, &groupsBuffer);
	uint32_t groupRespOffset = 0;
	const WireFormat::GetNextAvailableGroupSegmentId::Response* groupRespHdr = groupsBuffer.getOffset<WireFormat::GetNextAvailableGroupSegmentId::Response>(0);
	groupRespOffset += sizeof32(*groupRespHdr);

	uint32_t discoveredGroups = groupRespHdr->length;

	uint64_t nextGroupId = INVALID_GROUP_ID;
	for (uint32_t i=0; i<discoveredGroups; i++)
	{
		const GroupSegmentId* nextGroup = groupsBuffer.getOffset<GroupSegmentId>(groupRespOffset);
		nextGroupId = nextGroup->groupId;
		RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered groupId %lu", nextGroupId );
		groupRespOffset += sizeof32(*nextGroup);
	}

	uint32_t totalNumberObjectsRead = 0;
	uint64_t totalCountValueLength = 0;

    uint32_t numResponses = 1;

	while(INVALID_GROUP_ID != nextGroupId) { // I should check also offset
		//find segments
		Buffer offsetResponseBuffer;
		ramcloud->getGroupOffset(tableId1, keyHash, -1, nextGroupId, &offsetResponseBuffer);
		const WireFormat::GetGroupOffset::Response* currentGroupOffset =
				offsetResponseBuffer.getOffset<WireFormat::GetGroupOffset::Response>(0);

		Buffer responseBufferSegments;
		// KERA offset not used here but its meaning is for consumer's crashes
		//when we get next segments to be processed by using the group offset or the last known consumer offset
		uint32_t groupSegmentsCount = ramcloud->getSegmentsByGroupId(tableId1, streamletId, keyHash,
				nextGroupId, false, -1, -1, -1, &responseBufferSegments);
//		RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered number of segments %u", groupSegmentsCount );

		uint32_t respOffset = 0; //for segments
		//first object in the response
		const WireFormat::GetSegmentsByGroupId::Response* respHdr =
				responseBufferSegments.getOffset<WireFormat::GetSegmentsByGroupId::Response>(respOffset);
		respOffset =+ sizeof32(*respHdr);

		assert(respHdr->length == groupSegmentsCount);
		Tub<SegmentIdAndOffset> segments[groupSegmentsCount];
		for (uint32_t i=0; i<groupSegmentsCount; i++)
		{
			const SegmentIdAndOffset* next = responseBufferSegments.getOffset<SegmentIdAndOffset>(respOffset);
			segments[i].construct(next->segmentId);
//			RAMCLOUD_LOG(NOTICE, ">>> Next Segment id: %lu %lu", segments[i].get()->segmentId, next->segmentId);
		    respOffset += sizeof32(*next);
		}

		if (currentGroupOffset->segmentId == segments[groupSegmentsCount-1].get()->segmentId)
			break; //it means we processed this group ;; we should cache consumer offsets and validate also offset field

		//read one by one each segment, in batches given by number of objects
		for (uint32_t i=0; i<groupSegmentsCount; i++)
		{
			bool segmentProcessed = false;
			uint32_t numberObjectsRead = 0;
			uint32_t numberObjectsReadPerSegment = 0;
			uint64_t countValueLength=0;
			uint32_t lastOffset = 0;
			uint32_t SEGMENT_CLOSED = 0; //because offset management has to be developed, we use this flag  KERAI
			//but multiReadGroup could return 0 objects and segment is not yet closed, meaning we can read from another segment

//			uint64_t startRead = Cycles::rdtsc();

			while(!segmentProcessed) //keep it until this segment is processed
			{
				groupList[0].maxObjects = BATCH_STREAM;
				groupList[0].segmentId = segments[i].get()->segmentId;
				groupList[0].groupId = nextGroupId;
				groupList[0].offset = lastOffset;
		        groupList[0].maxResponseLength = BATCH_STREAM*120; //enabled

//		        objreq.get()->reset(); //RESET SHARED BUFFER
//				memset(data, 0, dataLength);
//		        objreq.get()->allocFirstChunkInternal(dataLength);
//		        objreq.get()->allocFirstChunk(dataLength, data);

				multiReadGroupObject.construct(tableId1, keyHash, &respvalues[0], groupList);
				MultiReadGroupObject* requests[] = {multiReadGroupObject.get()};

				//next is shared buffer for multi-reads
//				Tub<Buffer> rpcResponseArray[numResponses];
//				for(uint32_t ri=0; ri<numResponses; ri++) {
//					rpcResponseArray[ri].construct();
//				}
//				Buffer *rpcResponses[] = {rpcResponseArray[0].get()};

				uint32_t maxStreamletFetchBytes = 1000000; // configurable; up to this n bytes per streamlet
				MultiReadGroup request(ramcloud.get(), requests, 1, maxStreamletFetchBytes, NULL, numResponses, false);
				request.wait();

				ASSERT_TRUE(request.isReady());

				//next is using the shared/external buffer used to multi-read; initial respOffset modified
				Buffer* responseBuffer = multiReadGroupObject.get()->respvalue->get();
				numberObjectsRead = multiReadGroupObject.get()->numberObjectsRead;
				if(numberObjectsRead == SEGMENT_CLOSED) {
					segmentProcessed = true;
					continue;
				} else {
					lastOffset = multiReadGroupObject.get()->nextOffset;
					assert(lastOffset != static_cast<uint32_t>(0));
				}
				totalNumberObjectsRead += numberObjectsRead;
				numberObjectsReadPerSegment += numberObjectsRead;
				ASSERT_TRUE(numberObjectsRead <= multiReadGroupObject.get()->segmentInfo->maxObjects);

				//initialized by response in our object request
				uint32_t respOffset = 0;
				uint32_t totalLength = *responseBuffer->getOffset<uint32_t>(respOffset);
//				RAMCLOUD_LOG(NOTICE, "totalLength %u  buffer size %u",
//						totalLength,
//						multiReadGroupObject.get()->responseLength - 4);
				respOffset += sizeof32(uint32_t); // length

				SegmentCertificate sc; // take this from responseBuffer as well
				sc.segmentLength = totalLength;
				SegmentIterator it(responseBuffer->getRange(respOffset, totalLength), totalLength, sc);

				//for 10mil records, only iterating+objects adds 1.5s to the 0.8s just to pull data - due to Object creation & iteration
				while (!it.isDone()) {
					respOffset += it.getEntryOffset();
					StreamObject obj0(tableId1, 1, 0, *responseBuffer,
							respOffset+sizeof32(StreamObject::HeaderGroup), it.getLength()-sizeof32(StreamObject::HeaderGroup));
//					StreamObject obj0(*responseBuffer, respOffset, it.getLength()); //less efficient
//					RAMCLOUD_LOG(NOTICE, ">>> Done request: key=%s value=%s totalValueLength=%lu",
//						string(reinterpret_cast<const char*>(obj0.getKey()), obj0.getKeyLength()).c_str(),
//						string(reinterpret_cast<const char*>(obj0.getValue()), obj0.getValueLength()).c_str(),
//						countValueLength
//						);
					countValueLength += obj0.getValueLength();
					respOffset += it.getLength();
					it.next();
				}

			}
			totalCountValueLength += countValueLength;
			countValueLength = 0;

//			uint64_t stopRead = Cycles::rdtsc();
//			RAMCLOUD_LOG(NOTICE,
//								">>>>>>>>>>>>>>Actual READ OPS segment %lu objects read %u valueLength %lu time %.0f",
//								segments[i].get()->segmentId, numberObjectsReadPerSegment, countValueLength,
//								static_cast<double>(Cycles::toMicroseconds(stopRead - startRead)));

			//at this point I can update group offset - just mark this segment,
			//when last segment is offseted, group is closed
			//this is cheap, one RPC per segment
			//if we keep offsets on ingestion node after every processed batch, this RPC work
			//could be included in the next call for multiReadGroup !!!  KERA
//			RAMCLOUD_LOG(NOTICE, "updateGroupOffset %lu %lu %u", nextGroupId, segments[i].get()->segmentId, lastOffset);
			ramcloud->updateGroupOffset(tableId1, keyHash, streamletId,
	    			-1, nextGroupId, false, -1, -1, -1, -1,
					segments[i].get()->segmentId, lastOffset, -1, -1, NULL);

		}

		nextGroupId = ramcloud->getNextAvailableGroupId(tableId1, streamletId, readerId, keyHash);
//		RAMCLOUD_LOG(NOTICE,
//							">>>>>>>>>>>>>Discovered new groupId %lu",
//							nextGroupId
//							);
	}

//	delete[] data;
	uint64_t stopRead1000K = Cycles::rdtsc();
	RAMCLOUD_LOG(NOTICE,
						">>>>>>>>>>>>>>Actual READ OPS segments objects read expected %u total %u time %.0f total_countV %lu",
						recordCount, totalNumberObjectsRead,
						static_cast<double>(Cycles::toMicroseconds(stopRead1000K - startRead1000k)), totalCountValueLength);


	RAMCLOUD_LOG(NOTICE, "======================ENDING multiwritegroupbatch_multireadbatch_producerid_hugeobjects_noKey_maxsize===============================\n\n\n");
}

//OVERHEAD_MULTIWRITE 2613089
//1503014098.181208722 MultiReadWriteGroupBatchPerfTest.cc:832 in TestBody NOTICE[1]:
//>>>>>>>>>>>>>>Actual READ OPS segments objects read expected 10000000 total 10000000 time 2253841

}  // namespace KerA
