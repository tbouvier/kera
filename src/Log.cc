/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2009-2016 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <stdint.h>

#include "Log.h"
#include "PerfStats.h"
#include "ServerConfig.h"
#include "ShortMacros.h"

namespace KerA {

/**
 * Constructor for Log. No segments are allocated in the constructor, so if
 * replicas are being used no backups will have been contacted yet and there
 * will be no durable evidence of this log having existed. Call sync() to
 * allocate and force the head segment (and others, perhaps) to backups.
 *
 * The reason for this behaviour is complicated. We want to ensure that the
 * log is made durable before assigning any tablets to the master, since we
 * want a lack of the log on disk to unambiguously mean data loss if the
 * coordinator thinks we have any tablets. However, the constructor cannot
 * allocate and replicate the first head, since there is the potential for
 * deadlock (transport manager isn't running yet, so we can't learn of any
 * backups from the coordinator).
 *
 * \param context
 *      Overall information about the RAMCloud server.
 * \param config
 *      The ServerConfig containing configuration options that affect this
 *      log instance. Rather than passing in various separate bits, the log
 *      will extract any parameters it needs from the global server config.
 * \param entryHandlers
 *      Class to query for various bits of per-object information. For instance,
 *      the log may want to know whether an object is still needed or if it can
 *      be garbage collected. Methods on the given class instance will be
 *      invoked to field such queries.
 * \param segmentManager
 *      The SegmentManager this log should allocate its head segments from.
 * \param replicaManager
 *      The ReplicaManager that will be used to make each of this Log's
 *      Segments durable.
 */
Log::Log(Context* context,
         const ServerConfig* config,
         LogEntryHandlers* entryHandlers,
         SegmentManager* segmentManager,
         ReplicaManager* replicaManager,
		 uint64_t logId, uint64_t masterId, uint64_t streamId)
    : AbstractLog(entryHandlers,
                  segmentManager,
                  replicaManager,
                  config->segmentSize,
				  config->numberSegmentsPerGroup,
				  config->numberActiveGroupsPerStreamlet),
	  logId(logId),
	  masterId(masterId),
	  streamId(streamId),
      context(context),
      syncLock("Log::syncLock"),
	  nextSegmentId(1),
      metrics()
{

}

/**
 * Clean up after the Log.
 */
Log::~Log()
{
	// clean vsegments

	VirtualSegment* currentHead = vhead;
	VirtualSegment* prevHead = NULL;

	while (currentHead != NULL) {
		prevHead = currentHead->prevHead;
		delete currentHead;
		currentHead = prevHead;
	}

}

/**
 * Populate the given protocol buffer with various log metrics.
 *
 * \param[out] m
 *      The protocol buffer to fill with metrics.
 */
void
Log::getMetrics(ProtoBuf::LogMetrics& m)
{
    AbstractLog::getMetrics(m);
    m.set_total_sync_calls(metrics.totalSyncCalls);
    m.set_total_sync_ticks(metrics.totalSyncTicks);
}

/**
 * Return the position of the current log head. kera:not used
 */
LogPosition
Log::getHead() {
    SpinLock::Guard _(appendLock);
    return LogPosition(head->id, head->getAppendedLength());
}

/**
 *  VirtualSegment - work with vhead
 * Allocate a new head segment, changing the ``head'' field. If the allocation
 * succeeds and the allocated segment is writable (that is, not an emergency
 * head segment), return true. Otherwise, return false.
 *
 * This method centralizes a bit of subtle logic shared between the append
 * methods (the ``head'' field should never be NULL after the first segment
 * has been allocated).
 *
 * prevHead corresponds to last virtual segment head that keeps replicated segments
 *
 */
bool
Log::allocNewWritableHead(VirtualSegment* prevHead)
{
	//	should allocate a virtual segment with its replicated segment
	//logs are cleaned and are limited by disk size
	//at some point virtual segments are cleaned as well - todo through SegmentManager

	ReplicatedSegment* prevReplicatedSegment = NULL;

	//check prevHead->segment has more space
	if(NULL == prevHead) {
		head = segmentManager->allocVLogSegment(NULL);
		if (head == NULL) {
			//we never lose any groupId
			RAMCLOUD_CLOG(NOTICE, "No clean segments available; deferring "
									"operations until cleaner runs");
			return false; //we never create an emergency head, clients just retry, cleaner should make his job
		}
		segmentManager->writeHeaderDigest(logId, streamId, 0, 0UL, head, NULL);
	} else {
		prevReplicatedSegment = prevHead->replicatedSegment;
		//check that head contains enough storage for future chunks
		if(head->getFreeSpace() < 200+sizeof32(ChunkEntryDigest)) { //at least 1 chunk and virtual segment headers check VirtualSegment - openLen is 84 + additional 8B for prevVirtualSegment id in digest
			LogSegment* prevLogHead = head;
			head = segmentManager->allocVLogSegment(prevLogHead);
			if (head == NULL) {
				//we never lose any groupId
				RAMCLOUD_CLOG(NOTICE, "No clean segments available; deferring "
										"operations until cleaner runs");
				return false; //we never create an emergency head, clients just retry, cleaner should make his job
			}
			segmentManager->writeHeaderDigest(logId, streamId, 0, 0UL, head, prevLogHead);
		}
	}

	// pass head reference to vhead; head is the storage for vhead's chunks
	vhead = new VirtualSegment(masterId, logId,
		streamId, nextSegmentId++, segmentSize, head, prevHead, segmentManager);

	// Allocate a new ReplicatedSegment to handle backing up the new head. This
	// call will also sync the initial data (header, digest, etc) to the needed
	// number of replicas before returning.
	vhead->replicatedSegment = replicaManager->allocateHead(
			vhead->id, vhead, prevReplicatedSegment);

	if(prevHead != NULL) {
		prevHead->close();
		prevHead->replicatedSegment->close();

//		//see ReplicaManager destroyAndFreeReplicatedSegment
//		VirtualSegment* prevprevHead = prevHead->prevHead;
//		//only keep head and prevHead, for recovery we leverage log's vsegments state
//		while(prevprevHead != NULL && !prevprevHead->syncToHead) {
//			prevprevHead = prevprevHead->prevHead;
//		}
//		while(prevprevHead != NULL && prevprevHead->syncToHead) {
//			prevHead = prevprevHead->prevHead;
//			prevprevHead->replicatedSegment->free(); //no impact on backup replica
//			delete prevprevHead;
//			prevprevHead = prevHead;
//		}

	}
	return true;
}

void
Log::appendChunk(CEDSegment* chunk) {
	SpinLock::Guard _(appendLock);

	assert(vhead != NULL);
	uint32_t chunkLength = chunk->ced.length;
	uint32_t vheadFreeSpace = vhead->getFreeSpace();

	if(vheadFreeSpace < chunkLength) {
		VirtualSegment* originalHead = vhead;
		allocNewWritableHead(originalHead);
	}

	uint32_t lastHeadOffset = head->getFreeSpace();

	if(lastHeadOffset > sizeof32(ChunkEntryDigest)) {
		vhead->addChunk(chunk, 0);
	} else {
		//get a new vSegment (simplified but loses some space)
//		VirtualSegment* originalHead = vhead;
//		allocNewWritableHead(originalHead);
//		vhead->addChunk(chunk);

		//alloc new vhead
		lastHeadOffset = head->segmentSize - lastHeadOffset;
		LogSegment* prevLogHead = head;
		head = segmentManager->allocVLogSegment(prevLogHead);
		if (head == NULL) {
			//we never lose any groupId
			RAMCLOUD_CLOG(NOTICE, "No clean segments available; deferring "
									"operations until cleaner runs");
			return; //we never create an emergency head, clients just retry, cleaner should make his job todo
		}
		segmentManager->writeHeaderDigest(logId, streamId, 0, 0UL, head, prevLogHead);

//		fprintf(stdout, ">>> appendChunk headId %u log vheadId %u vhead vlogHeadId %u \n", head->id, vhead->id, vhead->vlogHead->id);
//		fflush (stdout);

		vhead->addChunk(chunk, lastHeadOffset, head);
	}

}

void
Log::sync()
{
//not used
}

/**
 * virtual log sync
 * called at stream creation #syncStreamlet
 * called after multiWrite appendChunks/s to a virtual log
 *
 * Wait for all log appends made at the time this method is invoked to be fully
 * replicated to backups. If no appends have ever been done, this method will
 * allocate the first log head and sync it to backups.
 *
 * This method is thread-safe. If no sync operation is currently underway, the
 * caller will fully sync the log and return. Doing so will propagate any
 * appends done since the last sync, including those performed by other threads.
 *
 * If a sync operation is already underway, this method will block until it
 * completes. Afterwards it will either return immediately if a previous call
 * did the work for us, or, if more replication is needed, it will sync the full
 * log contents at that time, including any appends made since this method
 * started waiting. This lets us batch backup writes and improve throughput for
 * small entries.
 *
 * An alternative to batching writes would have been to pipeline replication
 * RPCs to backups. That would probably also work just fine, but results in
 * more RPCs and is more complicated (we'd need to keep track of various RPCs
 * that may arrive out-of-order). The log also currently operates strictly
 * in-order, so there'd be no opportunity for small writes to skip ahead of
 * large ones anyway.
 */
void
Log::syncLog()
{
    CycleCounter<uint64_t> __(&PerfStats::threadStats.logSyncCycles);

    Tub<SpinLock::Guard> lock;
    lock.construct(appendLock);
    metrics.totalSyncCalls++;

    // The only time 'head' should be NULL is after construction and before the
    // initial call to this method. Even if we run out of memory in the future,
    // head will remain valid.
    if (vhead == NULL) { //head in KERAI is for each group
        assert(metrics.totalSyncCalls == 1);
        allocNewWritableHead(NULL); //todo if this fails (no available space, I should check and fail somewhere
    		return;
    }

    // Get the current log offset and assume this is the point we want to sync
    // to. It may include more data than our thread's previous appends due to
    // races between the append() call and this sync(), but it's both unlikely
    // and does not affect correctness. We simply may do a little more work
    // sometimes when the log really is synced up to our appends, but this logic
    // mistakes additional data from other threads' appends as stuff we care
    // about.
    uint32_t appendedLength = vhead->getAppendedLength();

    // Concurrent appends may cause the head segment to change while we wait
    // for another thread to finish syncing, so save the segment associated
    // with the appendedLength we just acquired.
    VirtualSegment* originalHead = vhead;

	// If segment != head, segment must have been closed and its replication
	// is queued already. Forcing sync of head segment will also make sure
	// that the closed segment is fully replicated.  This means we may have
    // multiple segments closed but not fully replicated, however sync called on
    //head makes things happen anyway due to ReplicaManager replicatedSegment.

    // We have a consistent view of the current head segment, so drop the append
    // lock and grab the sync lock. This allows other writers to append to the
    // log while we wait. Once we grab the sync lock, take the append lock again
    // to ensure our new view of the head is consistent.
    lock.destroy();
    SpinLock::Guard _(syncLock);
    lock.construct(appendLock);

	// See if we still have work to do. It's possible that another thread
	// already did the syncing we needed for us.
	if (appendedLength > originalHead->syncedLength) {
		// Get the latest segment length and certificate. This allows us to
		// batch up other appends that came in while we were waiting.
		SegmentCertificate certificate;
		appendedLength = originalHead->getAppendedLength(&certificate);

		// Drop the append lock. We don't want to block other appending threads
		// while we sync.
		lock.destroy();

//		uint64_t startInsertSec = Cycles::rdtsc();

		//replicate up to appendedLength
		originalHead->replicatedSegment->sync(appendedLength, &certificate); //wait

//		uint64_t stopInsertSec = Cycles::rdtsc();
//		double time = static_cast<double>(Cycles::toNanoseconds(stopInsertSec - startInsertSec));
//		fprintf(stdout, ">>> replication nanoseconds %.0f time \n", time);
//		startInsertSec = Cycles::rdtsc();

		originalHead->syncedLength = appendedLength;

		//at this point also previous segments are guaranteed to be replicated but their physical segments not yet synced
		std::vector<VirtualSegment*> prevHeadsToSync;
		VirtualSegment* prevHead = originalHead->prevHead;
		while (prevHead != NULL) {
			//check prevHead/s and syncTo them if needed
			if (!prevHead->syncToHead.load(std::memory_order_relaxed)) {
				prevHeadsToSync.push_back(prevHead);
				prevHead = prevHead->prevHead;
			} else {
				break;
			}
		}

        //sync durableHead on chunks' segments; this gives readers food
		//start from oldest impacts readers/correctness - all these previous segments are closed and replicated, optimize syncTo (cache physicalSegmentId - length)
		for (size_t ci = prevHeadsToSync.size(); ci > 0; ci--) {
			uint32_t prevAppendedLength =
					prevHeadsToSync[ci - 1]->getAppendedLength();
			prevHeadsToSync[ci - 1]->syncTo(prevAppendedLength);
		}

        originalHead->syncTo(appendedLength);

//		stopInsertSec = Cycles::rdtsc();
//		time = static_cast<double>(Cycles::toNanoseconds(stopInsertSec - startInsertSec));
//		fprintf(stdout, ">>> repl sync nanoseconds %.0f time \n", time);
//		fflush (stdout);

        TEST_LOG("log synced");
    } else {
        TEST_LOG("sync not needed: already fully replicated");
    }
}

/**
 * not used
 * Ensures the given log entry is fully replicated to backups. It will return
 * immediately if the entry is already replicated before. If the entry is not
 * yet replicated, we sync all log appends in current head segment with backups.
 *
 * Like sync(), this method is also thread-safe.
 */
void
Log::syncTo(Log::Reference reference)
{
//    CycleCounter<uint64_t> __(&PerfStats::threadStats.logSyncCycles);
//    metrics.totalSyncCalls++;
//
//    LogSegment* segment = getSegment(reference);
//
//    // Fast path: see if the log reference we want to sync is in
//    //            a segment that is already closed and synced.
//    if (segment->closedCommitted.load(std::memory_order_relaxed)) {
//        TEST_LOG("sync not needed: entry is already replicated");
//        return;
//    }
//
//    // Calculate the desired syncedLength, which is the end location of the
//    // given log entry in terms of segment offset.
//    uint32_t offset = segment->getOffset(reference);
//    uint32_t lengthWithMetadata;
//    segment->getEntry(offset, NULL, &lengthWithMetadata);
//    uint32_t desiredSyncedLength = offset + lengthWithMetadata;
//
//    SpinLock::Guard _(syncLock);
//
//    // See if we still have work to do. It's possible that another thread
//    // already did the syncing we needed for us.
//    if (desiredSyncedLength > segment->syncedLength) {
//        Tub<SpinLock::Guard> lock;
//        lock.construct(appendLock);
//
//        // Get the latest segment length and certificate. This allows us to
//        // batch up other appends that came in while we were waiting.
//        SegmentCertificate certificate;
//        // If segment != head, segment must have been closed and its replication
//        // is queued already. Forcing sync of head segment will also make sure
//        // that the closed segment is fully replicated.
//        uint32_t appendedLength = head->getAppendedLength(&certificate);
//
//        // Drop the append lock. We don't want to block other appending
//        // threads while we sync.
//        lock.destroy();
//
//        head->replicatedSegment->sync(appendedLength, &certificate);
//        head->syncedLength = appendedLength;
//        TEST_LOG("log synced");
//        return;
//    }
    TEST_LOG("sync not needed: entry is already replicated");
}

/**
 * not used
 * Force the log to roll over to a new head and return the new log position.
 * At the instant of the new head segment's creation, it will have the highest
 * segment identifier in the log. The log is guaranteed to be synced to this
 * point on backups.
 *
 * This method can be used to ensure that the head segment is advanced past any
 * survivor segments generated by the cleaner. This can be used to then
 * demarcate parts of the log that can and cannot contain certain data. For
 * example, when a tablet is migrated to a new machine, it rolls over the head
 * and records the position. All data in the tablet must exist at that position
 * or after it. If there were an old instance of the same tablet, we would not
 * want to recover that data if a failure occurrs. Fortunately, its data would
 * be at strictly lower positions in the log, so it's easy to filter during
 * recovery.
 */
LogPosition
Log::rollHeadOver()
{
//    SpinLock::Guard lock(syncLock);
//    SpinLock::Guard lock2(appendLock);
//
//    // Allocate the new head and sync the log. This will ensure that the
//    // position returned is stable on backups. This is paricularly important
//    // for SideLog::commit(), which rolls the head over to inject a SideLog
//    // into the main log (by adding segments to a new log digest and syncing
//    // that to disk). See RAM-489.
//    head = allocNextSegment(true);
//    SegmentCertificate certificate;
//    uint32_t appendedLength = head->getAppendedLength(&certificate);
//    head->replicatedSegment->sync(appendedLength, &certificate);
//    head->syncedLength = appendedLength;

    return LogPosition(head->id, head->getAppendedLength());
}

} // namespace
