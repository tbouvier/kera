/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIWRITEOBJECT_H
#define RAMCLOUD_MULTIWRITEOBJECT_H

#include "Minimal.h"
#include "MultiOpObject.h"
#include "RamCloud.h"
#include "RejectRules.h"

namespace KerA {

/**
 * Objects of this class are used to pass parameters into \c multiWrite
 * and for multiWrite to return status values for conditional operations
 * (if used).
 */
struct MultiWriteObject : public MultiOpObject {
    /**
     * Pointer to the contents of the new object.
     */
    const void* value;

    /**
     * Length of value in bytes.
     */
    uint32_t valueLength;

    /**
    * Number of keys in this multiWrite object
    */
    uint8_t numKeys;

    /**
     * List of keys and their lengths part of this multiWrite object.
     * This will be NULL for single key multiwrite objects
     */
    KeyInfo *keyInfo;

    /**
     * The RejectRules specify when conditional writes should be aborted.
     */
    const RejectRules* rejectRules;

    /**
     * The version number of the newly written object is returned here.
     */
    uint64_t version;

    // KERAI: see WireFormat::MultiOp::Request::WritePart for stream partitioning fields
    bool useNextGroup;
    bool allowSharedGroups;
    uint64_t producerId;

    /**
     * Typically used when each object has a single key.
     *
     * \param tableId
     *      The table containing the desired object (return value from
     *      a previous call to getTableId).
     * \param key
     *      Variable length key that uniquely identifies the object within tableId.
     *      It does not necessarily have to be null terminated.  The caller must
     *      ensure that the storage for this key is unchanged through the life of
     *      the RPC.
     * \param keyLength
     *      Size in bytes of the key.
     * \param value
     *      Address of the first byte of the new contents for the object;
     *      must contain at least length bytes.
     * \param valueLength
     *      Size in bytes of the value of the object.
     * \param rejectRules
     *      If non-NULL, specifies conditions under which the write
     *      should be aborted with an error.
     */
    MultiWriteObject(uint64_t tableId, const void* key, uint16_t keyLength,
                 const void* value, uint32_t valueLength, bool useNextGroup = true, bool allowSharedGroups = false, uint64_t producerId = -1,
                 const RejectRules* rejectRules = NULL)
        : MultiOpObject(tableId, key, keyLength)
        , value(value)
        , valueLength(valueLength)
        , numKeys(1)
        , keyInfo(NULL)
        , rejectRules(rejectRules)
        , version()
        , useNextGroup(useNextGroup)
        , allowSharedGroups(allowSharedGroups)
        , producerId(producerId)
    {}

    /**
     * Typically used when each object has multiple keys.
     * \param tableId
     *      The table containing the desired object (return value from
     *      a previous call to getTableId).
     * \param value
     *      Address of the first byte of the new contents for the object;
     *      must contain at least length bytes.
     * \param valueLength
     *      Size in bytes of the value of the object.
     * \param numKeys
     *      Number of keys in the object.  If is not >= 1, then behavior
     *      is undefined. A value of 1 indicates the presence of only the
     *      primary key
     * \param keyInfo
     *      List of keys and corresponding key lengths. The first entry should
     *      correspond to the primary key and its length. If this argument is
     *      NULL, then behavior is undefined. RamCloud currently uses a dense
     *      representation of key lengths. If a client does not want to write
     *      key_i in an object, keyInfo[i]->key should be NULL. The library also
     *      expects that if keyInfo[j]->key exists and keyInfo[j]->keyLength
     *      is 0, then key string is NULL terminated and the length is computed.
     * \param rejectRules
     *      If non-NULL, specifies conditions under which the write
     *      should be aborted with an error.
     *
     *      KERAI: does not implement stream partitioning
     */
    MultiWriteObject(uint64_t tableId,
            const void* value, uint32_t valueLength,
            uint8_t numKeys, KeyInfo *keyInfo,
            const RejectRules* rejectRules = NULL)
        : MultiOpObject(tableId, NULL, 0)
        , value(value)
        , valueLength(valueLength)
        , numKeys(numKeys)
        , keyInfo(keyInfo)
        , rejectRules(rejectRules)
        , version()
        , useNextGroup(true)
        , allowSharedGroups(false)
        , producerId(-1)
    {}

    MultiWriteObject()
        : MultiOpObject()
        , value()
        , valueLength()
        , numKeys()
        , keyInfo()
        , rejectRules()
        , version()
        , useNextGroup()
        , allowSharedGroups()
        , producerId(-1)
    {}

    MultiWriteObject(const MultiWriteObject& other)
        : MultiOpObject(other)
        , value(other.value)
        , valueLength(other.valueLength)
        , numKeys(other.numKeys)
        , keyInfo(other.keyInfo)
        , rejectRules(other.rejectRules)
        , version(other.version)
        , useNextGroup(other.useNextGroup)
        , allowSharedGroups(other.allowSharedGroups)
        , producerId(other.producerId)
    {}

    MultiWriteObject& operator=(const MultiWriteObject& other) {
        MultiOpObject::operator =(other);
        value = other.value;
        valueLength = other.valueLength;
        numKeys = other.numKeys;
        // shallow copy should be good enough
        keyInfo = other.keyInfo;
        rejectRules = other.rejectRules;
        version = other.version;
        useNextGroup = other.useNextGroup;
        allowSharedGroups = other.allowSharedGroups;
        producerId = other.producerId;
        return *this;
    }
};

}

#endif