/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "Crc32C.h"
#include "StreamObject.h"
#include "RamCloud.h"

namespace KerA {

StreamObject::StreamObject(uint64_t tableId,
               Buffer& keysAndValueBuffer)
    : //headerGroup(),
      keysAndValueLength(),
      keysAndValue(),
      keysAndValueBuffer(&keysAndValueBuffer),
      keysAndValueOffset(),
      keyOffsets(NULL)
{
    keysAndValueLength = 0;
}

void
StreamObject::reset(uint32_t startDataOffset,
        uint32_t length)
{
	keyOffsets = NULL;
	keysAndValueOffset = startDataOffset;

	keysAndValueLength = length;
    void* retPtr;
    if (keysAndValueBuffer->peek(startDataOffset, &retPtr) >= keysAndValueLength)
        keysAndValue = static_cast<char*>(retPtr);
}

StreamObject::StreamObject(uint64_t tableId,
               uint64_t version,
               uint32_t timestamp,
               Buffer& keysAndValueBuffer,
               uint32_t startDataOffset,
               uint32_t length)
    : //headerGroup(),
      keysAndValueLength(),
      keysAndValue(),
      keysAndValueBuffer(&keysAndValueBuffer),
      keysAndValueOffset(startDataOffset),
      keyOffsets(NULL)
{
    // compute the actual default value
    if (length == 0)
        length = this->keysAndValueBuffer->size() - startDataOffset;

    keysAndValueLength = length;

    void* retPtr;
    if (keysAndValueBuffer.peek(startDataOffset, &retPtr) >= keysAndValueLength)
        keysAndValue = static_cast<char*>(retPtr);
}

StreamObject::StreamObject(Buffer& buffer, uint32_t offset, uint32_t length)
    : //headerGroup(), //*buffer.getOffset<HeaderGroup>(offset)
      keysAndValueLength(),
      keysAndValue(),
      keysAndValueBuffer(&buffer),
      keysAndValueOffset(offset), //+ sizeof32(headerGroup)
      keyOffsets(NULL)
{
    // If length is not specified, compute the length of keysAndValue
    if (length == 0)
        keysAndValueLength = buffer.size() - offset; // - sizeof32(headerGroup);
    else
        keysAndValueLength = length; // - sizeof32(headerGroup);

    void* retPtr;
    if (buffer.peek(offset, &retPtr) >= keysAndValueLength) //buffer.peek(offset + sizeof32(headerGroup)
        keysAndValue = static_cast<char*>(retPtr);
}

void
StreamObject::assembleForLog(Buffer& buffer, bool useNewHeader)
{
//    headerGroup.checksum = computeChecksumGroup();
//    buffer.append(&headerGroup, sizeof32(headerGroup));
    appendKeysAndValueToBuffer(buffer);
}


/**
 * Append the the value associated with this object to a provided buffer.
 * This is may be a virtual copy or it may be a hard copy of the value.
 * The caller must ensure that the source (of the value) remains valid
 * as long as the buffer exists in the case of a virtual copy.
 *
 * \param buffer
 *      The buffer to append the value to.
 */
void
StreamObject::appendValueToBuffer(Buffer* buffer)
{
    uint32_t valueOffset;
    getValueOffset(&valueOffset);

    // Prioritize using the keysAndValueBuffer to do a buffer-to-buffer
    // copy as the Buffer class contains additional logic to safely
    // append data from another buffer (RAM-688)
    if (keysAndValueBuffer) {
        buffer->append(keysAndValueBuffer, keysAndValueOffset + valueOffset,
                getValueLength());
        return;
    }

    const uint8_t *ptr = reinterpret_cast<const uint8_t *>(keysAndValue);
    buffer->append(ptr + valueOffset, getValueLength());
}

/**
 * Append the cumulative key lengths, the keys and the value associated with
 * this object to a provided buffer. This is may be a virtual copy or it may
 * be a hard copy of the keys and value. The caller must ensure that the
 * source (of the keys and value) remains valid as long as the buffer
 * exists in the case of a virtual copy.
 *
 * \param buffer
 *      The buffer to append the keys and the value to.
 */
void
StreamObject::appendKeysAndValueToBuffer(Buffer& buffer)
{
    // Prioritize using the keysAndValueBuffer to do a buffer-to-buffer
    // copy as the Buffer class contains additional logic to safely
    // append data from another buffer (RAM-688)
    if (keysAndValueBuffer)
        // keysAndValueBuffer contains keyLengths, keys and value starting
        // at keysAndValueOffset
        buffer.append(keysAndValueBuffer, keysAndValueOffset,
                keysAndValueLength);
    else
        buffer.append(keysAndValue, keysAndValueLength);
}

/**
 * The typical use case for this function is during a write RPC when we want
 * the RPC payload to mirror the format of the object in the log as much as
 * possible. This is primarily used by the RamCloud library to make sure the
 * format of the object does not leak outside this class. It is also used by
 * the MultiWrite framework for the same purpose.
 *
 * \param tableId
 *      The tableId corresponding to this object.
 * \param numKeys
 *      The number of keys in this object.
 * \param keyList
 *      List of keys and key length values as provided by the end client
 * \param value
 *      Pointer to a single contiguous piece of memory that comprises this
 *      object's value.
 * \param valueLength
 *      Length of the value portion in bytes.
 * \param [out] request
 *      A buffer that can be used temporarily to store the keys and value
 *      for the object. Its lifetime must cover the lifetime of this Object.
 * \param [out] length
 *      Total length of keysAndValue
 */
void
StreamObject::appendKeysAndValueToBuffer(
        uint64_t tableId, KeyCount numKeys, KeyInfo *keyList,
        const void* value, uint32_t valueLength, Buffer* request,
        uint32_t *length)
{
    int i;
    uint32_t totalKeyLength = 0;
    uint32_t currentKeyLength = 0;
    if (keyList) {
        // allocate memory first for number of keys and all the cumulative
        // length values
        KeyOffsets *keyOffsetsHelper = reinterpret_cast<KeyOffsets *>(
                                    request->alloc(KEY_INFO_LENGTH(numKeys)));
        keyOffsetsHelper->numKeys = numKeys;
        CumulativeKeyLength *cumLengths = keyOffsetsHelper->cumulativeLengths;


        for (i = 0; i < numKeys; i++) {
            // if the length of a key is 0, we expect the corresponding key
            // is NULL terminated and hence compute the length

            if (!keyList[i].key) { // this key does not exist
                currentKeyLength = 0;
            } else if (!keyList[i].keyLength) {
                currentKeyLength = downCast<uint32_t>(
                                        strlen(static_cast<const char *>(
                                        keyList[i].key)));
            } else {
                currentKeyLength = keyList[i].keyLength;
            }
            // primary key must always exist
            if (i == 0)
                cumLengths[i] = downCast<CumulativeKeyLength>(
                                            currentKeyLength);
            else
                cumLengths[i] = downCast<CumulativeKeyLength>(
                                    cumLengths[i-1] +
                                    currentKeyLength);
            totalKeyLength += currentKeyLength;
        }
        void *keys = request->alloc(totalKeyLength);
        uint8_t *dest = reinterpret_cast<uint8_t *>(keys);
        for (i = 0; i < numKeys; i++) {
            // this key doesn't exist
            if (!keyList[i].key)
                continue;

            if (!keyList[i].keyLength) {
                currentKeyLength = downCast<uint32_t>(
                                        strlen(static_cast<const char *>(
                                        keyList[i].key)));
            } else {
                currentKeyLength = keyList[i].keyLength;
            }
            memcpy(dest, keyList[i].key, currentKeyLength);
            dest = dest + currentKeyLength;
        }
        request->append(value, valueLength);
        if (length)
            *length = KEY_INFO_LENGTH(numKeys) +
                      totalKeyLength + valueLength;
    }
}

/**
 * This is primarily used by the write RPC and the increment RPC handler in
 * MasterService when the objects have just a single key. It is also used by
 * unit tests. This is typically invoked when one does not require an instance
 * of class Object
 *
 * \param key
 *      Primary key for this object
 * \param value
 *      Pointer to a single contiguous piece of memory that comprises this
 *      object's value.
 * \param valueLength
 *      Length of the value portion in bytes.
 * \param buffer
 *      A buffer that can be used temporarily to store the keys and value
 *      for the object. Its lifetime must cover the lifetime of this Object.
 * \param appendCopy
 *      If true, keys and values will be copied to the end of buffer.  If false,
 *      value will be appended using appendExternal.
 * \param [out] length
 *      Total length of keysAndValue
 */
void
StreamObject::appendKeysAndValueToBuffer(
        Key& key, const void* value, uint32_t valueLength,
        Buffer* buffer, bool appendCopy, uint32_t *length)
{
    uint32_t primaryKeyInfoLength =
            KEY_INFO_LENGTH(1) + key.getStringKeyLength();

    if (length)
        *length = primaryKeyInfoLength + valueLength;

    uint8_t* keyInfo = static_cast<uint8_t*>(
            buffer->alloc(primaryKeyInfoLength));

    KeyCount keyCount = 1;
    KeyLength keyLength = static_cast<KeyLength>(key.getStringKeyLength());
    const void *keyString = key.getStringKey();
    memcpy(keyInfo, &keyCount, sizeof(KeyCount));
    memcpy(keyInfo + sizeof(KeyCount), &keyLength, sizeof(KeyLength));
    memcpy(keyInfo + KEY_INFO_LENGTH(1), keyString, keyLength);

    if (appendCopy) {
        buffer->appendCopy(value, valueLength);
    } else {
        buffer->appendExternal(value, valueLength);
    }
}

/**
 * This is primarily used by the write RPC and the increment RPC handler in
 * MasterService when the objects have just a single key. It is also used by
 * unit tests. This is typically invoked when one does not require an instance
 * of class Object
 *
 * \param key
 *      Primary key for this object
 * \param value
 *      Pointer to a single contiguous piece of memory that comprises this
 *      object's value.
 * \param valueLength
 *      Length of the value portion in bytes.
 * \param buffer
 *      A buffer that can be used temporarily to store the keys and value
 *      for the object. Its lifetime must cover the lifetime of this Object.
 * \param appendCopy
 *      If true, keys and values will be copied to the end of buffer.  If false,
 *      value will be appended using appendExternal.
 * \param [out] length
 *      Total length of keysAndValue
 */
void
StreamObject::appendHeaderKeysAndValueToBuffer(
        Key& key, const void* value, uint32_t valueLength,
        Buffer* buffer, bool appendCopy, uint32_t *length)
{
//	HeaderGroup objHeader; // = new HeaderGroup();
//    //update checksum for headerGroup
//    Crc32C crc;
//	// first compute the checksum on the object header excluding the
//	// checksum field
//	crc.update(reinterpret_cast<void *>(
//			   reinterpret_cast<uint8_t*>(
//			   &objHeader) + sizeof(objHeader.checksum)),
//			   downCast<uint32_t>(sizeof(objHeader) -
//			   sizeof(objHeader.checksum)));
//
//	// then compute the checksum on keysAndValue.
//	crc.update(value, valueLength);
//	objHeader.checksum = crc.getResult();
//
//    buffer->append(&objHeader, sizeof32(objHeader));

    uint32_t primaryKeyInfoLength =
            KEY_INFO_LENGTH(1) + key.getStringKeyLength();

    if (length)
        *length = primaryKeyInfoLength + valueLength; // + sizeof32(objHeader);

    uint8_t* keyInfo = static_cast<uint8_t*>(
            buffer->alloc(primaryKeyInfoLength));

    KeyCount keyCount = 1;
    KeyLength keyLength = static_cast<KeyLength>(key.getStringKeyLength());
    const void *keyString = key.getStringKey();
    memcpy(keyInfo, &keyCount, sizeof(KeyCount));
    memcpy(keyInfo + sizeof(KeyCount), &keyLength, sizeof(KeyLength));
    memcpy(keyInfo + KEY_INFO_LENGTH(1), keyString, keyLength);

    if (appendCopy) {
        buffer->appendCopy(value, valueLength);
    } else {
    	buffer->append(value, valueLength);
//        buffer->appendExternal(value, valueLength);
    }

}

uint32_t
StreamObject::getLengthHeaderKeysAndValue(Key& key, uint32_t valueLength)
{
	uint32_t primaryKeyInfoLength =
	            KEY_INFO_LENGTH(1) + key.getStringKeyLength();
	return primaryKeyInfoLength + valueLength + sizeof32(HeaderGroup); //HeaderGroup is 6B overhead
}

/**
 * Populate the keyOffsets structure so that it makes operations like
 * getKey() efficient. It is a NO-OP if is already populated.
 *
 * \return
 *      False if keysAndValueBuffer is not big enough to hold information
 *      for all the keys, True otherwise
 */
bool
StreamObject::fillKeyOffsets()
{
    if (!keyOffsets) {
        if (keysAndValue) {
            keyOffsets = static_cast<const struct KeyOffsets *>(
                                keysAndValue);
        } else {
            KeyCount numKeys = *(keysAndValueBuffer->getOffset<KeyCount>(
                                keysAndValueOffset));
            keyOffsets = static_cast<const struct KeyOffsets *>(
                                keysAndValueBuffer->getRange(
                                keysAndValueOffset, KEY_INFO_LENGTH(numKeys)));
            // Check if the keysAndValueBuffer had all the information required.
            if (!keyOffsets)
                return false;
        }
    }
    return true;
}

/**
 * Returns a pointer to one of the object's keys. The key is guaranteed to
 * be in contiguous memory (if it wasn't already contiguous, it will be
 * copied into a contiguous region).
 *
 * \param keyIndex
 *      Index position of this key
 * \param[out] keyLength
 *      Pointer to word that will be filled in with the length of the key
 *      indicated by keyIndex; if NULL then no length is returned.
 *
 * \return
 *      Pointer to the key which will be contiguous, or NULL if there is no
 *      key corresponding to keyIndex
 */
const void*
StreamObject::getKey(KeyIndex keyIndex, KeyLength *keyLength)
{
    if (!fillKeyOffsets())
        return NULL;

    if (keyIndex >= keyOffsets->numKeys)
        return NULL;

    uint32_t firstKeyPos = KEY_INFO_LENGTH(keyOffsets->numKeys);

    uint32_t keyOffset; // 0 corresponds to the starting of keysAndValue
    uint32_t length;
    const CumulativeKeyLength *cumLengths = keyOffsets->cumulativeLengths;
    if (keyIndex == 0) {
        keyOffset = firstKeyPos;
        length = cumLengths[0];
    } else {
        keyOffset = firstKeyPos + cumLengths[keyIndex - 1];
        length = cumLengths[keyIndex] - cumLengths[keyIndex - 1];
    }
    // key does not exist
    if (length == 0)
        return NULL;

    if (keyLength)
        *keyLength = downCast<KeyLength>(length);

    // check bounds
    if (keyOffset > keysAndValueLength)
        return NULL;

    // now that we have the key offset and key length, just return a pointer
    // to the key
    if (keysAndValue)
        return static_cast<const uint8_t *>(keysAndValue) + keyOffset;
    else
        return keysAndValueBuffer->getRange(keyOffset + keysAndValueOffset,
                length);
}

/**
 * Obtain the length of the key at position keyIndex.
 * If keyIndex >= numKeys, then endKeyOffset(keyIndex) will
 * return 0. This function should return 0 in such cases.
 *
 * \param keyIndex
 *      Numeric position of the index
 */
KeyLength
StreamObject::getKeyLength(KeyIndex keyIndex)
{
    if (!fillKeyOffsets())
        return 0;

    if (keyIndex >= keyOffsets->numKeys)
        return 0;

    const CumulativeKeyLength *cumLengths = keyOffsets->cumulativeLengths;
    if (keyIndex == 0)
        return cumLengths[0];
    else
        return static_cast<KeyLength>(cumLengths[keyIndex] -
                   cumLengths[keyIndex - 1]);
}

/**
 * Obtain a pointer to a contiguous copy of this object's value.
 * If the value is not already contiguous, it will be copied.
 * This will include the number of keys, the key lengths and the keys
 * along with the value. NOTE: This might be an expensive operation
 * depending on the number of keys and the size of the value
 */
const void*
StreamObject::getKeysAndValue()
{
    if (keysAndValue)
        return keysAndValue;

    return (keysAndValueBuffer)->getRange(keysAndValueOffset,
                                          keysAndValueLength);
}

/**
 * Get number of keys in this object.
 * \return
 *      Number of keys in this object.
 */
KeyCount
StreamObject::getKeyCount()
{
    if (!fillKeyOffsets())
        return 0;
    return keyOffsets->numKeys;
}

/**
 * Obtain a pointer to a contiguous copy of this object's value.
 * This will not contain the number of keys, the key lengths and the keys.
 * This function is primarily used by unit tests
 *
 * \param[out] valueLength
 *      The length of the object's value in bytes.
 *
 * \return
 *      NULL if the object is malformed,
 *      a pointer to a contiguous copy of the object's value otherwise
 */
const void*
StreamObject::getValue(uint32_t *valueLength)
{
    if (!fillKeyOffsets())
        return NULL;

    const CumulativeKeyLength *cumLengths = keyOffsets->cumulativeLengths;
    // To calculate the starting position of the value, we have to account for
    // the number of keys, all the cumulative length values and total length
    // of all the keys. The total length of all the keys is given by the
    // cumulative length value at the last key position.
    uint32_t valueOffset = KEY_INFO_LENGTH(keyOffsets->numKeys) +
                           cumLengths[keyOffsets->numKeys - 1];
    uint32_t valueLen = keysAndValueLength - valueOffset;

    if (valueLength)
        *valueLength = valueLen;

    // checks for bogus cumulative key length values
    if (valueLen + valueOffset > keysAndValueLength)
        return NULL;

    if (keysAndValue)
        return static_cast<const uint8_t*>(keysAndValue) + valueOffset;
    else
        return keysAndValueBuffer->getRange(keysAndValueOffset + valueOffset,
                                            valueLen);
}

/**
 * Obtain the offset of the object's value in the keysAndValue portion of
 * the object.
 *
 * \param[out] offset
 *      The offset of the value within keysAndValue.
 *
 * \return
 *      False if the object is malformed, True otherwise
 */
bool
StreamObject::getValueOffset(uint32_t *offset)
{
    if (!fillKeyOffsets())
        return false;
    const CumulativeKeyLength *cumLengths = keyOffsets->cumulativeLengths;
    // To calculate the starting position of the value, we have to account for
    // the number of keys, all the cumulative length values and total length
    // of all the keys.
    uint32_t valueOffset = KEY_INFO_LENGTH(keyOffsets->numKeys) +
                           cumLengths[keyOffsets->numKeys - 1];
    // IMPORTANT:
    // here we do not add keysAndValueOffset because getValueOffset()
    // is called only after a readKeysAndValueRpc and it should be relative
    // to the starting of keysAndValue
    if (offset)
        *offset = valueOffset;
    return true;
}

/**
 * Obtain the length of the object's value
 */
uint32_t
StreamObject::getValueLength()
{
    uint32_t valueOffset;
    if (!getValueOffset(&valueOffset))
        return 0;
    return keysAndValueLength - valueOffset;
}

/**
 * Obtain the length of the keys and the value associated with this object.
 */
uint32_t
StreamObject::getKeysAndValueLength()
{
    return keysAndValueLength;
}

/**
 * Obtain the total size of the object including the object header
 */
uint32_t
StreamObject::getSerializedLength()
{
    return keysAndValueLength; //sizeof32(headerGroup) +
}

/**
 * Compute a checksum on the object and determine whether or not it matches
 * what is stored in the object. Returns true if the checksum looks ok,
 * otherwise returns false. not required todo remove
 */
bool
StreamObject::checkIntegrity()
{
    return true; //computeChecksumGroup() == headerGroup.checksum;
}

/**
 * Compute checksum onto the provided Crc32c instance. This function may be
 * used to calculate checksum of big chunk containing Object.
 *
 * \param   crc this function updates this Crc32C object according to the
 *          contents of object.
 */
void
StreamObject::applyChecksum(Crc32C *crc)
{
    // first compute the checksum on the object header excluding the
    // checksum field
//    crc->update(reinterpret_cast<void *>(
//               reinterpret_cast<uint8_t*>(
//               &headerGroup) + sizeof(headerGroup.checksum)),
//               downCast<uint32_t>(sizeof(headerGroup) -
//               sizeof(headerGroup.checksum)));

    // then compute the checksum on keysAndValue.
    if (keysAndValue) {
        crc->update(keysAndValue, keysAndValueLength);
    } else {
        crc->update(*keysAndValueBuffer, keysAndValueOffset,
                   getKeysAndValueLength());
    }
}

/**
 * Compute the object's checksum and return it.
 */
uint32_t
StreamObject::computeChecksumGroup()
{
    assert(OFFSET_OF(HeaderGroup, checksum) == 0);

    Crc32C crc;
    // first compute the checksum on the object header excluding the
    // checksum field
//    crc.update(reinterpret_cast<void *>(
//               reinterpret_cast<uint8_t*>(
//               &headerGroup) + sizeof(headerGroup.checksum)),
//               downCast<uint32_t>(sizeof(headerGroup) -
//               sizeof(headerGroup.checksum)));

    // then compute the checksum on keysAndValue.
    if (keysAndValue) {
        crc.update(keysAndValue, keysAndValueLength);
    } else {
        crc.update(*keysAndValueBuffer, keysAndValueOffset,
                   getKeysAndValueLength());
    }

    return crc.getResult();
}

uint32_t
StreamObject::computeChecksum(const StreamObject::HeaderGroup* object,
                        uint32_t totalLength)
{
    Crc32C crc;
//    crc.update(reinterpret_cast<const void *>(
//               reinterpret_cast<const uint8_t *>(
//               object) + sizeof(headerGroup.checksum)),
//               downCast<uint32_t>(sizeof(headerGroup) -
//               sizeof(headerGroup.checksum)));

    uint32_t len = totalLength - sizeof32(HeaderGroup);
    crc.update(&object->keysAndData[0], len);
    return crc.getResult();
}

} // namespace KerA
