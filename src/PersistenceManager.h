/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2009-2012 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef RAMCLOUD_PERSISTENCEMANAGER_H
#define RAMCLOUD_PERSISTENCEMANAGER_H

#include <thread>
#include <map>

#include "Common.h"
#include "BackupClient.h"
#include "BackupMasterRecovery.h"
#include "BackupStorage.h"
#include "CoordinatorClient.h"
#include "MasterClient.h"
#include "Service.h"
#include "ServerConfig.h"
#include "TaskQueue.h"

namespace KerA {

/**
 * moves master segments to disk from disk.
 */
class PersistenceManager {
  PUBLIC:
  PersistenceManager(Context* context, const ServerConfig* config);
    virtual ~PersistenceManager();
    void benchmark();
    uint32_t getReadSpeed() { return readSpeed; }

	bool writeSegment(const WireFormat::BackupWrite::Request* req,
			Buffer& source);

    void initOnceEnlisted(ServerId assignedServerId);
    void trackerChangesEnqueued();

    /**
     * Shared RAMCloud information.
     */
    Context* context;

    /**
     * Provides mutual exclusion between handling RPCs and garbage collector.
     * Locked once for all RPCs in dispatch().
     */
    std::mutex mutex;
    typedef std::mutex Mutex;
    typedef std::unique_lock<Mutex> Lock;

    /// Settings passed to the constructor
    const ServerConfig* config;

    /**
     * If the backup was formerly part of a cluster this was its server id.
     * This is extracted from a superblock that is part of BackupStorage.
     * "Rejoining" means this backup service may have segment replicas stored
     * that were created by masters in the cluster.
     * In this case, the coordinator must be made told of the former server
     * id under which these replicas were created in order to ensure that
     * all masters are made aware of the former server's crash before learning
     * of its re-enlistment.
     */
    ServerId formerServerId;
    ServerId serverId;

    /**
     * The storage backend where closed segments are to be placed.
     * Must come before #frames so that if the reference count of some frames
     * drops to zero when the map is destroyed won't have been destroyed yet
     * in the storage instance.
     */
  PUBLIC:
    std::unique_ptr<BackupStorage> storage;

  PRIVATE:

    /// Type of the key for the frame map.
    struct MasterSegmentIdPair {
        MasterSegmentIdPair(ServerId masterId, uint64_t logId, uint64_t segmentId)
            : masterId(masterId)
            ,  logId(logId)
            , segmentId(segmentId)
        {
        }

        /// Comparison is needed for the type to be a key in a map.
        bool
        operator<(const MasterSegmentIdPair& right) const
        {
            return *masterId < *right.masterId ||
            		( !(*masterId > *right.masterId) && std::make_pair(logId, segmentId) <
                   std::make_pair(right.logId, right.segmentId));
        }

        ServerId masterId;
        uint64_t logId; //unique per master
        uint64_t segmentId;
    };

    /// Type of the frame map.
    typedef std::map<MasterSegmentIdPair, BackupStorage::FrameRef> FrameMap;
    /**
     * Mapping from (MasterId, logId, SegmentId) to a BackupStorage::FrameRef for
     * replicas that are currently open or in storage.
     */
    FrameMap frames;

    /// The uniform size of each segment this backup deals with.
    const uint32_t segmentSize;

    /// The results of storage.benchmark() in MB/s.
    uint32_t readSpeed;

    /// For unit testing.
    uint64_t bytesWritten;

    /// Used to ensure that init() is invoked before the dispatcher runs.
    bool initCalled;

    /// Set during unit tests to skip the check that ensures the caller is
    /// actually in the cluster.
    bool testingSkipCallerIdCheck;

    DISALLOW_COPY_AND_ASSIGN(PersistenceManager);
};

} // namespace KerA

#endif
