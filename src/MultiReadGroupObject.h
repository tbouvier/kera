/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIREADGROUPOBJECT_H
#define RAMCLOUD_MULTIREADGROUPOBJECT_H

#include "Buffer.h"
#include "GroupSegmentInfo.h"
#include "Minimal.h"
#include "MultiOpObject.h"
#include "Tub.h"

namespace KerA {

/**
 * Objects of this class are used to pass parameters into \c multiReadGroup
 * and for multiReadGroup to return result values.
 * Each object represents a streamlet-group-segment; a request can contain one or many objects,
 * in order to pipeline many batches per multi-read-group request.
 */
struct MultiReadGroupObject : public MultiOpObject {

    //Contains groupId, segmentId, valueOnly, offset/position, maxObjects - given in input
    GroupSegmentInfo *segmentInfo;

    //response buffer is managed by application - shared by many objects;
    //next are its offset and length, initialized in response
    uint32_t responseOffset;
    uint32_t responseLength;
    uint32_t numberObjectsRead; // initialized in response
    uint32_t nextOffset; //contains offset to last read object, initialized in response
    uint32_t numberOfObjectEntries; // initialized in response
    bool isSegmentClosed; // initialized in response
    bool isGroupProcessed;
    
    // Gives the segment's objects, if initialized returns false.
    Tub<Buffer>* respvalue;
    
    MultiReadGroupObject(uint64_t tableId, uint64_t keyHash,
            Tub<Buffer>* respvalue)
        : MultiOpObject(tableId, keyHash)
        , segmentInfo(NULL)
        , responseOffset()
        , responseLength()
        , numberObjectsRead()
        , nextOffset()
        , numberOfObjectEntries()
        , isSegmentClosed()
        , isGroupProcessed()
        , respvalue(respvalue)
    {}

    MultiReadGroupObject(uint64_t tableId, uint64_t keyHash,
            Tub<Buffer>* respvalue, GroupSegmentInfo *segmentInfo)
        : MultiOpObject(tableId, keyHash)
        , segmentInfo(segmentInfo)
        , responseOffset()
        , responseLength()
        , numberObjectsRead()
        , nextOffset()
        , numberOfObjectEntries()
        , isSegmentClosed()
        , isGroupProcessed()
        , respvalue(respvalue)
    {}

    MultiReadGroupObject()
        : MultiOpObject()
        , segmentInfo()
        , responseOffset()
        , responseLength()
        , numberObjectsRead()
        , nextOffset()
        , numberOfObjectEntries()
        , isSegmentClosed()
        , isGroupProcessed()
        , respvalue()
    {}

    MultiReadGroupObject(const MultiReadGroupObject& other) :
        MultiOpObject(other), segmentInfo(other.segmentInfo),
        responseOffset(other.responseOffset), responseLength(other.responseLength),
        numberObjectsRead(other.numberObjectsRead), nextOffset(other.nextOffset),
        numberOfObjectEntries(other.numberOfObjectEntries),
        isSegmentClosed(other.isSegmentClosed),
        isGroupProcessed(other.isGroupProcessed),
        respvalue(other.respvalue) {
    }

    MultiReadGroupObject& operator=(const MultiReadGroupObject& other) {
        MultiOpObject::operator =(other);
        segmentInfo = other.segmentInfo;
        responseOffset = other.responseOffset;
        responseLength = other.responseLength;
        numberObjectsRead = other.numberObjectsRead;
        nextOffset = other.nextOffset;
        numberOfObjectEntries = other.numberOfObjectEntries;
        isSegmentClosed = other.isSegmentClosed;
        isGroupProcessed = other.isGroupProcessed;
        respvalue = other.respvalue;
        return *this;
    }
};

}

#endif