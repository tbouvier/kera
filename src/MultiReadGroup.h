/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIREADGROUP_H
#define RAMCLOUD_MULTIREADGROUP_H

#include "MultiOp.h"
#include "MultiOpObject.h"
#include "ShortMacros.h"

namespace KerA {

/**
 * This class implements the client side of multiReadGroup operations. It
 * uses the MultiOp Framework to manage multiple concurrent RPCs,
 * each reading one or more objects from a group's segments from a single server. The behavior
 * of this class is similar to an RpcWrapper, but it isn't an
 * RpcWrapper subclass because it doesn't correspond to a single RPC.
 */

class MultiReadGroup : public MultiOp {
    static const WireFormat::MultiOp::OpType type =
                                        WireFormat::MultiOp::OpType::READGROUP;
  PUBLIC:
  MultiReadGroup(RamCloud* ramcloud,
              MultiReadGroupObject* const requests[],
              uint32_t numRequests, uint32_t maxStreamletFetchBytes,
			  Buffer *rpcResponses[], uint32_t numResponses, uint64_t totalObjectsSize, uint64_t readerId, bool useSharedPlasma);

  PROTECTED:
    void appendRequest(MultiOpObject* request, Buffer* buf);
    bool readResponse(MultiOpObject* request, Buffer* response,
                      uint32_t* respOffset);

    DISALLOW_COPY_AND_ASSIGN(MultiReadGroup);
};

} // end KerA

#endif /* RAMCLOUD_MULTIREADGROUP_H */
