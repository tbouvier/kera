/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2012 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef RAMCLOUD_FAILSESSION_H
#define RAMCLOUD_FAILSESSION_H

#include "Transport.h"

namespace KerA {

/**
 * This class implements a singleton dummy session that will accept RPCs
 * but immediately cause them to fail. It allows errors to be deferred
 * from session create time to RPC wait time.
 */
class FailSession : public Transport::Session {
  public:
    virtual ~FailSession();
    virtual void abort();
    virtual void cancelRequest(Transport::RpcNotifier* notifier);
    static FailSession* get();
    virtual void release();
    virtual void sendRequest(Buffer* request, Buffer* response,
                Transport::RpcNotifier* notifier);

  PROTECTED:
    // No-one gets to create FailSessions except the get method.
    FailSession();

    DISALLOW_COPY_AND_ASSIGN(FailSession);
};

} // end KerA

#endif  // RAMCLOUD_FAILSESSION_H
