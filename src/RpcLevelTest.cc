/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2015-2016 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "TestUtil.h"
#include "RpcLevel.h"

namespace KerA {

TEST(RpcLevelTest, checkCall) {
    TestLog::Enable _;
    RpcLevel::setCurrentOpcode(WireFormat::Opcode::BACKUP_WRITE);
    RpcLevel::checkCall(WireFormat::Opcode::WRITE);
    EXPECT_EQ("checkCall: Unexpected RPC from BACKUP_WRITE to WRITE "
            "creates potential for deadlock; must update 'callees' "
            "table in scripts/genLevels.py", TestLog::get());

    TestLog::reset();
    RpcLevel::setCurrentOpcode(WireFormat::Opcode::WRITE);
    RpcLevel::checkCall(WireFormat::Opcode::BACKUP_WRITE);
    EXPECT_EQ("", TestLog::get());
}

TEST(RpcLevelTest, getLevel) {
    EXPECT_EQ(2, RpcLevel::getLevel(WireFormat::Opcode::CREATE_TABLE));
}

TEST(RpcLevelTest, maxLevel) {
    RpcLevel::savedMaxLevel = 11;
    EXPECT_EQ(11, RpcLevel::maxLevel());

    RpcLevel::savedMaxLevel = -1;
    EXPECT_EQ(3, RpcLevel::maxLevel());
    EXPECT_EQ(3, RpcLevel::savedMaxLevel);
}

}  // namespace KerA
