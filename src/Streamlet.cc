/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdint.h>

#include "Streamlet.h"

namespace KerA {


Streamlet::Streamlet(uint64_t streamId, uint32_t streamletId,
		uint32_t numberSegmentsPerGroup,
		uint32_t numberActiveGroupsPerStreamlet,
		const std::vector<Log*> &logs)
: appendLock("Streamlet::appendLock"),
  entryLocks(),
  migrationFlag(false),
  streamId(streamId),
  streamletId(streamletId),
  nextGroupId(1),
  minGroupId(0),
  maxGroupId(0),
  streamletIdToEntryGroupMap(),
  numberSegmentsPerGroup(numberSegmentsPerGroup),
  numberActiveGroupsPerStreamlet(numberActiveGroupsPerStreamlet),
  groupIdLogicalIdToPhysicalSegmentIdMap(),
  readerIdProcessedActiveGroupIndex()
{
	streamletIdToEntryGroupMap.resize(numberActiveGroupsPerStreamlet);
	int logsCount = static_cast<int>(logs.size());
	assert(logsCount > 0);
	int logsIndex = 0; //how to equally split logs between streamlet entries?
	//e.g. numberActiveGroupsPerStreamlet=4, streamletCount=12, streamletIds per this broker 1,5, logsCount=2, next code works
	//if more logs than streamlet entries? handle it outside this method, just pass logs list
	//if we want to use 1 log per streamlet, we pass the list of logs just for this streamlet, ok
	for (uint32_t q = 0; q < numberActiveGroupsPerStreamlet; q++) {
		Log* log = logs[logsIndex++];
		streamletIdToEntryGroupMap[q] = new StreamletGroupMetadata(log);
		if(logsIndex == logsCount) {
			logsIndex = 0;
		}
	}
	for (uint16_t i = 0; i < 32; i++) {
		entryLocks[i].setName("entryIdLocks"); //used by writers
	}
}

Streamlet::~Streamlet() {
	foreach(auto key, streamletIdToEntryGroupMap) {
		delete key;
	}
}

//A streamlet holds up to Q active groups at any moment, Q>=1 e.g., Q=8
//Producers write deterministically to one of these active groups based on producerId%Q
//Groups are advertised in order of their creation, being initially empty (i.e., having an empty head segment)
//Assuming each streamlet is associated with one consumer, it is consumer's responsibility to ask
//for next available group and pull records from its segments
// we  advertise groups only after the first append of a producer.

uint32_t
Streamlet::getNextAvailableGroupSegmentIds(uint64_t readerId,
	uint32_t nActiveGroups, Buffer* response) {

	uint32_t groupsLength = 0;

	//get next available groups up to nActiveGroups

	uint64_t firstGivenGroupId = 0;
	uint64_t lastGivenGroupId = 0; //invalid

	{
		SpinLock::Guard lock(appendLock);
		if (readerIdProcessedActiveGroupIndex.count(readerId)) {
			firstGivenGroupId = readerIdProcessedActiveGroupIndex[readerId];
			lastGivenGroupId = firstGivenGroupId;
		}

		if (maxGroupId > lastGivenGroupId) {
			firstGivenGroupId++;
			while (nActiveGroups > 0 && maxGroupId > lastGivenGroupId) {
				lastGivenGroupId++;
				groupsLength++;
				nActiveGroups--;
			}
			readerIdProcessedActiveGroupIndex[readerId] = lastGivenGroupId; //update last given
		} else {
			return groupsLength;
		}
	}

	SpinLock::Guard lock(appendLock);
	//take last segment id for each given group
	for(uint64_t groupId = firstGivenGroupId; groupId<=lastGivenGroupId; groupId++) {
		uint64_t lastLogicalSegmentId = groupIdLogicalIdToPhysicalSegmentIdMap[groupId].size();
		response->emplaceAppend<GroupSegmentId>(groupId, lastLogicalSegmentId);
	}

	return groupsLength;
}

//not used
uint64_t
Streamlet::getNextAvailableGroupId(uint64_t readerId)
{
	return INVALID_GROUP_ID;
}

void
Streamlet::updateGroupIdToSegmentsListMap(uint64_t groupId, uint64_t segmentId, uint64_t physicalSegmentId) {
	//association of logical segments with physical segments
	SpinLock::Guard lock(appendLock);
	groupIdLogicalIdToPhysicalSegmentIdMap[groupId][segmentId] = physicalSegmentId;
}

//gives the physical segment id see SM#segmentsMap
uint64_t
Streamlet::getSegmentById(uint64_t groupId, uint64_t segmentId) {
	SpinLock::Guard lock(appendLock);

	bool segmentExists = groupIdLogicalIdToPhysicalSegmentIdMap.count(groupId)
			&& groupIdLogicalIdToPhysicalSegmentIdMap[groupId].count(segmentId);
	if (segmentExists) {
		return groupIdLogicalIdToPhysicalSegmentIdMap[groupId][segmentId];
	} else {
		return 0; //unknown
	}
}

//todo maybe move this on Stream streamletIdToEntryGroupMap[streamletId][entry]
StreamletGroupMetadata*
Streamlet::getStreamletGroupMetadata(uint32_t entryIndex) {
	//once the Streamlet gets created and syncStreamlet done, this vector is immutable
	//each entry corresponds to the Q active group as computed by Stream appendMultipleObjects
	return streamletIdToEntryGroupMap[entryIndex]; //created at ObjectManager::syncStreamlet
}

uint64_t
Streamlet::getNextGroupId() {
	SpinLock::Guard lock(appendLock);
	maxGroupId = nextGroupId;
	return nextGroupId++;
}

//it verifies that this segment id is last of the group and that the group is filled
bool
Streamlet::checkSegmentLastAndGroupFilled(uint64_t groupId, uint64_t segmentId, uint32_t numberSegmentsPerGroup)
{
	SpinLock::Guard lock(appendLock);

	bool idExists = groupIdLogicalIdToPhysicalSegmentIdMap.count(groupId)
			&& groupIdLogicalIdToPhysicalSegmentIdMap[groupId].count(segmentId);

	if(!idExists) {
		fprintf(stdout, "======== no groupId segmentId found %lu %lu =======\n", groupId, segmentId);
		fflush (stdout);
		return false;
	}

	uint64_t lastLogicalSegmentId = groupIdLogicalIdToPhysicalSegmentIdMap[groupId].size();

	if(lastLogicalSegmentId < numberSegmentsPerGroup) {
		return false; //this group not yet filled todo we should add a closed field on groups needed later by recovery or group timeout
	}

	return (segmentId == lastLogicalSegmentId);
}

bool Streamlet::getSegmentsByGroupId(uint64_t groupId, bool useOffset,
	uint64_t segmentId, Buffer* outBuffer, uint32_t* length)
{
	SpinLock::Guard lock(appendLock);

	bool idExists = groupIdLogicalIdToPhysicalSegmentIdMap.count(groupId);

	if(!idExists) {
		fprintf(stdout, "======== no groupId found %lu=======\n", groupId);
		fflush (stdout);
		return false;
	}

	uint64_t lastLogicalSegmentId = groupIdLogicalIdToPhysicalSegmentIdMap[groupId].size();

	uint64_t startSegmentId = useOffset ? (segmentId+1) : 1;

	if(startSegmentId > lastLogicalSegmentId) {
		*length = 0;
		return true; //no new segments
	} else {
		*length = 2;
		outBuffer->emplaceAppend<SegmentIdAndOffset>(startSegmentId);
		outBuffer->emplaceAppend<SegmentIdAndOffset>(lastLogicalSegmentId);
	}

	return true;
}

} // namespace
