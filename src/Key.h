/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2012-2015 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef RAMCLOUD_KEY_H
#define RAMCLOUD_KEY_H

#include "Buffer.h"
#include "LogEntryTypes.h"
#include "Tub.h"

namespace KerA {

/**
 * The type of the hash for the key of an object.
 */
typedef uint64_t KeyHash;
/*
 * The length of a key in an object.
 */
typedef uint16_t KeyLength;

/**
 * RAMCloud objects can contain multiple keys in addition to the primary key.
 * However, this class is used only for the primary key which is a 2-tuple
 * consisting of a 64-bit table identifier and a binary string key. This
 * class represents that tuple, and allows us to pass Key references around,
 * rather than a 64-bit tableId, void pointer, and a length.
 *
 * This class also caches the hash value of the key, avoiding recomputation
 * when the hash is needed in various layers of the system.
 *
 * Multiple constructors provide convenient ways to construct a key given an
 * entry in the log, a Buffer containing a key (perhaps an RPC request), etc.
 *
 * Note that this class does not manage any memory. It simply creates a wrapper
 * around memory that is assumed to live at least as long as the key object
 * does itself. This is typically not a concern, since keys are usually built
 * in an RPC handler and refer to data either in the request itself, or in the
 * log, both of which remain allocated for the duration of the RPC.
 */
class Key {
  public:
    Key(LogEntryType type, Buffer& buffer);
    Key(uint64_t tableId, Buffer& buffer,
        uint32_t keyOffset, KeyLength keyLength);
    Key(uint64_t tableId, const void* key, KeyLength keyLength);

    KeyHash getHash();
    static KeyHash getHash(uint64_t tableId,
                           const void* key,
                           KeyLength keyLength);
    const void* getStringKey() const;
    KeyLength getStringKeyLength() const;
    uint64_t getTableId() const;
    bool operator==(const Key& other) const;
    bool operator!=(const Key& other) const;
    string toString() const;

  PRIVATE:
    /// The 64-bit table identifier.
    uint64_t tableId;

    /// Pointer to the binary string key.
    const void* key;

    /// Length of the binary string key in bytes.
    KeyLength keyLength;

    /// Cache for this key's hash. Initially empty and filled on demand when
    /// getHash() is called. Used to avoid recalculation in subsequent calls.
    Tub<KeyHash> hash;

    DISALLOW_COPY_AND_ASSIGN(Key);
};

} // end KerA

#endif // RAMCLOUD_KEY_H
