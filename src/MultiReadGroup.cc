/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MultiReadGroup.h"

namespace KerA {

// Default RejectRules to use if none are provided by the caller: rejects
// nothing.
//static RejectRules defaultRejectRules;

/**
 * Constructor for MultiReadGroup objects: initiates one or more RPCs for a
 * multiReadGroup operation, but returns once the RPCs have been initiated,
 * without waiting for any of them to complete.
 *
 * \param ramcloud
 *      The RAMCloud object that governs this operation.
 * \param requests
 *      Each element in this array describes one object to read and
 *      where to put the value.
 * \param numRequests
 *      Number of elements in \c requests.
 */
MultiReadGroup::MultiReadGroup(RamCloud* ramcloud,
                     MultiReadGroupObject* const requests[],
                     uint32_t numRequests, uint32_t maxStreamletFetchBytes,
					 Buffer *rpcResponses[], uint32_t numResponses, uint64_t totalObjectsSize, uint64_t readerId, bool useSharedPlasma)
        : MultiOp(ramcloud, type,
                  reinterpret_cast<MultiOpObject* const *>(requests),
                  numRequests, rpcResponses, numResponses, totalObjectsSize, readerId, useSharedPlasma)
{
    for (uint32_t i = 0; i < numRequests; i++) {
        requests[i]->respvalue->destroy();
    }
    startRpcs();
}
/**
 * Append a given MultiReadGroupObject to a buffer.
 *
 * It is the responsibility of the caller to ensure that the
 * MultiOpObject passed in is actually a MultiReadGroupObject.
 *
 * \param request
 *      MultiReadGroupObject request to append
 * \param buf
 *      Buffer to append to
 */
void
MultiReadGroup::appendRequest(MultiOpObject* request, Buffer* buf)
{
	MultiReadGroupObject* req = reinterpret_cast<MultiReadGroupObject*>(request);
	GroupSegmentInfo gsi = req->segmentInfo[0];
	buf->emplaceAppend<WireFormat::MultiOp::Request::ReadGroupPart>(
			req->tableId, gsi.streamletId, gsi.groupId, gsi.segmentId, gsi.offset,
			gsi.maxObjects, gsi.maxResponseLength);
}

/**
 * Read the MultiReadGroup response in the buffer given an offset
 * and put the response into a MultiReadGroupObject. This modifies
 * the offset as necessary and checks for missing data.
 *
 * It is the responsibility of the caller to ensure that the
 * MultiOpObject passed in is actually a MultiReadGroupObject.
 *
 * \param request
 *      MultiReadGroupObject where the interpreted response goes
 * \param response
 *      Buffer to read the response from
 * \param respOffset
 *      offset into the buffer for the current position
 *              which will be modified as this method reads.
 *
 * \return
 *      true if there is missing data
 */
bool
MultiReadGroup::readResponse(MultiOpObject* request,
                         Buffer* response,
                         uint32_t* respOffset)
{
	MultiReadGroupObject* req = static_cast<MultiReadGroupObject*>(request);
	const WireFormat::MultiOp::Response::ReadGroupPart* part =
			response->getOffset<WireFormat::MultiOp::Response::ReadGroupPart>(*respOffset);

	if (part == NULL) {
		TEST_LOG("missing Response::Part");
		return true;
	}

	req->status = part->status;
	*respOffset += sizeof32(*part);

	if (part->status == STATUS_OK) {
		if (response->size() < *respOffset + part->length) {
			TEST_LOG("missing object data");
			return true;
		}

		req->respvalue->construct();
		if (part->numberObjectsRead > static_cast<uint32_t>(0)) {
			req->respvalue->get()->append(response, *respOffset, part->length);
		}

		req->responseOffset = *respOffset;
		req->responseLength = part->length;
		req->numberObjectsRead = part->numberObjectsRead;
		req->nextOffset = part->offset;
		req->numberOfObjectEntries = part->numberOfObjectEntries;
		req->isSegmentClosed = part->isSegmentClosed;
		req->isGroupProcessed = part->isGroupProcessed;
		*respOffset += part->length; // used by next response
	}

    return false;
}

} // RAMCloud
