/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2009-2015 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef RAMCLOUD_VIRTUALSEGMENT_H
#define RAMCLOUD_VIRTUALSEGMENT_H

#include <atomic>
#include <vector>
#include <unordered_map>

#include "Common.h"
#include "Buffer.h"
#include "Crc32C.h"
#include "LogEntryTypes.h"
#include "LogMetadata.h"
#include "VirtualLogDigest.h"
#include "LogSegment.h"
#include "ReplicatedSegment.h"

#include "ChunkEntryDigest.h"
#include "ChunkEntry.h"
#include "SpinLock.h"

namespace KerA {

/**
 * KERA virtual segments for Stream's log/s
 *
 */
class VirtualSegment {
  public:
    // This doesn't belong here, but rather in SegmentManager.
    enum : uint64_t { INVALID_SEGMENT_ID = -1UL };
    typedef std::unique_lock<std::mutex> Lock;

#ifdef VALGRIND
    // can't use more than 1M, see http://bugs.kde.org/show_bug.cgi?id=203877
    enum { DEFAULT_SEGMENT_SIZE = 1 * 1024 * 1024 };
#else
    enum { DEFAULT_SEGMENT_SIZE = 8 * 1024 * 1024 };
#endif

	VirtualSegment(uint64_t masterId, uint64_t logId, uint64_t streamId,
			uint64_t segmentId, uint32_t segmentSize, LogSegment* vlogHead,
			VirtualSegment* prevHead, SegmentManager* segmentManager);
    bool hasSpaceFor(uint32_t length);
    uint32_t getFreeSpace();

    void close();

    // next are used by replicatedSegment and WriteSegmentRpc
    //and work now with physical segments virtual entries/chunks
    uint32_t getAppendedLength(SegmentCertificate* certificate = NULL);
    void appendToBuffer(Buffer& buffer,
                         uint32_t offset, uint32_t length,
						uint32_t* chunkIndex, uint32_t* lastChunkOffset);

    void addChunk(CEDSegment* chunk, uint32_t lastHeadOffset, LogSegment* vlogNewHead = NULL);

    void syncTo(uint32_t syncedLength);

    const uint64_t logId;
    const uint64_t streamId;

    //needed to syncTo chunks after replica sync
    VirtualSegment* prevHead;

    /// The ReplicatedSegment instance that is responsible for replicating and
    /// this segment to backups.
    ReplicatedSegment* replicatedSegment;
    SegmentManager* segmentManager;
    /// Hook used for linking this VirtualSegment into an intrusive list according
    /// to this object's state in SegmentManager. todo not used yet
//    IntrusiveListHook vlistEntries;

    /// Number of bytes in this segment that have been synced in Log::sync. This
    /// is used in Log::sync to avoid issuing a sync() call to ReplicatedSegment
    /// when the desired data has already been synced (perhaps by another thread
    /// that bundled our replication traffic with theirs). The point is to allow
    /// batching of objects during backup writes when there are multiple threads
    /// appending to the log.
    uint32_t syncedLength;

    //all chunks in order, each chunk corresponds to one LogSegment of one stream
    //a stream has multiple streamlets & active groups

    //next are used for keeping chunks's state todo refactor use vlogHead only
    LogSegment* vlogHead; //keeps chunks' metadata
    std::atomic<uint32_t> vlogHeadOffset; //start offset from where chunks
    std::atomic<uint32_t> vlogHeadLength; //length in vlogHead of appended chunks
    LogSegment* vlogNextHead; //NULL
    std::atomic<uint32_t> vlogNextHeadOffset; //0
    uint32_t chunksCount;

    SpinLock chunkLock; //protects segmentsHeadMap

    //physicalSegmentId to (replicated) length - used by segments[key]->updateDurableHead(value);
    std::unordered_map<uint64_t, uint32_t> segmentsHeadMap;

    //points to last valid chunk already synchronized, see syncTo
//    uint32_t durableChunkIndex; //offset into last chunk durably synced - vlogHead
    std::atomic<uint32_t> durableChunkOffset; // <= head , how much was durably replicated

    /// Tells the close state of this segment is replicated durably.
    /// Log syncLog ensures chunk syncTo updates the durableHead on
    /// chunk's physical segment
    std::atomic<bool> closedCommitted; //used by replicated segment but useless for us here todo remove it

    /// Indicates whether or not this segment may ever be appended to again.
    /// Closing a segment is a permanent operation. Once closed, all future
    /// appends will fail.
    /// also used by replicatedSegment replica to avoid taking chunkLock
    std::atomic<bool> closed;

    /// Tells the close state of this segment is replicated durably.
    // true if closed && closedCommitted && physical segments synced
    // todo the persistence manager PM will only release a physical segment if its virtual segment syncToHead is true
    // before releasing a physical segment, PM updates invalid true, takes segmentsLock, do its job, releases segmentsLock
    //on normal case, if invalid is true, we take segmentsLock and check again, then PM is called to fetch a physical segment as necessary
    //PM then updates invalid back to false
    // if the one of the replicated virtual segment is lost
    std::atomic<bool> syncToHead; //managed by replicated segment

    /**
     * Number of bytes that must be replicated to the backup before a
     * replica can be considered "open"; consists of all the data in the
     * segment when this ReplicatedSegment is constructed (typically
     * contains header information such as the log digest). This applies
     * only to initial opens, not to opens occurring during rereplication
     * (during rereplication the replica isn't considered open until it
     * is fully up-to-date).
     *
     * length of header+digest, points to first Chunk entry header
     * size of headerDigestBuffer
     */
    uint32_t openLen;

    uint64_t id; //as given by Log#nextSegmentId, virtual segment id important for backups & recovery later
    uint32_t segmentSize;

  PRIVATE:

    /// Offset to the next free byte in Segment.
    /// KERAI atomic because consumers read this without appendGroupActiveLock
    std::atomic<uint32_t> head; //closedCommited gives durable closed segment

    /// Latest Segment checksum (crc32c). This is a checksum of all metadata
    /// in the Segment (that is, every Segment::Entry and ::Header).
    /// Any user data that is stored in the Segment is unprotected. Integrity
    /// is their responsibility. Used to generate SegmentCertificates.
    Crc32C checksum;

    DISALLOW_COPY_AND_ASSIGN(VirtualSegment);
};

} // namespace

#endif // !RAMCLOUD_VIRTUALSEGMENT_H
